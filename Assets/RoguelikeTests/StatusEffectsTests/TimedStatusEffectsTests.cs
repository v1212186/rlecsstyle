﻿using System;
using System.Collections;
using Ecs.Core;
using NUnit.Framework;
using Roguelike.Scripts.Actions.DirectionalActions;
using Roguelike.Scripts.Attributes;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.StatusEffects.TimedStatusEffects;
using UnityEngine;
using UnityEngine.TestTools;

namespace RoguelikeTests.StatusEffectsTests
{
    public class TimedStatusEffectsTests : StatusEffectsTestsBase
    {

        private const int Duration = 3;
        private const float ModifierValue = 10.0f;

        [UnityTest]
        public IEnumerator TimedStatusEffect_MustAffect_WhenApplied()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            TimedIncreaseAttributeStatusEffect timedIncreaseAttributeStatusEffect =
                new TimedIncreaseAttributeStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(), Duration,
                    new AttributeModifier(ModifierValue, AttributeModifierType.Flat, AttributeType.Health), fastGuid);
            statusEffectsComponent.ApplyStatusEffect(timedIncreaseAttributeStatusEffect);

            Assert.IsTrue(Math.Abs(currentHealth + ModifierValue - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon && 
                          statusEffectsComponent.TimedStatusEffects.Count == 1);
        }

        [UnityTest]
        public IEnumerator TimedStatusEffect_ShouldExpireWhenDurationEnded()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            TimedIncreaseAttributeStatusEffect timedIncreaseAttributeStatusEffect =
                new TimedIncreaseAttributeStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(), Duration,
                    new AttributeModifier(ModifierValue, AttributeModifierType.Flat, AttributeType.Health), fastGuid);
            statusEffectsComponent.ApplyStatusEffect(timedIncreaseAttributeStatusEffect);

            while (timedIncreaseAttributeStatusEffect.CurrentDuration>0)
            {
                player.GetEntityComponent<AbilitiesSetComponent>().CurrentAction = new MoveEntityAction(Vector2Int.right);
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForEndOfFrame();

            Assert.IsTrue(Math.Abs(currentHealth - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon &&
                          statusEffectsComponent.TimedStatusEffects.Count == 0);
        }

        [UnityTest]
        public IEnumerator TimedStatusEffect_CanBeRemoved()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            TimedIncreaseAttributeStatusEffect timedIncreaseAttributeStatusEffect =
                new TimedIncreaseAttributeStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(), Duration,
                    new AttributeModifier(ModifierValue, AttributeModifierType.Flat, AttributeType.Health), fastGuid);
            statusEffectsComponent.ApplyStatusEffect(timedIncreaseAttributeStatusEffect);

            statusEffectsComponent.RemoveStatusEffect(timedIncreaseAttributeStatusEffect);

            Assert.IsTrue(Math.Abs(currentHealth - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon &&
                          statusEffectsComponent.TimedStatusEffects.Count == 0);
        }

        [UnityTest]
        public IEnumerator TimedStatusEffect_CanBeRemoved_BySource()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            TimedIncreaseAttributeStatusEffect timedIncreaseAttributeStatusEffect =
                new TimedIncreaseAttributeStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(), Duration,
                    new AttributeModifier(ModifierValue, AttributeModifierType.Flat, AttributeType.Health), fastGuid);
            statusEffectsComponent.ApplyStatusEffect(timedIncreaseAttributeStatusEffect);

            statusEffectsComponent.RemoveAllStatusEffectsFromSource(fastGuid);

            Assert.IsTrue(Math.Abs(currentHealth - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon &&
                          statusEffectsComponent.TimedStatusEffects.Count == 0);
        }

    }
}
