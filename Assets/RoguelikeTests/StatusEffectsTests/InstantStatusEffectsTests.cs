﻿using System;
using System.Collections;
using Ecs.Core;
using NUnit.Framework;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.StatusEffects.InstantStatusEffects;
using UnityEngine.TestTools;

namespace RoguelikeTests.StatusEffectsTests
{
    [TestFixture]
    public class InstantStatusEffectsTests : StatusEffectsTestsBase
    {
        private float damageHealAmount = 25.0f;

        [UnityTest]
        public IEnumerator ReceiveDamageInstantStatusEffect_CanDamage()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            ReceiveDamageStatusEffect receiveDamageStatusEffect =
                new ReceiveDamageStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(), damageHealAmount);
            statusEffectsComponent.ApplyStatusEffect(receiveDamageStatusEffect);

            Assert.IsTrue(Math.Abs(currentHealth - damageHealAmount - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon);
        }

        [UnityTest]
        public IEnumerator ReceiveHealInstantStatusEffect_CanHeal()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            ReceiveDamageStatusEffect receiveDamageStatusEffect =
                new ReceiveDamageStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(), damageHealAmount);
            statusEffectsComponent.ApplyStatusEffect(receiveDamageStatusEffect);

            ReceiveHealStatusEffect receiveHealStatusEffect =
                new ReceiveHealStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(), damageHealAmount);
            statusEffectsComponent.ApplyStatusEffect(receiveHealStatusEffect);

            Assert.IsTrue(Math.Abs(currentHealth - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon);
        }
    }
}
