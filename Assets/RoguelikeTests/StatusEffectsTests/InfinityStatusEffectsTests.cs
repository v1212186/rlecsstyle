﻿using System;
using System.Collections;
using Ecs.Core;
using NUnit.Framework;
using Roguelike.Scripts.Attributes;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.StatusEffects.InfinityStatusEffects;
using UnityEngine.TestTools;

namespace RoguelikeTests.StatusEffectsTests
{
    public class InfinityStatusEffectsTests : StatusEffectsTestsBase {

        private const float ModifierValue = 10.0f;

        [UnityTest]
        public IEnumerator InfinityStatusEffect_MustAffect_WhenApplied()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            InfinityIncreaseAttributeStatusEffect infinityIncreaseAttributeStatusEffect =
                new InfinityIncreaseAttributeStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(),
                    new AttributeModifier(ModifierValue, AttributeModifierType.Flat, AttributeType.Health), fastGuid);
            statusEffectsComponent.ApplyStatusEffect(infinityIncreaseAttributeStatusEffect);

            Assert.IsTrue(Math.Abs(currentHealth + ModifierValue - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon &&
                          statusEffectsComponent.InfinityStatusEffects.Count == 1);
        }

        [UnityTest]
        public IEnumerator InfinityStatusEffect_CanBeRemoved()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            InfinityIncreaseAttributeStatusEffect infinityIncreaseAttributeStatusEffect =
                new InfinityIncreaseAttributeStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(),
                    new AttributeModifier(ModifierValue, AttributeModifierType.Flat, AttributeType.Health), fastGuid);
            statusEffectsComponent.ApplyStatusEffect(infinityIncreaseAttributeStatusEffect);

            statusEffectsComponent.RemoveStatusEffect(infinityIncreaseAttributeStatusEffect);

            Assert.IsTrue(Math.Abs(currentHealth  - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon &&
                          statusEffectsComponent.InfinityStatusEffects.Count == 0);
        }

        [UnityTest]
        public IEnumerator InfinityStatusEffect_CanBeRemoved_BySource()
        {
            yield return WaitForFrameAndStartGame();

            Entity player = World.Instance.GameManager.PlayerEntity;
            HealthAttributeComponent healthAttributeComponent = player.GetEntityComponent<HealthAttributeComponent>();
            StatusEffectsComponent statusEffectsComponent = player.GetEntityComponent<StatusEffectsComponent>();

            float currentHealth = healthAttributeComponent.GetCurrentHealthValue();

            InfinityIncreaseAttributeStatusEffect infinityIncreaseAttributeStatusEffect =
                new InfinityIncreaseAttributeStatusEffect(World.Instance.ResourcesManager.ScrollsSprites.GetSprite(),
                    new AttributeModifier(ModifierValue, AttributeModifierType.Flat, AttributeType.Health), fastGuid);
            statusEffectsComponent.ApplyStatusEffect(infinityIncreaseAttributeStatusEffect);

            statusEffectsComponent.RemoveAllStatusEffectsFromSource(fastGuid);

            Assert.IsTrue(Math.Abs(currentHealth - healthAttributeComponent.GetCurrentHealthValue()) < float.Epsilon &&
                          statusEffectsComponent.InfinityStatusEffects.Count == 0);
        }
    }
}
