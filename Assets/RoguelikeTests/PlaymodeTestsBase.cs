﻿using System.Collections;
using NUnit.Framework;
using Roguelike.Scripts.Ecs;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RoguelikeTests
{
    [TestFixture]
    public abstract class PlaymodeTestsBase {

        [SetUp]
        public virtual void Setup()
        {
            SceneManager.LoadScene("main");
        }

        protected IEnumerator WaitForFrameAndStartGame()
        {
            yield return null;
            World.Instance.GameManager.StartNewGame(World.Instance.EntitiesGenerator.CreatePlayerEntity(Vector2Int.one));
        }

    }
}
