﻿using System;
using NUnit.Framework;

namespace RoguelikeTests.EditorTests.AttributeTests
{
    public class RemovingAttributeModifiersWithSource : AttributesTestsBase {

        [Test]
        public void AttackAttribute_RemoveModifierWithSource()
        {
            attackAttribute.AddModifier(attackFlatAttributeModifier);
            attackAttribute.AddModifier(attackPercentAddAttributeModifier);
            attackAttribute.AddModifier(attackPercentMultAttributeModifier);

            attackAttribute.RemoveAllAttributeModifiersFromSource(fastGuid);
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - attackAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_RemoveModifierWithSource()
        {
            defenceAttribute.AddModifier(defenceFlatAttributeModifier);
            defenceAttribute.AddModifier(defencePercentAddAttributeModifier);
            defenceAttribute.AddModifier(defencePercentMultAttributeModifier);

            defenceAttribute.RemoveAllAttributeModifiersFromSource(fastGuid);
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - defenceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_RemoveModifierWithSource()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChanceFlatAttributeModifier);
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentAddAttributeModifier);
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentMultAttributeModifier);

            criticalHitChanceAttribute.RemoveAllAttributeModifiersFromSource(fastGuid);
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - criticalHitChanceAttribute.BaseValue) < float.Epsilon);
        }


        [Test]
        public void HitChanceAttribute_RemoveModifierWithSource()
        {
            hitChanceAttribute.AddModifier(hitChanceFlatAttributeModifier);
            hitChanceAttribute.AddModifier(hitChancePercentAddAttributeModifier);
            hitChanceAttribute.AddModifier(hitChancePercentMultAttributeModifier);

            hitChanceAttribute.RemoveAllAttributeModifiersFromSource(fastGuid);
            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - hitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_RemoveModifierWithSource()
        {
            healthAttribute.AddModifier(healthFlatAttributeModifier);
            healthAttribute.AddModifier(healthPercentAddAttributeModifier);
            healthAttribute.AddModifier(healthPercentMultAttributeModifier);

            healthAttribute.RemoveAllAttributeModifiersFromSource(fastGuid);
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - healthAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_RemoveModifierWithSource()
        {
            energyAttribute.AddModifier(energyFlatAttributeModifier);
            energyAttribute.AddModifier(energyPercentAddAttributeModifier);
            energyAttribute.AddModifier(energyPercentMultAttributeModifier);

            energyAttribute.RemoveAllAttributeModifiersFromSource(fastGuid);
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - energyAttribute.BaseValue) < float.Epsilon);
        }

    }
}
