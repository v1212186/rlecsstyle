﻿using System;
using NUnit.Framework;
using Roguelike.Scripts.Attributes;
using UnityEngine;

namespace RoguelikeTests.EditorTests.AttributeTests
{
    public class AddingRemovingModifiers : AttributesTestsBase
    {

        #region Adding flat modifier

        [Test]
        public void AttackAttribute_Add_FlatModifier()
        {
            attackAttribute.AddModifier(attackFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - (attackAttribute.BaseValue + FlatModifierValue)) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_Add_FlatModifier()
        {
            defenceAttribute.AddModifier(defenceFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - (defenceAttribute.BaseValue + FlatModifierValue)) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_Add_FlatModifier()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChanceFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - (criticalHitChanceAttribute.BaseValue + FlatModifierValue)) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_Add_FlatModifier()
        {
            hitChanceAttribute.AddModifier(hitChanceFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - (hitChanceAttribute.BaseValue + FlatModifierValue)) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_Add_FlatModifier()
        {
            healthAttribute.AddModifier(healthFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - (healthAttribute.BaseValue + FlatModifierValue)) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_Add_FlatModifier()
        {
            energyAttribute.AddModifier(energyFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - (energyAttribute.BaseValue - FlatModifierValue)) < float.Epsilon);
        }

        #endregion

        #region Removing flat modifier

        [Test]
        public void AttackAttribute_Remove_FlatModifier()
        {
            attackAttribute.AddModifier(attackFlatAttributeModifier);
            attackAttribute.RemoveModifier(attackFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - attackAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_Remove_FlatModifier()
        {
            defenceAttribute.AddModifier(defenceFlatAttributeModifier);
            defenceAttribute.RemoveModifier(defenceFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - defenceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_Remove_FlatModifier()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChanceFlatAttributeModifier);
            criticalHitChanceAttribute.RemoveModifier(criticalHitChanceFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - criticalHitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_Remove_FlatModifier()
        {
            hitChanceAttribute.AddModifier(hitChanceFlatAttributeModifier);
            hitChanceAttribute.RemoveModifier(hitChanceFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - hitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_Remove_FlatModifier()
        {
            healthAttribute.AddModifier(healthFlatAttributeModifier);
            healthAttribute.RemoveModifier(healthFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - healthAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_Remove_FlatModifier()
        {
            energyAttribute.AddModifier(energyFlatAttributeModifier);
            energyAttribute.RemoveModifier(energyFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - energyAttribute.BaseValue) < float.Epsilon);
        }

        #endregion

        #region Adding percent add modifier

        [Test]
        public void AttackAttribute_Add_PercentAddModifier()
        {
            attackAttribute.AddModifier(attackPercentAddAttributeModifier);
            float calculatedValue = Mathf.Round(attackAttribute.BaseValue * (1.0f + PercentAddModifierValue));
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_Add_PercentAddModifier()
        {
            defenceAttribute.AddModifier(defencePercentAddAttributeModifier);
            float calculatedValue = Mathf.Round(defenceAttribute.BaseValue * (1.0f + PercentAddModifierValue));
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_Add_PercentAddModifier()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentAddAttributeModifier);
            float calculatedValue = Mathf.Round(criticalHitChanceAttribute.BaseValue * (1.0f + PercentAddModifierValue));
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_Add_PercentAddModifier()
        {
            hitChanceAttribute.AddModifier(hitChancePercentAddAttributeModifier);
            float calculatedValue = Mathf.Round(hitChanceAttribute.BaseValue * (1.0f + PercentAddModifierValue));
            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_Add_PercentAddModifier()
        {
            healthAttribute.AddModifier(healthPercentAddAttributeModifier);
            float calculatedValue = Mathf.Round(healthAttribute.BaseValue * (1.0f + PercentAddModifierValue));
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_Add_PercentAddModifier()
        {
            energyAttribute.AddModifier(energyPercentAddAttributeModifier);
            float calculatedValue = Mathf.Round(energyAttribute.BaseValue / (1.0f + PercentAddModifierValue));
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        #endregion

        #region Removing percent add modifier

        [Test]
        public void AttackAttribute_Remove_PercentAddModifier()
        {
            attackAttribute.AddModifier(attackPercentAddAttributeModifier);
            attackAttribute.RemoveModifier(attackPercentAddAttributeModifier);
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - attackAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_Remove_PercentAddModifier()
        {
            defenceAttribute.AddModifier(defencePercentAddAttributeModifier);
            defenceAttribute.RemoveModifier(defencePercentAddAttributeModifier);
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - defenceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_Remove_PercentAddModifier()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentAddAttributeModifier);
            criticalHitChanceAttribute.RemoveModifier(criticalHitChancePercentAddAttributeModifier);
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - criticalHitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_Remove_PercentAddModifier()
        {
            hitChanceAttribute.AddModifier(hitChancePercentAddAttributeModifier);
            hitChanceAttribute.RemoveModifier(hitChancePercentAddAttributeModifier);
            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - hitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HealthChanceAttribute_Remove_PercentAddModifier()
        {
            healthAttribute.AddModifier(healthPercentAddAttributeModifier);
            healthAttribute.RemoveModifier(healthPercentAddAttributeModifier);
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - healthAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void EnergyChanceAttribute_Remove_PercentAddModifier()
        {
            energyAttribute.AddModifier(energyPercentAddAttributeModifier);
            energyAttribute.RemoveModifier(energyPercentAddAttributeModifier);
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - energyAttribute.BaseValue) < float.Epsilon);
        }

        #endregion

        #region Adding percent mult modifier

        [Test]
        public void AttackAttribute_Add_PercentMultModifier()
        {
            attackAttribute.AddModifier(attackPercentMultAttributeModifier);
            float calculatedValue = Mathf.Round(attackAttribute.BaseValue * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_Add_PercentMultModifier()
        {
            defenceAttribute.AddModifier(defencePercentMultAttributeModifier);
            float calculatedValue = Mathf.Round(defenceAttribute.BaseValue * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_Add_PercentMultModifier()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentMultAttributeModifier);
            float calculatedValue = Mathf.Round(criticalHitChanceAttribute.BaseValue * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_Add_PercentMultModifier()
        {
            hitChanceAttribute.AddModifier(hitChancePercentMultAttributeModifier);
            float calculatedValue = Mathf.Round(hitChanceAttribute.BaseValue * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_Add_PercentMultModifier()
        {
            healthAttribute.AddModifier(healthPercentMultAttributeModifier);
            float calculatedValue = Mathf.Round(healthAttribute.BaseValue * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_Add_PercentMultModifier()
        {
            energyAttribute.AddModifier(energyPercentMultAttributeModifier);
            float calculatedValue = Mathf.Round(energyAttribute.BaseValue / (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        #endregion

        #region Removing percent mult modifier

        [Test]
        public void AttackAttribute_Remove_PercentMultModifier()
        {
            attackAttribute.AddModifier(attackPercentMultAttributeModifier);
            attackAttribute.RemoveModifier(attackPercentMultAttributeModifier);
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - attackAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_Remove_PercentMultModifier()
        {
            defenceAttribute.AddModifier(defencePercentMultAttributeModifier);
            defenceAttribute.RemoveModifier(defencePercentMultAttributeModifier);
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - defenceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_Remove_PercentMultModifier()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentMultAttributeModifier);
            criticalHitChanceAttribute.RemoveModifier(criticalHitChancePercentMultAttributeModifier);
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - criticalHitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_Remove_PercentMultModifier()
        {
            hitChanceAttribute.AddModifier(hitChancePercentMultAttributeModifier);
            hitChanceAttribute.RemoveModifier(hitChancePercentMultAttributeModifier);
            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - hitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HealthChanceAttribute_Remove_PercentMultModifier()
        {
            healthAttribute.AddModifier(healthPercentMultAttributeModifier);
            healthAttribute.RemoveModifier(healthPercentMultAttributeModifier);
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - healthAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void EnergyChanceAttribute_Remove_PercentMultModifier()
        {
            energyAttribute.AddModifier(energyPercentMultAttributeModifier);
            energyAttribute.RemoveModifier(energyPercentMultAttributeModifier);
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - energyAttribute.BaseValue) < float.Epsilon);
        }

        #endregion

        #region Adding mixed modifiers

        [Test]
        public void AttackAttribute_Add_FlatPercentAddPercentMult_Modifiers()
        {
            attackAttribute.AddModifier(attackFlatAttributeModifier);
            attackAttribute.AddModifier(attackPercentAddAttributeModifier);
            attackAttribute.AddModifier(attackPercentMultAttributeModifier);

            float calculatedValue = Mathf.Round((attackAttribute.BaseValue + FlatModifierValue) * (1.0f + PercentAddModifierValue) * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_Add_FlatPercentAddPercentMult_Modifiers()
        {
            defenceAttribute.AddModifier(defenceFlatAttributeModifier);
            defenceAttribute.AddModifier(defencePercentAddAttributeModifier);
            defenceAttribute.AddModifier(defencePercentMultAttributeModifier);

            float calculatedValue = Mathf.Round((defenceAttribute.BaseValue + FlatModifierValue) * (1.0f + PercentAddModifierValue) * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_Add_FlatPercentAddPercentMult_Modifiers()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChanceFlatAttributeModifier);
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentAddAttributeModifier);
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentMultAttributeModifier);

            float calculatedValue = Mathf.Round((criticalHitChanceAttribute.BaseValue + FlatModifierValue) * (1.0f + PercentAddModifierValue) * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_Add_FlatPercentAddPercentMult_Modifiers()
        {
            hitChanceAttribute.AddModifier(hitChanceFlatAttributeModifier);
            hitChanceAttribute.AddModifier(hitChancePercentAddAttributeModifier);
            hitChanceAttribute.AddModifier(hitChancePercentMultAttributeModifier);

            float calculatedValue = Mathf.Clamp(Mathf.Round((hitChanceAttribute.BaseValue + FlatModifierValue) * (1.0f + PercentAddModifierValue) * (1.0f + PercentMultModifierValue)),
                HitChanceAttribute.ValueMinCap, HitChanceAttribute.ValueMaxCap);

            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_Add_FlatPercentAddPercentMult_Modifiers()
        {
            healthAttribute.AddModifier(healthFlatAttributeModifier);
            healthAttribute.AddModifier(healthPercentAddAttributeModifier);
            healthAttribute.AddModifier(healthPercentMultAttributeModifier);

            float calculatedValue = Mathf.Round((healthAttribute.BaseValue + FlatModifierValue) * (1.0f + PercentAddModifierValue) * (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_Add_FlatPercentAddPercentMult_Modifiers()
        {
            energyAttribute.AddModifier(energyFlatAttributeModifier);
            energyAttribute.AddModifier(energyPercentAddAttributeModifier);
            energyAttribute.AddModifier(energyPercentMultAttributeModifier);

            float calculatedValue = Mathf.Round((energyAttribute.BaseValue - FlatModifierValue) / (1.0f + PercentAddModifierValue) / (1.0f + PercentMultModifierValue));
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - calculatedValue) < float.Epsilon);
        }

        #endregion

        #region Removing mixed modifiers

        [Test]
        public void AttackAttribute_Remove_FlatPercentAddPercentMult_Modifiers()
        {
            attackAttribute.AddModifier(attackFlatAttributeModifier);
            attackAttribute.AddModifier(attackPercentAddAttributeModifier);
            attackAttribute.AddModifier(attackPercentMultAttributeModifier);

            attackAttribute.RemoveModifier(attackFlatAttributeModifier);
            attackAttribute.RemoveModifier(attackPercentAddAttributeModifier);
            attackAttribute.RemoveModifier(attackPercentMultAttributeModifier);

            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - attackAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_Remove_FlatPercentAddPercentMult_Modifiers()
        {
            defenceAttribute.AddModifier(defenceFlatAttributeModifier);
            defenceAttribute.AddModifier(defencePercentAddAttributeModifier);
            defenceAttribute.AddModifier(defencePercentMultAttributeModifier);

            defenceAttribute.RemoveModifier(defenceFlatAttributeModifier);
            defenceAttribute.RemoveModifier(defencePercentAddAttributeModifier);
            defenceAttribute.RemoveModifier(defencePercentMultAttributeModifier);

            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - defenceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_Remove_FlatPercentAddPercentMult_Modifiers()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChanceFlatAttributeModifier);
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentAddAttributeModifier);
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentMultAttributeModifier);

            criticalHitChanceAttribute.RemoveModifier(criticalHitChanceFlatAttributeModifier);
            criticalHitChanceAttribute.RemoveModifier(criticalHitChancePercentAddAttributeModifier);
            criticalHitChanceAttribute.RemoveModifier(criticalHitChancePercentMultAttributeModifier);

            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - criticalHitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_Remove_FlatPercentAddPercentMult_Modifiers()
        {
            hitChanceAttribute.AddModifier(hitChanceFlatAttributeModifier);
            hitChanceAttribute.AddModifier(hitChancePercentAddAttributeModifier);
            hitChanceAttribute.AddModifier(hitChancePercentMultAttributeModifier);

            hitChanceAttribute.RemoveModifier(hitChanceFlatAttributeModifier);
            hitChanceAttribute.RemoveModifier(hitChancePercentAddAttributeModifier);
            hitChanceAttribute.RemoveModifier(hitChancePercentMultAttributeModifier);

            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - hitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_Remove_FlatPercentAddPercentMult_Modifiers()
        {
            healthAttribute.AddModifier(healthFlatAttributeModifier);
            healthAttribute.AddModifier(healthPercentAddAttributeModifier);
            healthAttribute.AddModifier(healthPercentMultAttributeModifier);

            healthAttribute.RemoveModifier(healthFlatAttributeModifier);
            healthAttribute.RemoveModifier(healthPercentAddAttributeModifier);
            healthAttribute.RemoveModifier(healthPercentMultAttributeModifier);

            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - healthAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_Remove_FlatPercentAddPercentMult_Modifiers()
        {
            energyAttribute.AddModifier(energyFlatAttributeModifier);
            energyAttribute.AddModifier(energyPercentAddAttributeModifier);
            energyAttribute.AddModifier(energyPercentMultAttributeModifier);

            energyAttribute.RemoveModifier(energyFlatAttributeModifier);
            energyAttribute.RemoveModifier(energyPercentAddAttributeModifier);
            energyAttribute.RemoveModifier(energyPercentMultAttributeModifier);

            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - energyAttribute.BaseValue) < float.Epsilon);
        }

        #endregion

    }
}
