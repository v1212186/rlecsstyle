﻿using System.Collections.Generic;
using Ecs.Core;
using NUnit.Framework;
using Roguelike.Scripts.Attributes;

namespace RoguelikeTests.EditorTests.AttributeTests
{
    //TODO сделать тесты на аттрибуты, которые имеют верхние/нижние капы
    [TestFixture]
    public class AttributesTestsBase
    {
        protected const float FloatInnacurateComparison = 0.0001f;

        protected FastGuid fastGuid = FastGuid.NewGuid();
        protected AttackAttribute attackAttribute;
        protected DefenceAttribute defenceAttribute;
        protected CriticalHitChanceAttribute criticalHitChanceAttribute;
        protected HitChanceAttribute hitChanceAttribute;
        protected HealthAttribute healthAttribute;
        protected EnergyAttribute energyAttribute;

        protected const float ConstructorBaseValue = 50.0f;

        #region Flat modifiers

        protected const float FlatModifierValue = 10.0f;

        protected static AttributeModifier attackFlatAttributeModifier =
           new AttributeModifier(FlatModifierValue, AttributeModifierType.Flat, AttributeType.Attack);
        protected static AttributeModifier defenceFlatAttributeModifier =
            new AttributeModifier(FlatModifierValue, AttributeModifierType.Flat, AttributeType.Defence);
        protected static AttributeModifier criticalHitChanceFlatAttributeModifier =
            new AttributeModifier(FlatModifierValue, AttributeModifierType.Flat, AttributeType.CriticalHit);
        protected static AttributeModifier hitChanceFlatAttributeModifier =
            new AttributeModifier(FlatModifierValue, AttributeModifierType.Flat, AttributeType.HitChance);
        protected static AttributeModifier healthFlatAttributeModifier =
            new AttributeModifier(FlatModifierValue, AttributeModifierType.Flat, AttributeType.Health);
        protected static AttributeModifier energyFlatAttributeModifier =
            new AttributeModifier(FlatModifierValue, AttributeModifierType.Flat, AttributeType.Energy);

        protected static IEnumerable<TestCaseData> FlatAttributeModifiers
        {
            get
            {
                yield return new TestCaseData(attackFlatAttributeModifier);
                yield return new TestCaseData(defenceFlatAttributeModifier);
                yield return new TestCaseData(criticalHitChanceFlatAttributeModifier);
                yield return new TestCaseData(hitChanceFlatAttributeModifier);
                yield return new TestCaseData(healthFlatAttributeModifier);
                yield return new TestCaseData(energyFlatAttributeModifier);
            }
        }

        #endregion

        #region PercentAdd modifiers

        protected const float PercentAddModifierValue = 0.05f;

        protected static AttributeModifier attackPercentAddAttributeModifier =
            new AttributeModifier(PercentAddModifierValue, AttributeModifierType.PercentAdd, AttributeType.Attack);
        protected static AttributeModifier defencePercentAddAttributeModifier =
            new AttributeModifier(PercentAddModifierValue, AttributeModifierType.PercentAdd, AttributeType.Defence);
        protected static AttributeModifier criticalHitChancePercentAddAttributeModifier =
            new AttributeModifier(PercentAddModifierValue, AttributeModifierType.PercentAdd, AttributeType.CriticalHit);
        protected static AttributeModifier hitChancePercentAddAttributeModifier =
            new AttributeModifier(PercentAddModifierValue, AttributeModifierType.PercentAdd, AttributeType.HitChance);
        protected static AttributeModifier healthPercentAddAttributeModifier =
            new AttributeModifier(PercentAddModifierValue, AttributeModifierType.PercentAdd, AttributeType.Health);
        protected static AttributeModifier energyPercentAddAttributeModifier =
            new AttributeModifier(PercentAddModifierValue, AttributeModifierType.PercentAdd, AttributeType.Energy);

        protected static IEnumerable<TestCaseData> PercentAddAttributeModifiers
        {
            get
            {
                yield return new TestCaseData(attackPercentAddAttributeModifier);
                yield return new TestCaseData(defencePercentAddAttributeModifier);
                yield return new TestCaseData(criticalHitChancePercentAddAttributeModifier);
                yield return new TestCaseData(hitChancePercentAddAttributeModifier);
                yield return new TestCaseData(healthPercentAddAttributeModifier);
                yield return new TestCaseData(energyPercentAddAttributeModifier);
            }
        }

        #endregion

        #region PercentMult modifiers

        protected const float PercentMultModifierValue = 0.1f;

        protected static AttributeModifier attackPercentMultAttributeModifier =
            new AttributeModifier(PercentMultModifierValue, AttributeModifierType.PercentMult, AttributeType.Attack);
        protected static AttributeModifier defencePercentMultAttributeModifier =
            new AttributeModifier(PercentMultModifierValue, AttributeModifierType.PercentMult, AttributeType.Defence);
        protected static AttributeModifier criticalHitChancePercentMultAttributeModifier =
            new AttributeModifier(PercentMultModifierValue, AttributeModifierType.PercentMult, AttributeType.CriticalHit);
        protected static AttributeModifier hitChancePercentMultAttributeModifier =
            new AttributeModifier(PercentMultModifierValue, AttributeModifierType.PercentMult, AttributeType.HitChance);
        protected static AttributeModifier healthPercentMultAttributeModifier =
            new AttributeModifier(PercentMultModifierValue, AttributeModifierType.PercentMult, AttributeType.Health);
        protected static AttributeModifier energyPercentMultAttributeModifier =
            new AttributeModifier(PercentMultModifierValue, AttributeModifierType.PercentMult, AttributeType.Energy);

        protected static IEnumerable<TestCaseData> PercentMultAttributeModifiers
        {
            get
            {
                yield return new TestCaseData(attackPercentMultAttributeModifier);
                yield return new TestCaseData(defencePercentMultAttributeModifier);
                yield return new TestCaseData(criticalHitChancePercentMultAttributeModifier);
                yield return new TestCaseData(hitChancePercentMultAttributeModifier);
                yield return new TestCaseData(healthPercentMultAttributeModifier);
                yield return new TestCaseData(energyPercentMultAttributeModifier);
            }
        }

        #endregion

        #region Setup

        [SetUp]
        public void SetUp()
        {
            attackAttribute = new AttackAttribute(ConstructorBaseValue);
            defenceAttribute = new DefenceAttribute(ConstructorBaseValue);
            criticalHitChanceAttribute = new CriticalHitChanceAttribute();
            hitChanceAttribute = new HitChanceAttribute();
            healthAttribute = new HealthAttribute(ConstructorBaseValue);
            energyAttribute = new EnergyAttribute();

            attackFlatAttributeModifier.SetSourceGuid(fastGuid);
            attackPercentAddAttributeModifier.SetSourceGuid(fastGuid);
            attackPercentMultAttributeModifier.SetSourceGuid(fastGuid);

            defenceFlatAttributeModifier.SetSourceGuid(fastGuid);
            defencePercentAddAttributeModifier.SetSourceGuid(fastGuid);
            defencePercentMultAttributeModifier.SetSourceGuid(fastGuid);

            criticalHitChanceFlatAttributeModifier.SetSourceGuid(fastGuid);
            criticalHitChancePercentAddAttributeModifier.SetSourceGuid(fastGuid);
            criticalHitChancePercentMultAttributeModifier.SetSourceGuid(fastGuid);

            hitChanceFlatAttributeModifier.SetSourceGuid(fastGuid);
            hitChancePercentAddAttributeModifier.SetSourceGuid(fastGuid);
            hitChancePercentMultAttributeModifier.SetSourceGuid(fastGuid);

            healthFlatAttributeModifier.SetSourceGuid(fastGuid);
            healthPercentAddAttributeModifier.SetSourceGuid(fastGuid);
            healthPercentMultAttributeModifier.SetSourceGuid(fastGuid);

            energyFlatAttributeModifier.SetSourceGuid(fastGuid);
            energyPercentAddAttributeModifier.SetSourceGuid(fastGuid);
            energyPercentMultAttributeModifier.SetSourceGuid(fastGuid);
        }

        #endregion

    }
}
