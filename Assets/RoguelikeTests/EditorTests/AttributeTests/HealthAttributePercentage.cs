﻿using System;
using NUnit.Framework;

namespace RoguelikeTests.EditorTests.AttributeTests
{
    public class HealthAttributePercentage : AttributesTestsBase {

        [Test]
        public void HealthAttribute_ShouldKeepPercentageHealth_When_FlatModifier_Added()
        {
            healthAttribute.ChangeCurrentValue(-10.0f);

            float percentage = healthAttribute.CurrentPercentage;

            healthAttribute.AddModifier(healthFlatAttributeModifier);
            Assert.IsTrue(Math.Abs(percentage - healthAttribute.CurrentPercentage) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_ShouldKeepPercentageHealth_When_FlatModifier_Removed()
        {
            healthAttribute.ChangeCurrentValue(-10.0f);


            healthAttribute.AddModifier(healthFlatAttributeModifier);
            float percentage = healthAttribute.CurrentPercentage;

            healthAttribute.RemoveModifier(healthFlatAttributeModifier);

            Assert.IsTrue(Math.Abs(percentage - healthAttribute.CurrentPercentage) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_ShouldKeepPercentageHealth_When_PercentAddModifier_Added()
        {
            healthAttribute.ChangeCurrentValue(-10.0f);

            float percentage = healthAttribute.CurrentPercentage;

            healthAttribute.AddModifier(healthPercentAddAttributeModifier);
            Assert.IsTrue(Math.Abs(percentage - healthAttribute.CurrentPercentage) < FloatInnacurateComparison);
        }

        [Test]
        public void HealthAttribute_ShouldKeepPercentageHealth_When_PercenAddModifier_Removed()
        {
            healthAttribute.ChangeCurrentValue(-10.0f);


            healthAttribute.AddModifier(healthPercentAddAttributeModifier);
            float percentage = healthAttribute.CurrentPercentage;

            healthAttribute.RemoveModifier(healthPercentAddAttributeModifier);

            Assert.IsTrue(Math.Abs(percentage - healthAttribute.CurrentPercentage) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_ShouldKeepPercentageHealth_When_PercentMultModifier_Added()
        {
            healthAttribute.ChangeCurrentValue(-10.0f);

            float percentage = healthAttribute.CurrentPercentage;

            healthAttribute.AddModifier(healthPercentMultAttributeModifier);
            Assert.IsTrue(Math.Abs(percentage - healthAttribute.CurrentPercentage) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_ShouldKeepPercentageHealth_When_PercentMultModifier_Removed()
        {
            healthAttribute.ChangeCurrentValue(-10.0f);

            healthAttribute.AddModifier(healthPercentMultAttributeModifier);
            float percentage = healthAttribute.CurrentPercentage;

            healthAttribute.RemoveModifier(healthPercentMultAttributeModifier);

            Assert.IsTrue(Math.Abs(percentage - healthAttribute.CurrentPercentage) < float.Epsilon);
        }

    }
}
