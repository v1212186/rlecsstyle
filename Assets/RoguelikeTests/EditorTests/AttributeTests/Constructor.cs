﻿using System;
using NUnit.Framework;
using Roguelike.Scripts.Attributes;

namespace RoguelikeTests.EditorTests.AttributeTests
{
    public class Constructor : AttributesTestsBase {

        #region Constructor tests

        [Test]
        public void AttackAttribute_HasRightBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(attackAttribute.BaseValue - ConstructorBaseValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_HasRightBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(defenceAttribute.BaseValue - ConstructorBaseValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_HasRightBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.BaseValue - CriticalHitChanceAttribute.DefaultValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_HasRightBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(hitChanceAttribute.BaseValue - HitChanceAttribute.DefaultValue) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_HasRightBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(healthAttribute.BaseValue - ConstructorBaseValue) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_HasRightBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(energyAttribute.BaseValue - EnergyAttribute.DefaultValue) < float.Epsilon);
        }

        [Test]
        public void AttackAttribute_FinalValueEqualBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(attackAttribute.FinalValue - attackAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void DefenceAttribute_FinalValueEqualBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(defenceAttribute.FinalValue - defenceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void CriticalHitChanceAttribute_FinalValueEqualBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(criticalHitChanceAttribute.FinalValue - criticalHitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HitChanceAttribute_FinalValueEqualBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(hitChanceAttribute.FinalValue - hitChanceAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void HealthAttribute_FinalValueEqualBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(healthAttribute.FinalValue - healthAttribute.BaseValue) < float.Epsilon);
        }

        [Test]
        public void EnergyAttribute_FinalValueEqualBaseValue_FromConstructor()
        {
            Assert.IsTrue(Math.Abs(energyAttribute.FinalValue - energyAttribute.BaseValue) < float.Epsilon);
        }

        #endregion

    }
}
