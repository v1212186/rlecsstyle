﻿using NUnit.Framework;

namespace RoguelikeTests.EditorTests.AttributeTests
{
    public class SortingAttributeModifiers : AttributesTestsBase
    {

        #region Sorting multiple attribute moddifiers

        [Test]
        public void AttackAttribute_AllModifiers_CorrectlySorted()
        {
            attackAttribute.AddModifier(attackPercentMultAttributeModifier);
            attackAttribute.AddModifier(attackPercentAddAttributeModifier);
            attackAttribute.AddModifier(attackFlatAttributeModifier);

            Assert.IsTrue(attackAttribute.attributeModifiersReadOnly[0].AttributeModifierType == attackFlatAttributeModifier.AttributeModifierType &&
                          attackAttribute.attributeModifiersReadOnly[1].AttributeModifierType == attackPercentAddAttributeModifier.AttributeModifierType &&
                          attackAttribute.attributeModifiersReadOnly[2].AttributeModifierType == attackPercentMultAttributeModifier.AttributeModifierType);
        }

        [Test]
        public void DefenceAttribute_AllModifiers_CorrectlySorted()
        {
            defenceAttribute.AddModifier(defencePercentMultAttributeModifier);
            defenceAttribute.AddModifier(defencePercentAddAttributeModifier);
            defenceAttribute.AddModifier(defenceFlatAttributeModifier);

            Assert.IsTrue(defenceAttribute.attributeModifiersReadOnly[0].AttributeModifierType == defenceFlatAttributeModifier.AttributeModifierType &&
                          defenceAttribute.attributeModifiersReadOnly[1].AttributeModifierType == defencePercentAddAttributeModifier.AttributeModifierType &&
                          defenceAttribute.attributeModifiersReadOnly[2].AttributeModifierType == defencePercentMultAttributeModifier.AttributeModifierType);
        }

        [Test]
        public void CriticalHitChanceAttribute_AllModifiers_CorrectlySorted()
        {
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentMultAttributeModifier);
            criticalHitChanceAttribute.AddModifier(criticalHitChancePercentAddAttributeModifier);
            criticalHitChanceAttribute.AddModifier(criticalHitChanceFlatAttributeModifier);

            Assert.IsTrue(criticalHitChanceAttribute.attributeModifiersReadOnly[0].AttributeModifierType == criticalHitChanceFlatAttributeModifier.AttributeModifierType &&
                          criticalHitChanceAttribute.attributeModifiersReadOnly[1].AttributeModifierType == criticalHitChancePercentAddAttributeModifier.AttributeModifierType &&
                          criticalHitChanceAttribute.attributeModifiersReadOnly[2].AttributeModifierType == criticalHitChancePercentMultAttributeModifier.AttributeModifierType);
        }

        [Test]
        public void HitChanceAttribute_AllModifiers_CorrectlySorted()
        {
            hitChanceAttribute.AddModifier(hitChancePercentMultAttributeModifier);
            hitChanceAttribute.AddModifier(hitChancePercentAddAttributeModifier);
            hitChanceAttribute.AddModifier(hitChanceFlatAttributeModifier);

            Assert.IsTrue(hitChanceAttribute.attributeModifiersReadOnly[0].AttributeModifierType == hitChanceFlatAttributeModifier.AttributeModifierType &&
                          hitChanceAttribute.attributeModifiersReadOnly[1].AttributeModifierType == hitChancePercentAddAttributeModifier.AttributeModifierType &&
                          hitChanceAttribute.attributeModifiersReadOnly[2].AttributeModifierType == hitChancePercentMultAttributeModifier.AttributeModifierType);
        }

        [Test]
        public void HealthAttribute_AllModifiers_CorrectlySorted()
        {
            healthAttribute.AddModifier(healthPercentMultAttributeModifier);
            healthAttribute.AddModifier(healthPercentAddAttributeModifier);
            healthAttribute.AddModifier(healthFlatAttributeModifier);

            Assert.IsTrue(healthAttribute.attributeModifiersReadOnly[0].AttributeModifierType == healthFlatAttributeModifier.AttributeModifierType &&
                          healthAttribute.attributeModifiersReadOnly[1].AttributeModifierType == healthPercentAddAttributeModifier.AttributeModifierType &&
                          healthAttribute.attributeModifiersReadOnly[2].AttributeModifierType == healthPercentMultAttributeModifier.AttributeModifierType);
        }

        [Test]
        public void EnergyAttribute_AllModifiers_CorrectlySorted()
        {
            energyAttribute.AddModifier(energyPercentMultAttributeModifier);
            energyAttribute.AddModifier(energyPercentAddAttributeModifier);
            energyAttribute.AddModifier(energyFlatAttributeModifier);

            Assert.IsTrue(energyAttribute.attributeModifiersReadOnly[0].AttributeModifierType == energyFlatAttributeModifier.AttributeModifierType &&
                          energyAttribute.attributeModifiersReadOnly[1].AttributeModifierType == energyPercentAddAttributeModifier.AttributeModifierType &&
                          energyAttribute.attributeModifiersReadOnly[2].AttributeModifierType == energyPercentMultAttributeModifier.AttributeModifierType);
        }

        #endregion

    }
}
