﻿using NUnit.Framework;
using Roguelike.Scripts.Attributes;

namespace RoguelikeTests.EditorTests.AttributeTests
{
    public class MismatchingAttributeModifierException : AttributesTestsBase {

        #region Adding mismatching attribute modifier

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void AttackAttribute_Add_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.Attack)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => attackAttribute.AddModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void DefenceAttribute_Add_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.Defence)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => defenceAttribute.AddModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void CriticalHitChanceAttribute_Add_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.CriticalHit)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => criticalHitChanceAttribute.AddModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void HitChanceAttribute_Add_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.HitChance)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => hitChanceAttribute.AddModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void HealthAttribute_Add_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.Health)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => healthAttribute.AddModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void EnergyAttribute_Add_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.Energy)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => energyAttribute.AddModifier(_attributeModifier));
        }

        #endregion

        #region Removing mismatching attribute modifier

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void AttackAttribute_Remove_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.Attack)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => attackAttribute.RemoveModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void DefenceAttribute_Remove_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.Defence)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => defenceAttribute.RemoveModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void CriticalHitChanceAttribute_Remove_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.CriticalHit)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => criticalHitChanceAttribute.RemoveModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void HitChanceAttribute_Remove_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.HitChance)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => hitChanceAttribute.RemoveModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void HealthAttribute_Remove_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.Health)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => healthAttribute.RemoveModifier(_attributeModifier));
        }

        [TestCaseSource(nameof(FlatAttributeModifiers))]
        public void EnergyAttribute_Remove_MismatchingAttributeModifier_Exception(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType == AttributeType.Energy)
            {
                return;
            }
            Assert.Catch<MismatchingAttributeTypeException>(() => energyAttribute.RemoveModifier(_attributeModifier));
        }

        #endregion
    }
}
