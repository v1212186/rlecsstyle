﻿using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Levels.LevelGenerators
{
    public abstract class AbstractLevelGenerator : ScriptableObject
    {
        #region Methods

        public abstract Level GenerateLevel();

        #endregion
    }
}
