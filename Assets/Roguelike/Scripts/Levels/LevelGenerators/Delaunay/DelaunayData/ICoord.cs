using UnityEngine;

namespace Roguelike.Scripts.Levels.LevelGenerators.Delaunay.DelaunayData
{
	
	public interface ICoord
	{
		Vector2 Coord {
			get;
		}
	}
}