﻿using UnityEngine;

namespace Roguelike.Scripts.Levels.LevelGenerators.Delaunay
{
    public class LevelGeneratorHeader : MonoBehaviour
    {
        public static LevelGeneratorHeader singleton;
        
        void Awake()
        {
            singleton = this;
        }

    }
}
