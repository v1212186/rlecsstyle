namespace Roguelike.Scripts.Levels.LevelGenerators.Delaunay.Geometry
{
    public enum Winding
    {
        NONE = 0, CLOCKWISE, COUNTERCLOCKWISE
    }
}