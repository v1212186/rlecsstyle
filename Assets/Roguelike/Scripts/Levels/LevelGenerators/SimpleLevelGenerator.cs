﻿using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Levels.LevelGenerators
{
    public class SimpleLevelGenerator : AbstractLevelGenerator
    {

        #region Variables

        [SerializeField]
        private Vector2Int dimensions;

        #endregion

        #region Methods

        public override Level GenerateLevel()
        {
            Level level = new Level(dimensions);

            for (int i = 1; i < dimensions.x - 1; i++)
            {
                for (int j = 1; j < dimensions.y - 1; j++)
                {
                    level.SetCellAtPosition(new Cell(new CellProperties(CellType.Floor, false, false)), new Vector2Int(i, j));
                }
            }

            for (int i = 0; i < dimensions.x; i++)
            {
                level.SetCellAtPosition(new Cell(new CellProperties(CellType.Wall, true, true)), new Vector2Int(i, 0));
                level.SetCellAtPosition(new Cell(new CellProperties(CellType.Wall, true, true)), new Vector2Int(i, dimensions.y - 1));
            }

            for (int i = 0; i < dimensions.y; i++)
            {
                level.SetCellAtPosition(new Cell(new CellProperties(CellType.Wall, true, true)), new Vector2Int(0, i));
                level.SetCellAtPosition(new Cell(new CellProperties(CellType.Wall, true, true)), new Vector2Int(dimensions.x - 1, i));
            }

            level.SetCellAtPosition(new Cell(new CellProperties(CellType.Wall, true, true)), new Vector2Int(5, 4));

            return level;
        }

        #endregion
    }
}
