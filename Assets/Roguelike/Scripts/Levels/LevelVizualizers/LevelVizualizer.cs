﻿using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Roguelike.Scripts.Levels.LevelVizualizers
{
    public class LevelVizualizer {

        #region Constructor

        public LevelVizualizer()
        {

        }

        #endregion


        #region Methods

        //TODO получить ссылку на тайлмапу в конструкторе
        public void VisualizeLevel(Level _level)
        {
            TileSet tileSet = World.Instance.ResourcesManager.GetTileSet(_level.TileSetName);

            for (int i = 0; i < _level.Dimensions.x; i++)
            {
                for (int j = 0; j < _level.Dimensions.y; j++)
                {
                    TileBase tile = null;
                    if (_level.GetCellProperties(new Vector2Int(i, j)).CellType == CellType.Floor)
                    {
                        tile = tileSet.FloorTile;
                    }
                    if (_level.GetCellProperties(new Vector2Int(i, j)).CellType == CellType.Wall)
                    {
                        tile = tileSet.WallTile;
                    }
                    World.Instance.ResourcesManager.GameGridStructure.Tilemap.SetTile(new Vector3Int(i, j, 0), tile);
                }
            }
        }

        public void ClearVizualization()
        {
            World.Instance.ResourcesManager.GameGridStructure.Tilemap.ClearAllTiles();
        }

        #endregion
    }
}
