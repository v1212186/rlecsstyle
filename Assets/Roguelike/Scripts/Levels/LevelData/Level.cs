﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.Levels.LevelData
{
    public class Level : IJsonSerializable
    {

        #region Variables

        private TileSet.TileSetName tileSetName;
        public TileSet.TileSetName TileSetName => tileSetName;

        private Cell[,] cells;

        private Vector2Int dimensions;
        public Vector2Int Dimensions => dimensions;

        #endregion

        #region Constructor

        public Level(JObject _jObject)
        {
            ((IJsonSerializable)this).FromJson(_jObject);
        }

        public Level(TileSet.TileSetName _tileSetName, Vector2Int _dimensions)
        {
            tileSetName = _tileSetName;
            dimensions = _dimensions;
            cells = new Cell[dimensions.x, dimensions.y];
        }

        public Level(Vector2Int _dimensions) : this(TileSet.TileSetName.Default, _dimensions)
        {

        }

        #endregion

        #region Methods

        public bool IsWithinBounds(Vector2Int _position)
        {
            return _position.x >= 0 && _position.y >= 0 & _position.x < dimensions.x &&
                   _position.y < dimensions.y;
        }

        //TODO пока что подразумевается, что не будет null cell (хотя в реале могут возникнуть при генерации, либо пустые пространства заполнять целлами, либо еще че)
        //TODO добавлять отдельно позиции для входа и выхода на уровень, потом в генераторе спавнить по этим точкам
        private Cell GetCellAtPosition(Vector2Int _position)
        {
            if (!IsWithinBounds(_position))
            {
                Exception exception = new Exceptions.CellOutOfBoundsException("Tried to get cell at " + _position);
                exception.Data.Add("Position", _position);
                exception.Data.Add("Cells", cells);
                throw exception;
            }

            Cell cell = cells[_position.x, _position.y];
            if (cell == null)
            {
                Exception exception = new NullReferenceException();
                exception.Data.Add("Position", _position);
                exception.Data.Add("Cells", cells);
                throw exception;
            }
            return cell;
        }

        public void SetCellAtPosition(Cell _cell, Vector2Int _position)
        {
            if (!IsWithinBounds(_position))
            {
                Exception exception = new Exceptions.CellOutOfBoundsException("Tried to set cell at " + _position);
                throw exception;
            }
            cells[_position.x, _position.y] = _cell;
        }

        public void CastLight(Vector2Int _position)
        {
            CellProperties cellProperties = GetCellProperties(_position);
            cellProperties.IsDiscovered = true;
            cellProperties.IsObserved = true;
        }

        public void CastShadow(Vector2Int _position)
        {
            CellProperties cellProperties = GetCellProperties(_position);
            cellProperties.IsObserved = false;
        }

        public bool RegisterEntity(Vector2Int _position, Entity _entity)
        {
            return GetCellAtPosition(_position).RegisterEntity(_entity);
        }

        public bool UnregisterEntity(Vector2Int _position, Entity _entity)
        {
            return GetCellAtPosition(_position).UnregisterEntity(_entity);
        }

        public CellProperties GetCellProperties(Vector2Int _position)
        {
            return GetCellAtPosition(_position).CellProperties;
        }

        public bool HasMatchingEntity(Vector2Int _position, ComponentsFilter _componentsFilter)
        {
            return GetCellAtPosition(_position).HasMatchingEntity(_componentsFilter);
        }

        public Entity GetMatchingEntity(Vector2Int _position, ComponentsFilter _componentsFilter)
        {
            return GetCellAtPosition(_position).GetMatchingEntity(_componentsFilter);
        }

        public void SetTilesetName(TileSet.TileSetName _tileSetName)
        {
            tileSetName = _tileSetName;
        }

        public JObject ToJson()
        {
            JArray cellsJArray = new JArray();
            for (int i = 0; i < cells.GetLength(0); i++)
            {
                for (int j = 0; j < cells.GetLength(1); j++)
                {
                    JObject cellJObject = new JObject();
                    cellJObject["CellType"] = JToken.FromObject(cells[i, j].CellProperties.CellType);
                    cellJObject["IsCollidable"] = cells[i, j].CellProperties.IsCollidable;
                    cellJObject["IsBlockingView"] = cells[i, j].CellProperties.IsBlockingView;
                    cellJObject["IsDiscovered"] = cells[i, j].CellProperties.IsDiscovered;
                    cellJObject["IsObserved"] = cells[i, j].CellProperties.IsObserved;
                    cellJObject["xPos"] = i;
                    cellJObject["yPos"] = j;

                    cellsJArray.Add(cellJObject);
                }
            }

            JObject jObject = new JObject
            {
                ["Cells"] = cellsJArray,
                ["DimensionX"] = dimensions.x,
                ["DimensionY"] = dimensions.y,
                ["TileSet"] = JToken.FromObject(tileSetName)
            };
            return jObject;
        }

        void IJsonSerializable.FromJson(JObject _jsonObject)
        {
            dimensions.x = _jsonObject["DimensionX"].ToObject<int>();
            dimensions.y = _jsonObject["DimensionY"].ToObject<int>();
            tileSetName = _jsonObject["TileSet"].ToObject<TileSet.TileSetName>();
            cells = new Cell[dimensions.x, dimensions.y];

            JArray jArray = _jsonObject["Cells"].ToObject<JArray>();
            foreach (JToken jToken in jArray)
            {
                cells[jToken["xPos"].ToObject<int>(), jToken["yPos"].ToObject<int>()] =
                    new Cell(new CellProperties(jToken["CellType"].ToObject<CellType>(), jToken["IsCollidable"].ToObject<bool>(),
                        jToken["IsBlockingView"].ToObject<bool>(), jToken["IsDiscovered"].ToObject<bool>(),
                        jToken["IsObserved"].ToObject<bool>()));
            }
        }

        #endregion

    }
}
