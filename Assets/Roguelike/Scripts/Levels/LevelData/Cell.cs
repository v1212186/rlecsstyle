﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Ecs.Core;

namespace Roguelike.Scripts.Levels.LevelData
{

    public enum CellType
    {
        Floor = 0, Wall = 1
    }

    public class CellProperties
    {
        #region Variables

        public readonly CellType CellType;
        public readonly bool IsCollidable;
        public readonly bool IsBlockingView;
        public bool IsDiscovered { get; set; }
        public bool IsObserved { get; set; }

        #endregion

        #region Constructor

        public CellProperties(CellType _cellType, bool _isCollidable, bool _isBlockingView, bool _isDiscovered = false, bool _isObserved = false)
        {
            CellType = _cellType;
            IsCollidable = _isCollidable;
            IsBlockingView = _isBlockingView;
            IsDiscovered = _isDiscovered;
            IsObserved = _isObserved;
        }

        #endregion

    }

    //TODO добавить фильтр для определения какие энтити можно регать?
    public class Cell
    {

        #region Variables

        public readonly CellProperties CellProperties;

        private readonly List<Entity> entities;
        public readonly ReadOnlyCollection<Entity> Entities;

        #endregion

        #region Constructor

        public Cell(CellProperties _cellProperties)
        {
            CellProperties = _cellProperties;
            entities = new List<Entity>(1);
            Entities = entities.AsReadOnly();
        }

        #endregion

        #region Methods

        //TODO сейчас можно больше одного энтити на клетке, переделать чтобы нельзя было больше одного Creature или больше одного Collidable, в идеале заюзать фильтр
        public bool RegisterEntity(Entity _entity)
        {
            if (IsEntityRegistered(_entity))
            {
                Exception exception = new CantRegisterEntityException("Tried to register already registered entity at cell");
                exception.Data.Add("Entity", _entity);
                exception.Data.Add("Cell", this);
                throw exception;
            }

            if (CellProperties.IsCollidable)
            {
                Exception exception = new CantRegisterEntityException("Tried to register entity at collidable cell");
                exception.Data.Add("Entity", _entity);
                exception.Data.Add("Cell", this);
                throw exception;
            }

            entities.Add(_entity);
            return true;
        }

        public bool UnregisterEntity(Entity _entity)
        {
            if (!IsEntityRegistered(_entity))
            {
                Exception exception = new CantUnregisterEntityException("Tried to unregisted not registered entity at cell");
                exception.Data.Add("Entity", _entity);
                exception.Data.Add("Cell", this);
                throw exception;
            }
            return entities.Remove(_entity);
        }

        public bool IsEntityRegistered(Entity _entity)
        {
            return entities.Contains(_entity);
        }

        public bool HasMatchingEntity(ComponentsFilter _componentsFilter)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (_componentsFilter.IsEntityMatches(entities[i]))
                {
                    return true;
                }
            }
            return false;
        }

        public Entity GetMatchingEntity(ComponentsFilter _componentsFilter)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (_componentsFilter.IsEntityMatches(entities[i]))
                {
                    return entities[i];
                }
            }

            return null;
        }

        #endregion

    }
}
