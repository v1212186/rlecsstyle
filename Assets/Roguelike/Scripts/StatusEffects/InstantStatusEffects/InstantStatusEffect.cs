﻿using Ecs.Core;
using Roguelike.Scripts.Ecs.Managers;

namespace Roguelike.Scripts.StatusEffects.InstantStatusEffects
{
    public abstract class InstantStatusEffect : AbstractStatusEffect {

        #region Constructors

        protected InstantStatusEffect()
        {

        }

        protected InstantStatusEffect(SpritesCollection.SpriteId _spriteId) : base(_spriteId)
        {

        }

        protected InstantStatusEffect(SpritesCollection.SpriteId _spriteId, FastGuid _sourceGuid) : base(_spriteId, _sourceGuid)
        {

        }

        #endregion

        #region Methods

        public override bool RemoveStatusEffect(Entity _entity)
        {
            return false;
        }

        #endregion


    }
}
