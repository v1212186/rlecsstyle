﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.CombatSystem;
using Roguelike.Scripts.Ecs.Managers;
namespace Roguelike.Scripts.StatusEffects.InstantStatusEffects
{
    public class ReceiveDamageStatusEffect : InstantStatusEffect
    {

        #region Variables

        private float damageAmount;

        #endregion

        #region Properties

        public float DamageAmount => damageAmount;

        #endregion

        #region Constructors

        protected ReceiveDamageStatusEffect() : base()
        {
        }

        public ReceiveDamageStatusEffect(SpritesCollection.SpriteId _spriteId, float _damageAmount) : base(_spriteId)
        {
            damageAmount = _damageAmount;
        }

        public ReceiveDamageStatusEffect(SpritesCollection.SpriteId _spriteId, float _damageAmount, FastGuid _sourceGuid) : base(_spriteId, _sourceGuid)
        {
            damageAmount = _damageAmount;
        }

        #endregion

        #region Methods

        public override bool ApplyStatusEffect(Entity _entity)
        {  
            Combat.DealDamage(_entity, damageAmount);
            return true;
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();
            jObject["Type"] = JToken.FromObject(GetType());
            jObject["damageAmount"] = damageAmount;
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            base.OnFromJson(_jsonObject);
            damageAmount = _jsonObject["damageAmount"].ToObject<float>();
        }

        #endregion

    }
}
