﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.CombatSystem;
using Roguelike.Scripts.Ecs.Managers;
namespace Roguelike.Scripts.StatusEffects.InstantStatusEffects
{
    public class ReceiveHealStatusEffect : InstantStatusEffect
    {
        #region Variables

        private float healAmount;

        #endregion

        #region Properties

        public float HealAmount => healAmount;

        #endregion

        #region Constructors

        protected ReceiveHealStatusEffect() : base()
        {
        }

        public ReceiveHealStatusEffect(SpritesCollection.SpriteId _spriteId, float _healAmount) : base(_spriteId)
        {
            healAmount = _healAmount;
        }

        public ReceiveHealStatusEffect(SpritesCollection.SpriteId _spriteId, float _healAmount, FastGuid _sourceGuid) : base(_spriteId, _sourceGuid)
        {
            healAmount = _healAmount;
        }

        #endregion

        #region Methods

        public override bool ApplyStatusEffect(Entity _entity)
        {
            Combat.ReceiveHeal(_entity, healAmount);
            return true;
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();
            jObject["Type"] = JToken.FromObject(GetType());
            jObject["healAmount"] = healAmount;
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            base.OnFromJson(_jsonObject);
            healAmount = _jsonObject["healAmount"].ToObject<float>();
        }
        #endregion

    }
}
