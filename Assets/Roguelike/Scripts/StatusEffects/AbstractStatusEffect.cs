﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Managers;

namespace Roguelike.Scripts.StatusEffects
{
    //TODO добавить обновление времени эффекта при наложении такого же эффекта
    public abstract class AbstractStatusEffect : IJsonSerializable, IEquatable<AbstractStatusEffect>
    {
        #region Variables

        protected SpritesCollection.SpriteId spriteId;
        protected FastGuid guid;
        protected FastGuid? sourceGuid;

        #endregion

        #region Properties

        public FastGuid Guid => guid;
        public FastGuid? SourceGuid => sourceGuid;

        #endregion

        #region Constructor

        protected AbstractStatusEffect()
        {

        }

        protected AbstractStatusEffect(SpritesCollection.SpriteId _spriteId, FastGuid? _sourceGuid)
        {
            spriteId = _spriteId;
            guid = FastGuid.NewGuid();
            sourceGuid = _sourceGuid;
        }

        protected AbstractStatusEffect(SpritesCollection.SpriteId _spriteId)
            : this(_spriteId, null)
        {

        }

        #endregion

        #region Methods

        public abstract bool ApplyStatusEffect(Entity _entity);

        public abstract bool RemoveStatusEffect(Entity _entity);

        //TODO добавить фильтр для определения, является ли таргет энтити подходящей целью
        //public abstract bool CanApplyToEntity(Entity _entity);

        public JObject ToJson()
        {
            return OnToJson();
        }

        public void FromJson(JObject _jsonObject)
        {
            OnFromJson(_jsonObject);
        }

        protected virtual JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["spriteId"] = spriteId.Id,
                ["guid"] = guid.GuidToJson(),
                ["sourceGuid"] = sourceGuid?.GuidToJson()
            };
            return jObject;
        }

        protected virtual void OnFromJson(JObject _jsonObject)
        {
            FastGuid? guidJObject = EcsUtilities.GuidFromJson(_jsonObject["guid"].ToObject<JObject>());
            if (guidJObject.HasValue)
            {
                guid = guidJObject.Value;
            }
            sourceGuid = EcsUtilities.GuidFromJson(_jsonObject["sourceGuid"].ToObject<JObject>());
            spriteId = World.Instance.ResourcesManager.ScrollsSprites.GetSprite(_jsonObject["spriteId"]
                .ToObject<int>());
        }

        public bool Equals(AbstractStatusEffect _other)
        {
            if (ReferenceEquals(null, _other))
            {
                return false;
            }

            if (ReferenceEquals(this, _other))
            {
                return true;
            }

            return guid.Equals(_other.guid);
        }

        public override bool Equals(object _obj)
        {
            if (ReferenceEquals(null, _obj))
            {
                return false;
            }

            if (ReferenceEquals(this, _obj))
            {
                return true;
            }

            if (_obj.GetType() != this.GetType())
            {
                return false;
            }
            return Equals((AbstractStatusEffect)_obj);
        }

        public override int GetHashCode()
        {
            return guid.GetHashCode();
        }

        #endregion

    }
}
