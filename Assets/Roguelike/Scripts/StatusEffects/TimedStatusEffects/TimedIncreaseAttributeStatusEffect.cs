﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Attributes;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.Ecs.Managers;

namespace Roguelike.Scripts.StatusEffects.TimedStatusEffects
{
    public class TimedIncreaseAttributeStatusEffect : TimedStatusEffect
    {

        #region Variables

        private AttributeModifier attributeModifier;
        public AttributeModifier AttributeModifier => attributeModifier;

        #endregion

        #region Constructor

        private TimedIncreaseAttributeStatusEffect() : base()
        {

        }

        public TimedIncreaseAttributeStatusEffect(SpritesCollection.SpriteId _spriteId, int _duration, AttributeModifier _attributeModifier, FastGuid _sourceGuid)
            : base(_spriteId, _duration, _sourceGuid)
        {
            attributeModifier = _attributeModifier;
        }

        public TimedIncreaseAttributeStatusEffect(SpritesCollection.SpriteId _spriteId, int _duration, AttributeModifier _attributeModifier)
            : base(_spriteId, _duration)
        {
            attributeModifier = _attributeModifier;
        }

        #endregion

        #region Methods


        public override bool ApplyStatusEffect(Entity _entity)
        {
            attributeModifier.SetSourceGuid(guid);
            return AttributeComponentsHelper.ApplyModifierToAttributeComponent(_entity, attributeModifier);
        }

        public override bool RemoveStatusEffect(Entity _entity)
        {
            return AttributeComponentsHelper.RemoveModifierFromAttributeComponent(_entity, attributeModifier);
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();
            jObject["Type"] = JToken.FromObject(GetType());
            jObject["attributeModifier"] = attributeModifier.ToJson();
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            base.OnFromJson(_jsonObject);
            attributeModifier = new AttributeModifier(_jsonObject["attributeModifier"].ToObject<JObject>());
        }

        #endregion
    }
}
