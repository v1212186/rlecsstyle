﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.StatusEffects.TimedStatusEffects
{
    public abstract class TimedStatusEffect : AbstractStatusEffect
    {

        #region Variables

        protected const float MinDuration = 0.0f;

        protected int duration;
        protected int currentDuration;

        #endregion

        #region Properties

        public int Duration => duration;
        public int CurrentDuration => currentDuration;

        #endregion

        #region Constructors

        protected TimedStatusEffect() : base()
        {

        }

        protected TimedStatusEffect(SpritesCollection.SpriteId _spriteId, int _duration, FastGuid _sourceGuid) : base(_spriteId, _sourceGuid)
        {
            duration = _duration;
            currentDuration = duration;
        }

        protected TimedStatusEffect(SpritesCollection.SpriteId _spriteId, int _duration) : base(_spriteId)
        {
            duration = _duration;
            currentDuration = duration;
        }

        #endregion

        #region Methods

        public int ChangeDuration(int _value)
        {
            return currentDuration = (int)Mathf.Clamp(currentDuration + _value, MinDuration, duration);
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();

            jObject["duration"] = duration;
            jObject["currentDuration"] = currentDuration;

            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            base.OnFromJson(_jsonObject);
            duration = _jsonObject["duration"].ToObject<int>();
            currentDuration = _jsonObject["currentDuration"].ToObject<int>();
        }

        #endregion

    }
}
