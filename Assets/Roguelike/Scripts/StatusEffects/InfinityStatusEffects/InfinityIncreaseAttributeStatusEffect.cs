﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Attributes;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.Ecs.Managers;

namespace Roguelike.Scripts.StatusEffects.InfinityStatusEffects
{
    public class InfinityIncreaseAttributeStatusEffect : InfinityStatusEffect
    {

        #region Variables

        private AttributeModifier attributeModifier;
        public AttributeModifier AttributeModifier => attributeModifier;

        #endregion

        #region Constructors

        protected InfinityIncreaseAttributeStatusEffect() : base()
        {
        }

        public InfinityIncreaseAttributeStatusEffect(SpritesCollection.SpriteId _spriteId, AttributeModifier _attributeModifier)
            : base(_spriteId)
        {
            attributeModifier = _attributeModifier;
            attributeModifier.SetSourceGuid(sourceGuid);
        }

        public InfinityIncreaseAttributeStatusEffect(SpritesCollection.SpriteId _spriteId, AttributeModifier _attributeModifier, FastGuid _sourceGuid)
            : base(_spriteId, _sourceGuid)
        {
            attributeModifier = _attributeModifier;
            attributeModifier.SetSourceGuid(sourceGuid);
        }

        #endregion

        #region Methods


        public override bool ApplyStatusEffect(Entity _entity)
        {
            return AttributeComponentsHelper.ApplyModifierToAttributeComponent(_entity, attributeModifier);
        }

        public override bool RemoveStatusEffect(Entity _entity)
        {
            return AttributeComponentsHelper.RemoveModifierFromAttributeComponent(_entity, attributeModifier);
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();
            jObject["Type"] = JToken.FromObject(GetType());
            jObject["attributeModifier"] = attributeModifier.ToJson();
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            base.OnFromJson(_jsonObject);
            attributeModifier = new AttributeModifier(_jsonObject["attributeModifier"].ToObject<JObject>());
        }

        #endregion

    }
}
