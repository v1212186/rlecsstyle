﻿using System;
using Ecs.Core;
using Roguelike.Scripts.Ecs.Managers;

namespace Roguelike.Scripts.StatusEffects.InfinityStatusEffects
{
    public abstract class InfinityStatusEffect : AbstractStatusEffect
    {

        #region Constructors

        protected InfinityStatusEffect()
        {

        }

        protected InfinityStatusEffect(SpritesCollection.SpriteId _spriteId) : base(_spriteId)
        {

        }

        protected InfinityStatusEffect(SpritesCollection.SpriteId _spriteId, FastGuid _sourceGuid) : base(_spriteId, _sourceGuid)
        {

        }

        #endregion

    }
}
