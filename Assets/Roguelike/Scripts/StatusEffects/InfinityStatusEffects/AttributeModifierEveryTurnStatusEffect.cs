﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Attributes;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.StatusEffects.InfinityStatusEffects
{
    public class AttributeModifierEveryTurnStatusEffect : InfinityStatusEffect
    {

        #region Variables

        private Entity Entity => World.Instance.EntityManager.GetEntityByGuid(entityGuid);
        private FastGuid entityGuid;
        private Group turnGroup;
        private AttributeModifier attributeModifier;
        private int currentTurn;
        private int maximumTurns;

        #endregion

        #region Properties

        #endregion

        #region Constructors

        protected AttributeModifierEveryTurnStatusEffect()
        {
        }

        public AttributeModifierEveryTurnStatusEffect(SpritesCollection.SpriteId _spriteId, AttributeModifier _attributeModifier, int _maximumTurns)
            : base(_spriteId)
        {
            attributeModifier = _attributeModifier;
            attributeModifier.SetSourceGuid(sourceGuid);
            maximumTurns = _maximumTurns;
            currentTurn = 0;
        }

        public AttributeModifierEveryTurnStatusEffect(SpritesCollection.SpriteId _spriteId, AttributeModifier _attributeModifier, int _maximumTurns, FastGuid _sourceGuid)
            : base(_spriteId, _sourceGuid)
        {
            attributeModifier = _attributeModifier;
            attributeModifier.SetSourceGuid(sourceGuid);
            maximumTurns = _maximumTurns;
            currentTurn = 0;
        }

        #endregion

        #region Methods

        public override bool ApplyStatusEffect(Entity _entity)
        {
            Initialize(_entity.Guid);
            return true;
        }

        public override bool RemoveStatusEffect(Entity _entity)
        {
            UnsubscribeFromTurnProcessing();
            return true;
        }

        private void Initialize(FastGuid _entityGuid)
        {
            entityGuid = _entityGuid;
            SubscribeToTurnProcessing();
        }

        private void SubscribeToTurnProcessing()
        {
            turnGroup =
                World.Instance.EntityManager.GetOrCreateGroup(
                    new Group(new ComponentsFilter().AllOf(typeof(TurnFlag))));
            turnGroup.GroupEntityAddedEvent.Add(OnTurnGroupEntityAdded);
            turnGroup.GroupEntityRemovedEvent.Add(OnTurnGroupEntityRemoved);
        }

        private void UnsubscribeFromTurnProcessing()
        {
            turnGroup.GroupEntityAddedEvent.Remove(OnTurnGroupEntityAdded);
            turnGroup.GroupEntityRemovedEvent.Remove(OnTurnGroupEntityRemoved);
        }

        private void OnTurnGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            if (Entity.Equals(_entityEventArgs.Entity))
            {
                currentTurn++;
                if (currentTurn % maximumTurns == 0)
                {
                    Debug.Log("Applying");
                    currentTurn = 0;
                    AttributeComponentsHelper.ApplyModifierToAttributeComponent(Entity, attributeModifier);
                }
                else
                {
                    AttributeComponentsHelper.RemoveModifierFromAttributeComponent(Entity, attributeModifier);
                }
            }
        }

        private void OnTurnGroupEntityRemoved(Group _group, EntityEventArgs _entityEventArgs)
        {
            if (Entity.Equals(_entityEventArgs.Entity))
            {

            }
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();
            jObject["Type"] = JToken.FromObject(GetType());
            jObject["attributeModifier"] = attributeModifier.ToJson();
            jObject["currentTurn"] = currentTurn;
            jObject["maximumTurns"] = maximumTurns;
            jObject["entityGuid"] = entityGuid.GuidToJson();
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            base.OnFromJson(_jsonObject);
            attributeModifier = new AttributeModifier(_jsonObject["attributeModifier"].ToObject<JObject>());
            currentTurn = _jsonObject["currentTurn"].ToObject<int>();
            maximumTurns = _jsonObject["maximumTurns"].ToObject<int>();
            entityGuid = entityGuid.GuidFromJson(_jsonObject["entityGuid"].ToObject<JObject>());
            Initialize(entityGuid);
        }

        #endregion


    }
}
