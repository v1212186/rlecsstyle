﻿using System.Collections.Generic;
using UnityEngine;

namespace Roguelike.Scripts.Utilities
{
    public static class Extensions
    {
        public static Vector2 GetVector2(this Vector2Int _vector2Int)
        {
            return new Vector2(_vector2Int.x, _vector2Int.y);
        }

        public static Vector2Int GetVector2Int(this Vector2 _vector2)
        {
            return new Vector2Int((int)_vector2.x, (int)_vector2.y);
        }

        public static Vector2Int SwapValues(this Vector2Int _vector2Int)
        {
            int temp = _vector2Int.x;
            _vector2Int.x = _vector2Int.y;
            _vector2Int.y = temp;
            return _vector2Int;
        }

        public static void ShuffleList<T>(this IList<T> _list)
        {
            int n = _list.Count;
            while (n > 1)
            {
                n--;
                int k = Random.Range(0, n + 1);
                T value = _list[k];
                _list[k] = _list[n];
                _list[n] = value;
            }
        }
    }
}