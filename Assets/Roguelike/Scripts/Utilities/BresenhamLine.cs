﻿using System.Collections.Generic;
using UnityEngine;

namespace Roguelike.Scripts.Utilities
{
    public static class BresenhamLine
    {

        #region Methods

        public static List<Vector2Int> CreateLine(Vector2Int _start, Vector2Int _end)
        {
            bool steep = Mathf.Abs(_end.y - _start.y) > Mathf.Abs(_end.x - _start.x);

            if (steep)
            {
                int temp = _start.x;
                _start.x = _start.y;
                _start.y = temp;

                temp = _end.x;
                _end.x = _end.y;
                _end.y = temp;
            }

            bool reverse = _start.x > _end.x;

            if (reverse)
            {
                int temp = _start.x;
                _start.x = _end.x;
                _end.x = temp;

                temp = _start.y;
                _start.y = _end.y;
                _end.y = temp;
            }

            int dX = (_end.x - _start.x),
                dY = Mathf.Abs(_end.y - _start.y),
                err = (dX / 2),
                ystep = (_start.y < _end.y ? 1 : -1),
                y = _start.y;

            List<Vector2Int> line = new List<Vector2Int>();

            //Line.Clear();
            for (int x = _start.x; x <= _end.x; ++x)
            {
                line.Add(steep ? new Vector2Int(y, x) : new Vector2Int(x, y));
                err = err - dY;
                if (err < 0) { y += ystep; err += dX; }
            }

            if (!steep && reverse || steep && reverse)
            {
                line.Reverse();
            }

            return line;
        }

        #endregion
    }
}
