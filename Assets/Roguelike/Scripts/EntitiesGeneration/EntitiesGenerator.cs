﻿using System.Collections.Generic;
using Ecs.Core;
using Roguelike.Scripts.Actions.DirectionalActions;
using Roguelike.Scripts.Attributes;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.Enums;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.EntitiesGeneration
{
    public class EntitiesGenerator
    {

        #region Variables

        private const float ViewDistance = 5.5f;

        private readonly EntityManager entityManager;

        private readonly ResourcesManager resourcesManager;

        #endregion

        #region Properties

        #endregion

        #region Constructors

        public EntitiesGenerator(EntityManager _entityManager, ResourcesManager _resourcesManager)
        {
            entityManager = _entityManager;
            resourcesManager = _resourcesManager;
        }

        #endregion

        #region Methods

        public List<Entity> GenerateEntitesForLevel(Level _level)
        {
            List<Entity> entities = new List<Entity>();

            for (int i = 2; i < 4; i++)
            {
                var entity = CreateNpcEntity(Vector2Int.up * i + Vector2Int.right);
                entities.Add(entity);
            }

            return entities;
        }

        //TODO разбить компоненты на логические группы (геймобжект, актрабл и прочее)
        private Entity CreateCreatureEntity(Vector2Int _pos)
        {
            Entity entity = entityManager.CreateEntity();
            entityManager.AddComponent(entity, new GameObjectComponent(GameGridStructure.GridObjectLayer.Actor, _pos));
            entityManager.AddComponent(entity, new RendererComponent(SortingLayers.Actors));
            entityManager.AddComponent(entity, new GridPositionComponent(_pos));
            entityManager.AddComponent(entity, new HeadingComponent(Vector2Int.left));

            entityManager.AddComponent(entity, new CustomAnimationComponent(resourcesManager.HumanoidAnimations.GetHumanoidAnimationSprite()));

            entityManager.AddComponent(entity, new IsCollidableFlag());

            entityManager.AddComponent(entity,
                new EnergyAttributeComponent(new EnergyAttribute()));

            entityManager.AddComponent(entity, new AbilitiesSetComponent());
            AbilitiesSetComponent abilitiesSetComponent =
                entity.GetEntityComponent<AbilitiesSetComponent>();
            abilitiesSetComponent.MovementAction = new MoveEntityAction();
            abilitiesSetComponent.AttackAction = new AttackEntityAction(AnimationCurves.AnimationCurveTypes.Attack);
            abilitiesSetComponent.DefaultAction = abilitiesSetComponent.MovementAction;
            abilitiesSetComponent.ResetToDefaultAction();

            entityManager.AddComponent(entity, new HealthAttributeComponent(new HealthAttribute(100.0f)));

            CombatAttributesComponent combatAttributesComponent = new CombatAttributesComponent(new AttackAttribute(25.0f), new DefenceAttribute(20.0f),
                new CriticalHitChanceAttribute(), new HitChanceAttribute());
            entityManager.AddComponent(entity, combatAttributesComponent);

            entityManager.AddComponent(entity, new StatusEffectsComponent()); 

            return entity;
        }

        public Entity CreateNpcEntity(Vector2Int _pos)
        {
            Entity entity = CreateCreatureEntity(_pos);
            entityManager.AddComponent(entity, new NpcActorComponent());
            entityManager.AddComponent(entity, new ShadowMapVisibilityComponent());

            return entity;
        }

        public Entity CreatePlayerEntity(Vector2Int _pos)
        {
            Entity entity = CreateCreatureEntity(_pos);
            entityManager.AddComponent(entity, new PlayerActorComponent());
            entityManager.AddComponent(entity, new PlayerFlag());
            entityManager.AddComponent(entity, new ShadowMapProviderComponent(ViewDistance));

            return entity;
        }

        #endregion

    }
}
