﻿using UnityEngine;
using UnityEngine.UI;

namespace Roguelike.Scripts.WorldUserInterface.HealthAndEnergyBar
{
    public class WorldHealthBarUi : MonoBehaviour {

        #region Variables

        [SerializeField]
        private Image healthBarImage;

        public float CurrentValue => healthBarImage.fillAmount;

        #endregion

        #region Methods

        public void SetHealthBarValue(float _value)
        {
            healthBarImage.fillAmount = Mathf.Clamp01(_value);
        }

        public void SetActive(bool _isActive)
        {
            if (healthBarImage.enabled == _isActive)
            {
                return;
            }

            healthBarImage.enabled = _isActive;
        }

        #endregion

    }
}
