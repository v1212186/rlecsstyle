﻿using Roguelike.Scripts.Enums;
using UnityEngine;

namespace Roguelike.Scripts.WorldUserInterface
{
    public class TileHighlight : MonoBehaviour
    {

        #region Variables

        [SerializeField]
        private SpriteRenderer spriteRenderer;

        public static Color HighlightColor = new Color(1, 1, 1, 0.5f);
        public static Color ValidColor = new Color(0, 1, 0, 0.5f);
        public static Color InvalidColor = new Color(1, 0, 0, 0.5f);
        public static Color RadiusColor = new Color(0.5f, 0.5f, 1f, 0.2f);

        #endregion

        #region MonoBehaviour methods

        //private void Awake()
        //{
        //    spriteRenderer = GetComponent<SpriteRenderer>();
        //    spriteRenderer.sortingLayerName = SortingLayers.WorldUserInterface.ToString();
        //}

        #endregion

        #region Methods

        public void Setup()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = SortingLayers.WorldUserInterface.ToString();
        }

        public void Activate(Color _color, Vector2Int _position)
        {
            transform.position = new Vector3(_position.x, _position.y, 0.0f);
            spriteRenderer.color = _color;
            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }

        #endregion
    }
}
