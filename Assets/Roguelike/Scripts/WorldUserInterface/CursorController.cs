﻿using System;
using Roguelike.Scripts.Enums;
using UnityEngine;

namespace Roguelike.Scripts.WorldUserInterface
{
    public class CursorController : MonoBehaviour
    {

        #region Variables

        private static CursorController instance;
        public static CursorController Instance
        {
            get { return instance; }
        }

        [SerializeField]
        private Transform cursorTransform;

        [SerializeField] private SpriteRenderer spriteRenderer;

        #endregion

        #region MonoBehaviour methods

        private void Awake()
        {
            instance = this;
            cursorTransform = transform;
            spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = SortingLayers.WorldUserInterface.ToString();
        }

        #endregion

        #region Methods

        public void SetPosition(Vector2Int _position)
        {
            cursorTransform.position = new Vector3(_position.x, _position.y, 0.0f);
        }

        public void SetPosition(Vector2Int _position, Func<bool> _validityCheck)
        {
            if (_validityCheck())
            {
                cursorTransform.position = new Vector3(_position.x, _position.y, 0.0f);
            }
        }

        public Vector2Int GetPosition()
        {
            return new Vector2Int((int)cursorTransform.position.x, (int)cursorTransform.position.y);
        }

        public void ShowCursor()
        {
            if (spriteRenderer.enabled)
            {
                return;
            }
            spriteRenderer.enabled = true;
        }

        public void HideCursor()
        {
            if (!spriteRenderer.enabled)
            {
                return;
            }
            spriteRenderer.enabled = false;
        }

        #endregion


    }
}
