﻿using System.Collections.Generic;
using UnityEngine;

namespace Roguelike.Scripts.WorldUserInterface
{
    public struct ColorPosition
    {
        #region Variables

        public readonly Color Color;
        public readonly Vector2Int Position;

        #endregion

        #region Constructor

        public ColorPosition(Color _color, Vector2Int _position)
        {
            Color = _color;
            Position = _position;
        }

        #endregion
    }

    public class TileHighlightController : MonoBehaviour
    {

        #region Nested classes

        private class TileHighlightPool
        {
            #region Variables

            private readonly Stack<TileHighlight> tileHighlights = new Stack<TileHighlight>();

            #endregion

            #region Methods

            public bool HasTileHighlights()
            {
                return tileHighlights.Count > 0;
            }

            public TileHighlight GetTileHighlightFromPool()
            {
                return tileHighlights.Pop();
            }

            public void ReturnTileHighlightToPool(TileHighlight _tileHighlight)
            {
                tileHighlights.Push(_tileHighlight);
            }

            #endregion
        }

        #endregion

        #region Variables

        private static TileHighlightController instance;
        public static TileHighlightController Instance
        {
            get { return instance; }
        }

        private List<TileHighlight> tileHighlights = new List<TileHighlight>();

        private TileHighlightPool tileHighlightPool;

        [SerializeField]
        private GameObject tileHighlightPrefab;

        #endregion

        #region MonoBehaviour methods

        private void Awake()
        {
            instance = this;
            tileHighlightPool = new TileHighlightPool();
        }

        #endregion

        #region Methods

        public void CreateTileHighlightAtPositions( params ColorPosition[] _colorPositions)
        {
            for (int i = 0; i < _colorPositions.Length; i++)
            {
                TileHighlight tileHighlight;
                if (tileHighlightPool.HasTileHighlights())
                {
                    tileHighlight = tileHighlightPool.GetTileHighlightFromPool();
                }
                else
                {
                    tileHighlight = Instantiate(tileHighlightPrefab, transform).GetComponent<TileHighlight>();
                    tileHighlight.Setup();
                }

                tileHighlight.Activate(_colorPositions[i].Color, _colorPositions[i].Position);

                tileHighlights.Add(tileHighlight);
            }
        }

        public void ClearTiles()
        {
            for (int i = 0; i < tileHighlights.Count; i++)
            {
                tileHighlights[i].Deactivate();
                tileHighlightPool.ReturnTileHighlightToPool(tileHighlights[i]);
            }
            tileHighlights.Clear();
        }

        #endregion
    }
}
