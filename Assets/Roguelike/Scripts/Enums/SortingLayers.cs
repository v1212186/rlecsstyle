﻿namespace Roguelike.Scripts.Enums
{
    public enum SortingLayers
    {
        Tiles = 1, Entities = 2, Items = 3, Actors = 4, Effects = 5, WorldUserInterface = 6, ShadowMap = 7
    }
}