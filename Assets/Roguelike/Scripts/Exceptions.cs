﻿using System;

namespace Roguelike.Scripts
{
    public class Exceptions {

        

        [Serializable]
        public class CellOutOfBoundsException : Exception
        {
            #region Constructor

            public CellOutOfBoundsException(string _message) : base(_message)
            {

            }

            #endregion
        }

    }
}
