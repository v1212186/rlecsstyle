﻿namespace Roguelike.Scripts.CombatSystem
{
    public enum AttackResult
    {
        Miss, Hit, CannotAttack
    }
}
