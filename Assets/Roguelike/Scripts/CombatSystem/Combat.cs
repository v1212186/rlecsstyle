﻿using Ecs.Core;
using Roguelike.Scripts.Attributes;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using UnityEngine;

namespace Roguelike.Scripts.CombatSystem
{
    public static class Combat
    {

        #region Variables

        private const float MinDamageRange = 0.95f;
        private const float MaxDamageRange = 1.05f;

        #endregion

        #region Methods

        //TODO где проверять на наличие необходимых компонентов? там где вызываем этот метод или все же в этом методе?
        public static AttackResult Attack(Entity _sourceEntity, Entity _targetEntity)
        {
            if (!_sourceEntity.HasEntityComponent<CombatAttributesComponent>() ||
                !_targetEntity.HasEntityComponent<HealthAttributeComponent>())
            {
                return AttackResult.CannotAttack;
            }

            //TODO проверить на Invinsible флаг или статус, как будет реализовано
            //if ()
            //{
            //    return AttackResult.Invincible;
            //}

            if (Random.value >= _sourceEntity.GetEntityComponent<CombatAttributesComponent>().HitChanceAttribute.FinalValue / HitChanceAttribute.ValueMaxCap)
            {
                return AttackResult.Miss;
            }

            float damage = CalculateDamage(_sourceEntity, _targetEntity);

            DealDamage(_targetEntity, damage);

            return AttackResult.Hit;
        }

        //Damage = sAttack * sAttack / (sAttack + dDefence)
        public static float CalculateDamage(Entity _sourceEntity, Entity _targetEntity)
        {
            float sourceAttackValue = _sourceEntity.GetEntityComponent<CombatAttributesComponent>().AttackAttribute.FinalValue;
            float targetDefenceValue =
                _targetEntity.GetEntityComponent<CombatAttributesComponent>().DefenceAttribute.FinalValue;

            float damage = Random.Range(MinDamageRange, MaxDamageRange) * (sourceAttackValue * sourceAttackValue) /
                           (sourceAttackValue + targetDefenceValue);

            return damage;
        }

        public static void DealDamage(Entity _targetEntity, float _damage)
        {
            _targetEntity.GetEntityComponent<HealthAttributeComponent>().ChangeHealthValue(-_damage);
            //TODO сделать систему, которая будет обрабатывать этот флаг, выводя цифры дамага + может какой эффект
            //TODO может во флаг передавать напраление, чтобы сделать соотв анимацию
            World.Instance.EntityManager.AddTempFlagComponent(_targetEntity, new DamageReceivedTempFlag());
        }

        public static void ReceiveHeal(Entity _targetEntity, float _heal)
        {
            _targetEntity.GetEntityComponent<HealthAttributeComponent>().ChangeHealthValue(_heal);
            //TODO скорее всего систему с каким-нибудь эффектом
            World.Instance.EntityManager.AddTempFlagComponent(_targetEntity, new HealReceivedTempFlag());
        }

        #endregion

    }
}
