﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.Actions.DirectionalActions
{
    public abstract class DirectionalAction : EntityAction
    {

        #region Variables

        protected Vector2Int? direction;

        #endregion

        #region Constructors

        protected DirectionalAction() : base()
        {
        }

        protected DirectionalAction(Vector2Int? _direction) : base()
        {
            SetDirection(_direction);
        }

        #endregion

        #region Methods

        public void SetDirection(Vector2Int? _direction)
        {
            if (!_direction.HasValue)
            {
                return;
            }
            direction = _direction;
            recentActionResult.SetFlag(ActionResultFlags.Checking);
        }

        protected void GetDirectionalInput()
        {
            direction = InputManager.GetDirectionalInput();
            if (direction != null)
            {
                recentActionResult.SetFlag(ActionResultFlags.Checking);
            }
        }

        public abstract bool CanApplyInDirection(Vector2Int? _direction, Entity _actorEntity);

        protected override void OnActionEnd(Entity _actorEntity)
        {
            base.OnActionEnd(_actorEntity);
            direction = null;
        }


        public override void OnActionFailOrCancel()
        {
            base.OnActionFailOrCancel();
            direction = null;
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();

            if (direction.HasValue)
            {
                jObject["directionX"] = direction.Value.x;
                jObject["directionY"] = direction.Value.y;
            }

            return jObject;
        }

        protected override void OnFromJson(JObject _jObject)
        {
            base.OnFromJson(_jObject);

            direction = null;

            JToken directionX;
            _jObject.TryGetValue("directionX", out directionX);
            JToken directionY;
            _jObject.TryGetValue("directionY", out directionY);
            if (directionX != null && directionY != null)
            {
                direction = new Vector2Int(directionX.ToObject<int>(), directionY.ToObject<int>());
            }
        }

        #endregion

    }
}
