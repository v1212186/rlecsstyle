﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.CombatSystem;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.Actions.DirectionalActions
{
    public class AttackEntityAction : DirectionalAction
    {

        #region Variables

        private float timer;

        private const float AttackTime = 0.15f;

        private AnimationCurves.AnimationCurveTypes animationCurveType;

        private AnimationCurve attackAnimationCurve;

        public AnimationCurve AttackAnimationCurve
        {
            get
            {
                if (attackAnimationCurve == null)
                {
                    //TODO избавиться от референса на Ворлд
                    attackAnimationCurve = World.Instance.ResourcesManager.GetAnimationCurve(animationCurveType);
                }

                return attackAnimationCurve;
            }
        }

        private readonly ComponentsFilter canApplyInDirectionComponentsFilter = new ComponentsFilter().AllOf(typeof(HealthAttributeComponent));

        #endregion

        #region Constructors

        public AttackEntityAction() : base()
        {

        }

        public AttackEntityAction(AnimationCurves.AnimationCurveTypes _animationCurveType) : this(null, _animationCurveType)
        {
            animationCurveType = _animationCurveType;
        }

        public AttackEntityAction(Vector2Int? _direction, AnimationCurves.AnimationCurveTypes _animationCurveType) : base(_direction)
        {
            animationCurveType = _animationCurveType;
        }

        #endregion

        #region Methods

        protected override void OnProcessAction(float _deltaTime, Entity _actorEntity)
        {
            if (recentActionResult.IsChecking)
            {
                if (CanApplyInDirection(direction, _actorEntity))
                {
                    recentActionResult.SetFlag(ActionResultFlags.Performing);

                    GridPositionComponent gridPositionComponent =
                        _actorEntity.GetEntityComponent<GridPositionComponent>();
                    World.Instance.EntityManager.AddTempFlagComponent(_actorEntity, new HeadingChangedTempFlag(gridPositionComponent.Position,
                        gridPositionComponent.Position + direction.Value));
                }
                else
                {
                    recentActionResult.SetFlag(ActionResultFlags.Failed);
                }
            }

            if (recentActionResult.IsPerforming)
            {
                timer += _deltaTime;
                GameObjectComponent gameObjectComponent = _actorEntity.GetEntityComponent<GameObjectComponent>();
                GridPositionComponent gridPositionComponent =
                    _actorEntity.GetEntityComponent<GridPositionComponent>();
                gameObjectComponent.SetTransformLocalPosition(Vector2.Lerp(gridPositionComponent.Position, gridPositionComponent.Position + direction.Value,
                    AttackAnimationCurve.Evaluate(timer / AttackTime)));

                if (timer >= AttackTime)
                {
                    //TODO не делать анимацию здесь, вынести в отдельный компонент, брать время анимации из константы (AttackTime), добавить возможность атаковать без ожидания анимации
                    gameObjectComponent.SetTransformLocalPosition(new Vector3(gridPositionComponent.Position.x, gridPositionComponent.Position.y));
                    recentActionResult.SetFlag(ActionResultFlags.Done);

                    Vector2Int targetPos = gridPositionComponent.Position + direction.Value;

                    Combat.Attack(_actorEntity, World.Instance.LevelManager.CurrentLevel.GetMatchingEntity(targetPos,
                        canApplyInDirectionComponentsFilter));
                }
            }
        }

        protected override void OnActionStart(Entity _actorEntity)
        {
            recentActionResult.SetFlag(ActionResultFlags.Checking);
        }

        protected override void OnActionEnd(Entity _actorEntity)
        {
            base.OnActionEnd(_actorEntity);
            timer = 0.0f;
        }

        public override bool CanApplyInDirection(Vector2Int? _direction, Entity _actorEntity)
        {
            if (!_direction.HasValue)
            {
                return false;
            }

            Vector2Int targetPos = _actorEntity.GetEntityComponent<GridPositionComponent>()
                                       .Position + _direction.Value;

            return World.Instance.LevelManager.CurrentLevel.IsWithinBounds(targetPos) &&
                   World.Instance.LevelManager.CurrentLevel.HasMatchingEntity(targetPos, canApplyInDirectionComponentsFilter);
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();
            jObject["Type"] = JToken.FromObject(GetType());
            jObject["timer"] = timer;
            jObject["animationCurveType"] = JToken.FromObject(animationCurveType);
      

            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            base.OnFromJson(_jsonObject);

            timer = _jsonObject["timer"].ToObject<float>();
            animationCurveType = _jsonObject["animationCurveType"].ToObject<AnimationCurves.AnimationCurveTypes>();
        }

        #endregion
    }
}
