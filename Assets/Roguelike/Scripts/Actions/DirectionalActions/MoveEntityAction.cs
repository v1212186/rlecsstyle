﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.Actions.DirectionalActions
{
    public class MoveEntityAction : DirectionalAction
    {
        #region Variables

        private readonly ComponentsFilter canApplyInDirectionComponentsFilter = new ComponentsFilter().AllOf(typeof(IsCollidableFlag));

        #endregion

        #region Constructor

        public MoveEntityAction() : base()
        {
        }

        public MoveEntityAction(Vector2Int? _direction) : base(_direction)
        {
        }

        #endregion

        #region Methods

        protected override void OnProcessAction(float _deltaTime, Entity _actorEntity)
        {
            if (recentActionResult.IsAwaitingInput)
            {
                GetDirectionalInput();
            }

            if (!direction.HasValue)
            {
                return;
            }

            if (recentActionResult.IsChecking)
            {
                if (CanApplyInDirection(direction.Value, _actorEntity))
                {
                    recentActionResult.SetFlag(ActionResultFlags.Performing);
                }
                else
                {
                    AttackEntityAction attackEntityAction = _actorEntity.GetEntityComponent<AbilitiesSetComponent>().AttackAction as AttackEntityAction;
                    attackEntityAction?.SetDirection(direction);
                    recentActionResult = attackEntityAction;
                }
            }

            if (recentActionResult.IsPerforming)
            {
                GridPositionComponent gridPositionComponent =
                    _actorEntity.GetEntityComponent<GridPositionComponent>();

                Vector2Int destinationPos = gridPositionComponent.Position + direction.Value;

                EntityMoverComponent entityMoverComponent = _actorEntity.GetEntityComponent<EntityMoverComponent>();
                if (entityMoverComponent != null)
                {
                    entityMoverComponent.EndPosition = destinationPos;
                    entityMoverComponent.Timer = 0.0f;
                }
                else
                {
                    World.Instance.EntityManager.AddComponent(_actorEntity,
                        new EntityMoverComponent(destinationPos, AnimationCurves.AnimationCurveTypes.Movement));
                }

                World.Instance.EntityManager.AddTempFlagComponent(_actorEntity, new HeadingChangedTempFlag(gridPositionComponent.Position, destinationPos));
                World.Instance.EntityManager.AddTempFlagComponent(_actorEntity, new GridPositionChangedTempFlag(destinationPos));

                recentActionResult.SetFlag(ActionResultFlags.Done);
            }
        }

        //TODo передавать level или levelMap? чтобы избавиться от обращений к статику
        public override bool CanApplyInDirection(Vector2Int? _direction, Entity _actorEntity)
        {
            if (!_direction.HasValue)
            {
                return false;
            }

            Vector2Int targetPos = _actorEntity.GetEntityComponent<GridPositionComponent>()
                                       .Position + _direction.Value;

            return World.Instance.LevelManager.CurrentLevel.IsWithinBounds(targetPos) &&
                   !World.Instance.LevelManager.CurrentLevel.GetCellProperties(targetPos).IsCollidable &&
                   !World.Instance.LevelManager.CurrentLevel.HasMatchingEntity(targetPos, canApplyInDirectionComponentsFilter);
        }

        protected override void OnActionStart(Entity _actorEntity)
        {
            base.OnActionStart(_actorEntity);
            if (direction == null)
            {
                recentActionResult.SetFlag(ActionResultFlags.AwaitingInput);
            }
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();
            jObject["Type"] = JToken.FromObject(GetType());
        
            return jObject;
        }

        #endregion
    }
}
