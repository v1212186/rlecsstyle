﻿using Ecs.Core;
using Newtonsoft.Json.Linq;

namespace Roguelike.Scripts.Actions
{
    //TODO чтобы избавиться от ручного вызова конца действия, сделать метод ActionIsDone() и прочие, который будет присваивать значение результату и вызывать соотв методы
    //TODO не забыть про bool use energy, вынести его в экшон резалт, возможно еще добавить спенд турн он фейл
    
    //TODO сделать наподжобии монобехавиор, виртуальные методы OnActionDone,OnActionChecking,OnActionFailed и так далее
    //TODO разделить Failed и Canceled
    public abstract class EntityAction : IJsonSerializable
    {

        #region Variables

        protected ActionResult recentActionResult;  //TODO пока что я забил на экшон резалт при сериализации
        public ActionResult RecentActionResult { get { return recentActionResult; } }

        private bool isActionStarted = false;

        #endregion

        #region Constructor

        protected EntityAction()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Default);
        }

        #endregion

        #region Methods

        public static implicit operator ActionResult(EntityAction _action)
        {
            return new ActionResult(_action);
        }

        public ActionResult ProcessAction(float _deltaTime, Entity _actorEntity)
        {
            if (!isActionStarted)
            {
                OnActionStart(_actorEntity);
                isActionStarted = true;
            }

            OnProcessAction(_deltaTime, _actorEntity);

            if (recentActionResult.IsDone)
            {
                OnActionEnd(_actorEntity);
                isActionStarted = false;
            }

            if (recentActionResult.IsFailed)
            {
                OnActionFailOrCancel();
                isActionStarted = false;
            }
            return recentActionResult;
        }

        protected abstract void OnProcessAction(float _deltaTime, Entity _actorEntity);

        protected virtual void OnActionStart(Entity _actorEntity)
        {
            return;
        }

        protected virtual void OnActionEnd(Entity _actorEntity)
        {
            return;
        }

        public virtual void OnActionFailOrCancel()
        {
            return;
        }

        #region Serialization

        public JObject ToJson()
        {
            return OnToJson();
        }

        public void FromJson(JObject _jObject)
        {
            OnFromJson(_jObject);
        }

        protected virtual JObject OnToJson()
        {
            JObject entityAction = new JObject
            {
                ["recentActionResult"] = JToken.FromObject(recentActionResult.Flags)
            };
            return entityAction;
        }

        protected virtual void OnFromJson(JObject _jObject)
        {
            recentActionResult = new ActionResult(_jObject["recentActionResult"].ToObject<ActionResultFlags>());
        } 

        #endregion

        #endregion

    }
}
