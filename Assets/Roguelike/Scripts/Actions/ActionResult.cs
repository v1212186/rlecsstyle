﻿namespace Roguelike.Scripts.Actions
{
    //[Flags]
    public enum ActionResultFlags
    {
        Default = 0x0,
        Performing = 0x1,
        Done = 0x2,
        AwaitingInput = 0x4,
        Checking = 0x8,
        Failed = 0x10,
        //0x10, 0x20, 0x40, 0x80 ...
    }

    public class ActionResult
    {
        #region Variables

        private ActionResultFlags flags;
        public ActionResultFlags Flags => flags;

        private readonly EntityAction alternateAction;
        public EntityAction AlternateAction => alternateAction;

        public bool IsFailed => IsFlagSet(ActionResultFlags.Failed);
        public bool IsAwaitingInput => IsFlagSet(ActionResultFlags.AwaitingInput);
        public bool IsChecking => IsFlagSet(ActionResultFlags.Checking);
        public bool IsDone => IsFlagSet(ActionResultFlags.Done);
        public bool IsPerforming => IsFlagSet(ActionResultFlags.Performing);

        #endregion

        #region Constructor

        public ActionResult(ActionResultFlags _flags)
        {
            flags = _flags;
        }

        public ActionResult(EntityAction _alternateActionnate)
        {
            alternateAction = _alternateActionnate;
            flags = ActionResultFlags.Failed;
        }

        #endregion

        #region Methods

        private bool IsFlagSet(ActionResultFlags _flag)
        {
            return flags == _flag;
        }

        public void SetFlag(ActionResultFlags _flag)
        {
            flags = _flag;
        } 

        #endregion

    }
}