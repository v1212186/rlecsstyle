﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.CombatSystem;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.WorldUserInterface;

namespace Roguelike.Scripts.Actions.LinearActions
{
    public class LightningAction : LineAction
    {

        #region Variables

        private GridPositionComponent gridPositionComponent;

        #endregion

        #region Constructor

        public LightningAction() : base()
        {

        }

        public LightningAction(LineProperties _lineProperties, ComponentsFilter _targetComponentsFilter) : base(_lineProperties, _targetComponentsFilter)
        {

        }

        #endregion

        #region Methods

        protected override void OnProcessAction(float _deltaTime, Entity _actorEntity)
        {
            if (recentActionResult.IsAwaitingInput)
            {
                GetDirectionalInputForCursor(_actorEntity);
               
                FindAndDrawLine(gridPositionComponent.Position, CursorController.Instance.GetPosition());
                if (GetConfirmInput())
                {
                    recentActionResult.SetFlag(ActionResultFlags.Performing);
                }
            }

            if (recentActionResult.IsPerforming)
            {
                for (int i = 0; i < positionValidities.Count; i++)
                {
                    if (positionValidities[i].IsValid)
                    {
                        Entity entity =
                            World.Instance.LevelManager.CurrentLevel.GetMatchingEntity(positionValidities[i].Position, targetComponentsFilter);
                        Combat.DealDamage(entity, Combat.CalculateDamage(_actorEntity, entity));
                    }
                }
                recentActionResult.SetFlag(ActionResultFlags.Done);
            }
        }

        protected override void OnActionStart(Entity _actorEntity)
        {
            base.OnActionStart(_actorEntity);
            gridPositionComponent = _actorEntity.GetEntityComponent<GridPositionComponent>();
            CursorController.Instance.SetPosition(gridPositionComponent.Position);
            recentActionResult.SetFlag(ActionResultFlags.AwaitingInput);
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();

            jObject["Type"] = JToken.FromObject(GetType());

            return jObject;
        }

        #endregion

    }
}
