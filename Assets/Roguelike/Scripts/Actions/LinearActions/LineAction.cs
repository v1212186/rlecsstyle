﻿using System.Collections.Generic;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.FieldOfView;
using Roguelike.Scripts.Utilities;
using Roguelike.Scripts.WorldUserInterface;
using UnityEngine;

namespace Roguelike.Scripts.Actions.LinearActions
{
    public struct PositionValidity
    {
        #region Variables

        public readonly Vector2Int Position;
        public readonly bool IsValid;

        #endregion

        #region Constructor

        public PositionValidity(Vector2Int _position, bool _isValid)
        {
            Position = _position;
            IsValid = _isValid;
        }

        #endregion
    }

    public class LineProperties
    {
        #region Variables

        /// <summary>
        /// Line range, it's fractional part is always 0.5f, because it is needed for FOV
        /// </summary>
        public readonly float Range;

        /// <summary>
        /// Can line ignore collidable cells
        /// </summary>
        public readonly bool IgnoreCollidableCells;

        #endregion

        #region Constructors

        public LineProperties(float _range, bool _ignoreCollidableCells)
        {
            Range = Mathf.Floor(_range) + 0.5f;
            IgnoreCollidableCells = _ignoreCollidableCells;
        }

        #endregion
    }

    //TODO передавать сюда level
    public abstract class LineAction : EntityAction
    {

        #region Variables

        protected LineProperties lineProperties;

        protected bool needToRedraw;

        protected readonly List<PositionValidity> positionValidities;

        private readonly RecursiveShadowmap recursiveShadowmap = new RecursiveShadowmap();

        /// <summary>
        /// Determines targetable entities
        /// </summary>
        protected ComponentsFilter targetComponentsFilter;

        #endregion

        #region Constructors

        //TODO сделать возможность каста сразу в точку для NPC, либо сделать некоторые методы публичными, чтобы это можно было делать вне класса
        protected LineAction() : base()
        {
            needToRedraw = true;
            positionValidities = new List<PositionValidity>();
        }

        protected LineAction(LineProperties _lineProperties, ComponentsFilter _targetComponentsFilter) : base()
        {
            lineProperties = _lineProperties;
            targetComponentsFilter = _targetComponentsFilter;
            needToRedraw = true;
            positionValidities = new List<PositionValidity>();
        }

        #endregion

        #region Methods

        protected void GetDirectionalInputForCursor(Entity _actorEntity)
        {
            CursorController.Instance.ShowCursor();
            Vector2Int? directionalInput = InputManager.GetDirectionalInput();
            if (directionalInput.HasValue)
            {
                Vector2Int newCursorPosition = CursorController.Instance.GetPosition() + directionalInput.Value;
                if (CheckPositionForRules(
                        _actorEntity.GetEntityComponent<GridPositionComponent>().Position,
                        newCursorPosition))
                {
                    CursorController.Instance.SetPosition(newCursorPosition);
                    needToRedraw = true;
                }
            }
        }

        protected bool GetConfirmInput()
        {
            if (InputManager.GetConfirmInput())
            {
                TileHighlightController.Instance.ClearTiles();
                CursorController.Instance.HideCursor();
                return true;
            }
            return false;
        }

        protected void FindAndDrawLine(Vector2Int _start, Vector2Int _end)
        {
            if (!needToRedraw)
            {
                return;
            }

            TileHighlightController.Instance.ClearTiles();

            DrawRadius(_start);

            CheckLineForValidity(_start, _end);

            ColorPosition[] colorPositions = new ColorPosition[positionValidities.Count];
            for (int i = 0; i < positionValidities.Count; i++)
            {
                //TODO добавить InvalidColor при непроходе проверки на валидность, однако как отличать от просто позиции, где вообще нет энтити
                colorPositions[i] = new ColorPosition(positionValidities[i].IsValid ? TileHighlight.ValidColor : TileHighlight.HighlightColor, positionValidities[i].Position);
            }

            TileHighlightController.Instance.CreateTileHighlightAtPositions(colorPositions);

            needToRedraw = false;
        }

        protected void CheckLineForValidity(Vector2Int _start, Vector2Int _end)
        {
            positionValidities.Clear();

            if (_start == _end)
            {
                positionValidities.Add(new PositionValidity(_start, CheckPositionForValidity(_end)));
                return;
            }

            List<XiaolinWuLine.Vector2IntConfidencePair> confidencePairs = XiaolinWuLine.CreateLine(_start, _end);

            for (int i = 1; i < confidencePairs.Count; i++)
            {
                Vector2Int pos = CheckVectorConfidencePairForRules(_start, confidencePairs[i]);
                positionValidities.Add(new PositionValidity(pos, CheckPositionForValidity(pos)));
            }
        }

        private Vector2Int CheckVectorConfidencePairForRules(Vector2Int _start, XiaolinWuLine.Vector2IntConfidencePair _confidencePair)
        {
            bool firstPassed = CheckPositionForRules(_start, _confidencePair.vectorConfidencePair[0].Vector2Int);
            bool secondPassed = CheckPositionForRules(_start, _confidencePair.vectorConfidencePair[1].Vector2Int);

            if (firstPassed && secondPassed)
            {
                return _confidencePair.vectorConfidencePair[0].Confidence >
                       _confidencePair.vectorConfidencePair[1].Confidence ?
                   _confidencePair.vectorConfidencePair[0].Vector2Int :
                   _confidencePair.vectorConfidencePair[1].Vector2Int;
            }

            if (firstPassed)
            {
                return _confidencePair.vectorConfidencePair[0].Vector2Int;
            }

            if (secondPassed)
            {
                return _confidencePair.vectorConfidencePair[1].Vector2Int;
            }

            return _confidencePair.vectorConfidencePair[0].Confidence >
                   _confidencePair.vectorConfidencePair[1].Confidence ?
             _confidencePair.vectorConfidencePair[0].Vector2Int :
               _confidencePair.vectorConfidencePair[1].Vector2Int;
        }

        /// <summary>
        /// Checks position according to line properties relative to the starting pos.
        /// </summary>
        /// <param name="_start">Start position, only for radius check</param>
        /// <param name="_end">End position that will be checked if it's within bounds, observed and collidable</param>
        /// <returns></returns>
        protected bool CheckPositionForRules(Vector2Int _start, Vector2Int _end)
        {
            if (!World.Instance.LevelManager.CurrentLevel.IsWithinBounds(_end))
            {
                return false;
            }

            float distance = Vector2Int.Distance(_start, _end);

            bool isValid = distance < lineProperties.Range;

            isValid &= World.Instance.LevelManager.CurrentLevel.GetCellProperties(_end).IsObserved; //We can select only observed cells

            if (!lineProperties.IgnoreCollidableCells)
            {
                isValid &= !World.Instance.LevelManager.CurrentLevel.GetCellProperties(_end).IsCollidable;
            }

            return isValid;
        }

        /// <summary>
        /// Checks if there are any entity that matches TargetComponentFilter.
        /// </summary>
        /// <param name="_position">Position to check</param>
        /// <returns></returns>
        protected bool CheckPositionForValidity(Vector2Int _position)
        {
            if (targetComponentsFilter != null)
            {
                return World.Instance.LevelManager.CurrentLevel.HasMatchingEntity(_position, targetComponentsFilter);
            }

            return true;
        }

        protected void DrawRadius(Vector2Int _pos)
        {
            Vector2Int[] pos = new Vector2Int[] { _pos };
            float[] radius = new float[] { lineProperties.Range };
            recursiveShadowmap.ComputeVisibility(World.Instance.LevelManager.CurrentLevel, pos, radius);    //TODO в фове сделать возможность игнорить коллайдабл? для случая, когда можно игнорить

            ColorPosition[] colorPositions = new ColorPosition[recursiveShadowmap.LightenedArea.Count];
            for (int i = 0; i < recursiveShadowmap.LightenedArea.Count; i++)
            {
                colorPositions[i] = new ColorPosition(TileHighlight.RadiusColor, recursiveShadowmap.LightenedArea[i]);
            }
            TileHighlightController.Instance.CreateTileHighlightAtPositions(colorPositions);
        }

        protected override void OnActionEnd(Entity _actorEntity)
        {
            base.OnActionEnd(_actorEntity);
            positionValidities.Clear();
            TileHighlightController.Instance.ClearTiles();
        }

        public override void OnActionFailOrCancel()
        {
            positionValidities.Clear();
            TileHighlightController.Instance.ClearTiles();
        }

        protected override JObject OnToJson()
        {
            JObject jObject = base.OnToJson();

            JObject lineActionPropertiesJObject = new JObject();
            lineActionPropertiesJObject["Range"] = lineProperties.Range;
            lineActionPropertiesJObject["IgnoreCollidableCells"] = lineProperties.IgnoreCollidableCells;

            jObject["Type"] = JToken.FromObject(GetType());
            jObject["lineActionProperties"] = lineActionPropertiesJObject;
            jObject["targetComponentsFilter"] = targetComponentsFilter.ToJson();
            jObject["needToRedraw"] = needToRedraw;
            
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            base.OnFromJson(_jsonObject);

            JObject lineActionPropertiesJObject = _jsonObject["lineActionProperties"].ToObject<JObject>();
            int range = lineActionPropertiesJObject["Range"].ToObject<int>();
            bool ignoreCollidable = lineActionPropertiesJObject["IgnoreCollidableCells"].ToObject<bool>();
            lineProperties = new LineProperties(range, ignoreCollidable);

            targetComponentsFilter = new ComponentsFilter(lineActionPropertiesJObject["TargetComponentsFilter"].ToObject<JObject>());

            needToRedraw = _jsonObject["needToRedraw"].ToObject<bool>();
        }

        #endregion
    }
}
