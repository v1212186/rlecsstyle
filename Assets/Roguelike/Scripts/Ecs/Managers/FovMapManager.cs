﻿using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Roguelike.Scripts.Ecs.Managers
{
    public class FovMapManager
    {
        #region Variables

        private readonly ShadowMapData shadowMapData;

        private readonly LevelManager levelManager;

        private readonly Tilemap shadowMapTilemap;

        #endregion

        #region Constructor

        public FovMapManager(LevelManager _levelManager, ResourcesManager _resourcesManager)
        {
            shadowMapData = _resourcesManager.ShadowMapData;
            levelManager = _levelManager;
            shadowMapTilemap = _resourcesManager.GameGridStructure.ShadowMapTilemap;
        }

        #endregion

        #region Methods

        public void RebuildShadowMapForCurrentLevel()
        {
            Vector2Int dimensions = levelManager.CurrentLevel.Dimensions;

            for (int i = 0; i < dimensions.x; i++)
            {
                for (int j = 0; j < dimensions.y; j++)
                {
                    Vector3Int tilePos = new Vector3Int(i, j, 0);
                    shadowMapTilemap.SetTile(tilePos, shadowMapData.ShadowMapTile);
                }
            }
        }

        public void UpdateShadowMapForCurrentLevel()
        {
            UpdateShadowMapUv();
        }

        public void DisposeShadowMap()
        {
            shadowMapTilemap.ClearAllTiles();
        }

        public void UpdateShadowMapUv()
        {
            Vector2Int dimensions = levelManager.CurrentLevel.Dimensions;

            for (int i = 0; i < dimensions.x; i++)
            {
                for (int j = 0; j < dimensions.y; j++)
                {
                    CellProperties cellProperties = levelManager.CurrentLevel.GetCellProperties(new Vector2Int(i, j));

                    Color tileColor;
                    if (cellProperties.IsObserved)
                    {
                        tileColor = shadowMapData.GetSpriteColor(ShadowMapData.ShadowMapTileType.Observed);
                    }
                    else if (cellProperties.IsDiscovered)
                    {
                        tileColor = shadowMapData.GetSpriteColor(ShadowMapData.ShadowMapTileType.Discovered);
                    }
                    else
                    {
                        tileColor = shadowMapData.GetSpriteColor(ShadowMapData.ShadowMapTileType.Undiscovered);
                    }

                    Vector3Int tilePos = new Vector3Int(i, j, 0);
                    shadowMapTilemap.SetColor(tilePos, tileColor);
                }
            }
        }

        #endregion
    }
}

