﻿using System;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Levels.LevelData;
using Roguelike.Scripts.Levels.LevelGenerators;
using Roguelike.Scripts.Levels.LevelVizualizers;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Managers
{
    [Serializable]
    public class LevelManager
    {

        #region Variables

        private Level currentLevel;
        public Level CurrentLevel
        {
            get { return currentLevel; }
            set { currentLevel = value; }
        }

        [SerializeField]
        private AbstractLevelGenerator levelGenerator;

        private readonly LevelVizualizer levelVizualizer;

        #endregion

        #region Constructor

        public LevelManager()
        {
            levelVizualizer = new LevelVizualizer();
        }

        #endregion

        #region Methods

        public Level GenerateLevel(TileSet.TileSetName _tileSetName)
        {
            Level level = levelGenerator.GenerateLevel();
            level.SetTilesetName(_tileSetName);
            return level;
        }

        public void VisualizeLevel(Level _level)
        {
            levelVizualizer.VisualizeLevel(_level);
        }

        public void DisposeLevel()
        {
            currentLevel = null;
            levelVizualizer.ClearVizualization();
        }

        public JObject SaveLevel()
        {
            return currentLevel.ToJson();
        }

        public void LoadLevel(Level _level)
        {
            currentLevel = _level;
            levelVizualizer.VisualizeLevel(CurrentLevel);
        }

        //TODO доделать + скорее всего здесь будет ссылка на фов менеджер + сохранять нужно ли сохранять в тайлах видит ли плеер текущий тайл
        public void CastLight(Vector2Int _position)
        {

        }

        #endregion

    }
}
