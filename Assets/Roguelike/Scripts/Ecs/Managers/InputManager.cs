﻿using UnityEngine;

namespace Roguelike.Scripts.Ecs.Managers
{
    //TODO добавить метод ресет и вызывать его в конце каждого кадра, нужно для конфирм инпута,чтобы потом его по кнопке можно было сделать
    public static class InputManager
    {

        #region Variables

        private static Vector2Int? directionalInput;

        private static bool confirmInput;

        #endregion

        #region Constructor

        static InputManager()
        {

        }

        #endregion

        #region Methods

        public static void ProcessInput()
        {
            ProcessDirectionalInput();
            ProcessConfirmInput();
        }

        #region Directional input

        public static Vector2Int? GetDirectionalInput()
        {
            return directionalInput;
        }

        private static void ProcessDirectionalInput()
        {
            directionalInput = ProcessKeyboardInput();
            if (directionalInput != null)
            {
                return;
            }

            directionalInput = ProcessMouseInput();
            if (directionalInput != null)
            {
                return;
            }

            directionalInput = ProcessTouchInput();
        }

        private static Vector2Int? ProcessKeyboardInput()
        {
            int h = 0, v = 0;

            if (Input.GetKeyDown(KeyCode.A))
            {
                h = -1;
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                v = 1;
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                h = 1;
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                v = -1;
            }

            Vector2Int? input = new Vector2Int(h, v);
            return input != Vector2Int.zero ? input : null;
        }

        private static Vector2Int? ProcessMouseInput()
        {
            int h = 0, v = 0;

            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mousePosRaw = Input.mousePosition;
                mousePosRaw.z = 0.0f;
                Vector3 screenCenter = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f);
                Vector3 diff = mousePosRaw - screenCenter;
                diff.x /= Screen.width * 0.5f;
                diff.y /= Screen.height * 0.5f;

                if (Mathf.Abs(diff.x) > Mathf.Abs(diff.y))
                {
                    h = (int)Mathf.Sign(diff.x);
                }
                else
                {
                    v = (int)Mathf.Sign(diff.y);
                }
            }

            Vector2Int? input = new Vector2Int(h, v);
            return input != Vector2Int.zero ? input : null;
        }

        private static Vector2Int? ProcessTouchInput()
        {
            int h = 0, v = 0;

            if (Input.touchCount > 0)
            {
                if (Input.touches[0].phase == TouchPhase.Began)
                {
                    Vector3 mousePosRaw = Input.touches[0].position;
                    mousePosRaw.z = 0.0f;
                    Vector3 screenCenter = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f);
                    Vector3 diff = mousePosRaw - screenCenter;
                    diff.x /= Screen.width * 0.5f;
                    diff.y /= Screen.height * 0.5f;

                    if (Mathf.Abs(diff.x) > Mathf.Abs(diff.y))
                    {
                        h = (int)Mathf.Sign(diff.x);
                    }
                    else
                    {
                        v = (int)Mathf.Sign(diff.y);
                    }
                }
            }

            Vector2Int? input = new Vector2Int(h, v);
            return input != Vector2Int.zero ? input : null;
        }

        #endregion

        #region Confirm input

        public static bool GetConfirmInput()
        {
            return confirmInput;
        }

        private static void ProcessConfirmInput()
        {
            confirmInput = Input.GetKeyDown(KeyCode.Space);
        }

        #endregion

        #endregion

    }
}
