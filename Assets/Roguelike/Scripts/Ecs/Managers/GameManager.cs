﻿using System.Collections.Generic;
using Ecs.Core;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.EntitiesGeneration;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Managers
{
    public enum GameState
    {
        MainMenu, Playing
    }

    //TODO написать метод для смены левела (например при переходе между уровнями)
    public class GameManager
    {
        #region Variables

        private readonly EntityManager entityManager;

        private readonly LevelManager levelManager;

        private readonly EntitiesGenerator entitiesGenerator;

        private readonly SavesManager savesManager;

        private readonly FovMapManager fovMapManager;

        public Entity PlayerEntity { get; private set; }    //TODO Трекать плеер энтити можно через группу

        private GameState gameState;

        #endregion

        #region Properties

        public GameState GameState => gameState;

        #endregion

        #region Constructors

        public GameManager(EntityManager _entityManager, LevelManager _levelManager, EntitiesGenerator _entitiesGenerator, SavesManager _savesManager,
            FovMapManager _fovMapManager)
        {
            entityManager = _entityManager;
            levelManager = _levelManager;
            entitiesGenerator = _entitiesGenerator;
            savesManager = _savesManager;
            fovMapManager = _fovMapManager;
            gameState = GameState.MainMenu;
        }

        #endregion

        #region Methods

        public void StartNewGame(Entity _player)
        {
            PlayerEntity = _player;

            levelManager.CurrentLevel = levelManager.GenerateLevel(TileSet.TileSetName.Default);
            levelManager.VisualizeLevel(levelManager.CurrentLevel);
            fovMapManager.RebuildShadowMapForCurrentLevel();

            List<Entity> entities = entitiesGenerator.GenerateEntitesForLevel(levelManager.CurrentLevel);

            entityManager.RegisterEntity(PlayerEntity);
            foreach (Entity entity in entities)
            {
                entityManager.RegisterEntity(entity);
            }

            gameState = GameState.Playing;
        }

        public void LoadGame()
        {
            entityManager.DisposeAllEntities();
            levelManager.DisposeLevel();
            fovMapManager.DisposeShadowMap();

            SavesManager.GameSave gameSave = savesManager.LoadGame();

            levelManager.LoadLevel(gameSave.Level);
            fovMapManager.RebuildShadowMapForCurrentLevel();

            PlayerEntity = gameSave.PlayerEntity;
            entityManager.RegisterEntity(PlayerEntity);
            for (int i = 0; i < gameSave.Entities.Length; i++)
            {
                entityManager.RegisterEntity(gameSave.Entities[i]);
            }

            gameState = GameState.Playing;
        }

        public void MoveToTheNextLevel()
        {
            entityManager.DisposeAllEntities();
            levelManager.DisposeLevel();
            fovMapManager.DisposeShadowMap();

            levelManager.CurrentLevel = levelManager.GenerateLevel(TileSet.TileSetName.Default);
            levelManager.VisualizeLevel(levelManager.CurrentLevel);
            fovMapManager.RebuildShadowMapForCurrentLevel();

            List<Entity> entities = entitiesGenerator.GenerateEntitesForLevel(levelManager.CurrentLevel);

            entityManager.RegisterEntity(PlayerEntity);

            foreach (Entity entity in entities)
            {
                entityManager.RegisterEntity(entity);
            }
            gameState = GameState.Playing;
        }

        #endregion

    }
}
