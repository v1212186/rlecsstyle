﻿using System;
using System.Collections.Generic;
using Cinemachine;
using Roguelike.Scripts.DynamicTiles;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.WorldUserInterface.HealthAndEnergyBar;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

namespace Roguelike.Scripts.Ecs.Managers
{
    #region Nested classes

    [Serializable]
    public class GameGridStructure
    {
        public enum GridObjectLayer
        {
            Entity, Item, Actor, Effect, World
        }

        [SerializeField]
        private Tilemap tilemap;
        public Tilemap Tilemap { get { return tilemap; } }
        [SerializeField]
        private Transform tilesLayer;
        //public Transform TilesLayer { get { return tilesLayer; } }
        [SerializeField]
        private Transform entitiesLayer;
        //public Transform EntitiesLayer { get { return entitiesLayer; } }
        [SerializeField]
        private Transform itemsLayer;
        //public Transform ItemsLayer { get { return itemsLayer; } }
        [SerializeField]
        private Transform actorsLayer;
        //public Transform ActorsLayer { get { return actorsLayer; } }
        [SerializeField]
        private Transform effectsLayer;
        //public Transform EffectsLayer { get { return effectsLayer; } }
        [SerializeField]
        private Transform worldLayer;
        //public Transform WorldLayer { get { return worldLayer; } }
        [SerializeField]
        private Tilemap shadowMapTilemap;
        public Tilemap ShadowMapTilemap => shadowMapTilemap;

        public Transform GetParentTransform(GridObjectLayer _gridObjectLayer)
        {
            switch (_gridObjectLayer)
            {
                case GridObjectLayer.Entity:
                    return entitiesLayer;
                case GridObjectLayer.Item:
                    return itemsLayer;
                case GridObjectLayer.Actor:
                    return actorsLayer;
                case GridObjectLayer.Effect:
                    return effectsLayer;
                case GridObjectLayer.World:
                    return worldLayer;
                default: return null;
            }
        }
    }

    [Serializable]
    public class TileSet
    {
        public enum TileSetName
        {
            Default
        }

        [SerializeField]
        private TileSetName tileSetName;
        public TileSetName Name
        {
            get { return tileSetName; }
        }

        [SerializeField]
        private DynamicTile floorTile;
        public DynamicTile FloorTile { get { return floorTile; } }
        [SerializeField]
        private DynamicTile wallTile;
        public DynamicTile WallTile { get { return wallTile; } }
    }

    [Serializable]
    public class AnimationCurves
    {
        public enum AnimationCurveTypes
        {
            Movement, Attack
        }

        [SerializeField]
        private AnimationCurveTypes animationCurveTypes;

        public AnimationCurveTypes AnimationCurveType => animationCurveTypes;

        [SerializeField]
        private AnimationCurve animationCurve;
        public AnimationCurve AnimationCurve => animationCurve;
    }

    [Serializable]
    public class ShadowMapData
    {
        public enum ShadowMapTileType
        {
            Observed, Discovered, Undiscovered
        }

        [SerializeField]
        private Tile shadowMapTile;
        public Tile ShadowMapTile => shadowMapTile;

        [SerializeField]
        private Color observedColor;
        [SerializeField]
        private Color discoveredColor;
        [SerializeField]
        private Color undiscoveredColor;

        public Color GetSpriteColor(ShadowMapTileType _shadowMapTileType)
        {
            switch (_shadowMapTileType)
            {
                case ShadowMapTileType.Observed:
                    return observedColor;
                case ShadowMapTileType.Discovered:
                    return discoveredColor;
                case ShadowMapTileType.Undiscovered:
                    return undiscoveredColor;
                default:
                    return undiscoveredColor;
            }
        }
    }

    [Serializable]
    public class HumanoidAnimations
    {
        [SerializeField]
        private Sprite[] humanoid0Sprites;
        [SerializeField]
        private Sprite[] humanoid1Sprites;

        public CustomAnimationComponent.Animation GetHumanoidAnimationSprite(int? _animationId = null)
        {
            Sprite[] sprites = new Sprite[2];
            int rng = _animationId ?? Random.Range(0, humanoid0Sprites.Length);
            sprites[0] = humanoid0Sprites[rng];
            sprites[1] = humanoid1Sprites[rng];
            return new CustomAnimationComponent.Animation(rng, sprites);
        }
    }



    [Serializable]
    public class SpritesCollection
    {
        [Serializable]
        public class SpriteId
        {
            [SerializeField]
            private Sprite sprite;
            public Sprite Sprite => sprite;

            [SerializeField]
            private int id;
            public int Id => id;

            public SpriteId(Sprite _sprite, int _id)
            {
                sprite = _sprite;
                id = _id;
            }
        }

        [SerializeField]
        private Sprite[] sprites;

        public SpriteId GetSprite(int? _id = null)
        {
            int rng = _id ?? Random.Range(0, sprites.Length);
            return new SpriteId(sprites[rng], rng);
        }
    }

    [Serializable]
    public class CameraSetup
    {
        [SerializeField]
        private Transform cameraFollowTransform;

        public Transform CameraFollowTransform => cameraFollowTransform;

        [SerializeField]
        private Transform cameraSetupTransform;
        public Transform CameraSetupTransform => cameraSetupTransform;
    }

    #endregion

    [Serializable]
    public class ResourcesManager
    {

        #region Variables
        [SerializeField]
        private CameraSetup cameraSetup;
        public CameraSetup CameraSetup => cameraSetup;

        [SerializeField]
        private GameGridStructure gameGridStructure;
        public GameGridStructure GameGridStructure
        {
            get { return gameGridStructure; }
        }

        [SerializeField]
        private List<AnimationCurves> animationCurves;

        [SerializeField]
        private HumanoidAnimations humanoidAnimations;
        public HumanoidAnimations HumanoidAnimations => humanoidAnimations;

        [SerializeField]
        private SpritesCollection scrollsSprites;
        public SpritesCollection ScrollsSprites => scrollsSprites;

        [SerializeField]
        private List<TileSet> tileSets;

        [SerializeField]
        private ShadowMapData shadowMapData;
        public ShadowMapData ShadowMapData => shadowMapData;

        [SerializeField]
        private WorldHealthBarUi worldHealthBarUiPrefab;
        public WorldHealthBarUi WorldHealthBarUiPrefab => worldHealthBarUiPrefab;

        #endregion

        #region Methods

        public TileSet GetTileSet(TileSet.TileSetName _tileSetName)
        {
            for (int i = 0; i < tileSets.Count; i++)
            {
                if (tileSets[i].Name == _tileSetName)
                {
                    return tileSets[i];
                }
            }

            return null;
        }

        public AnimationCurve GetAnimationCurve(AnimationCurves.AnimationCurveTypes _animationCurveType)
        {
            for (int i = 0; i < animationCurves.Count; i++)
            {
                if (animationCurves[i].AnimationCurveType == _animationCurveType)
                {
                    return animationCurves[i].AnimationCurve;
                }
            }

            return null;
        }

        #endregion

    }
}
