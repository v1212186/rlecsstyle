﻿using Roguelike.Scripts.EntitiesGeneration;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Managers
{
    public class UserInterfaceManager : MonoBehaviour
    {

        #region Variables
	
        private GameManager gameManager;

        private EntitiesGenerator entitiesGenerator;

        #endregion
	
        #region Properties
	
        #endregion

        #region MonoBehaviour methods
	
        private void Start ()
        {
            gameManager = World.Instance.GameManager;
            entitiesGenerator = World.Instance.EntitiesGenerator;
        }
	
        private void Update () {
		
        }
	
        #endregion
	
        #region Methods

        public void StartNewGame()
        {
            gameManager.StartNewGame(entitiesGenerator.CreatePlayerEntity(Vector2Int.one));
        }

        public void LoadGame()
        {
            gameManager.LoadGame();
        }

        #endregion

    }
}
