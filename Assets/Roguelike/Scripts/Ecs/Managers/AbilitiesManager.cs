﻿using Roguelike.Scripts.Actions;

namespace Roguelike.Scripts.Ecs.Managers
{
    public class AbilitiesManager  {

        #region Variables

        public readonly EntityAction DefaultMoveAction;

        public readonly EntityAction DefaultAttackAction;

        #endregion

        #region Constructor

        public AbilitiesManager()
        {
        }

        #endregion
    }
}
