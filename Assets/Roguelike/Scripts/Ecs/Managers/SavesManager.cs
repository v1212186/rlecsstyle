﻿using System;
using System.IO;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Managers
{
    [Serializable]
    public class MoreThanOnePlayerException : Exception
    {
        #region Constructor

        public MoreThanOnePlayerException(string _message) : base(_message)
        {

        }

        #endregion
    }

    //TODO написать класс обертку для JObject, чтобы там сразу хранить плеера, левел и всю остальную нужную инфу
    public class SavesManager
    {
        #region Nested classes

        public class GameSave
        {

            #region Variables

            public readonly DateTime DateTime;
            public readonly Level Level;
            public readonly Entity PlayerEntity;
            public readonly Entity[] Entities;

            #endregion

            #region Constructor

            public GameSave(JObject _gameSaveJObject, EntityManager _entityManager)
            {
                DateTime = _gameSaveJObject["DateTime"].ToObject<DateTime>();
                Level = new Level(_gameSaveJObject["Level"].ToObject<JObject>());
                PlayerEntity = _entityManager.CreateEntityFromJson(_gameSaveJObject["Player"].ToObject<JObject>());

                JArray entitiesJArray = _gameSaveJObject["Entities"].ToObject<JArray>();
                Entities = new Entity[entitiesJArray.Count];

                for (int i = 0; i < entitiesJArray.Count; i++)
                {
                    Entities[i] = _entityManager.CreateEntityFromJson(entitiesJArray[i].ToObject<JObject>());
                }
            }

            #endregion

            #region Methods



            #endregion

        }

        #endregion

        #region Variables

        private const string SaveDirectory = "/RogueGameSave/";

        private const string SaveName = "GameSave_00.txt";  //TODO когда разберусь как различать сейвы, то менять имя динамически

        private readonly LevelManager levelManager;

        private readonly EntityManager entityManager;

        private readonly Group playerActorGroup;

        private readonly Group nonPlayerGroup;

        #endregion

        #region Constructor

        public SavesManager(LevelManager _levelManager, EntityManager _entityManager)
        {
            levelManager = _levelManager;
            entityManager = _entityManager;
            playerActorGroup =
                entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().AllOf(typeof(PlayerFlag))));
            nonPlayerGroup =
                entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().NoneOf(typeof(PlayerFlag))));
        }

        #endregion

        #region Methods

        public void SaveGame()
        {
            JObject gameSave = CreateGameSaveJObject();
            SaveToDisc(gameSave);
        }

        public GameSave LoadGame()
        {
            GameSave gameSave = new GameSave(LoadFromDiscJObject(), entityManager);
            return gameSave;
        }

        private JObject CreateGameSaveJObject()
        {
            JObject gameSave = new JObject();

            if (playerActorGroup.Count > 1)
            {
                Exception exception = new MoreThanOnePlayerException("More than one Player registered");
                exception.Data.Add("EntityManager", entityManager);
                exception.Data.Add("LevelManager", levelManager);
                throw exception;
            }

            gameSave["DateTime"] = JToken.FromObject(DateTime.Now);

            Entity playerEntity = playerActorGroup.GetEntities()[0];

            gameSave["Player"] = playerEntity.ToJson();

            Entity[] nonPlayerEntities = nonPlayerGroup.GetEntities();

            JArray entitiesJArray = new JArray();
            foreach (Entity entity in nonPlayerEntities)
            {
                entitiesJArray.Add(entity.ToJson());
            }

            gameSave["Entities"] = entitiesJArray;
            gameSave["Level"] = levelManager.SaveLevel();
            return gameSave;
        }

        private JObject LoadFromDiscJObject()
        {
            string saveName = SaveName;
            string savePath = GetSavePath();

            if (!Directory.Exists(savePath))
            {
                throw new DirectoryNotFoundException("No directory at save path: " + savePath + " to load from");
            }

            StreamReader sr = new StreamReader(savePath + saveName);
            string fileContents = sr.ReadToEnd();
            sr.Close();

            JObject gameSaveJObject = JObject.Parse(fileContents);
            return gameSaveJObject;
        }

        private void SaveToDisc(JObject _gameSave)
        {
            string saveName = SaveName;
            string savePath = GetSavePath();

            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }

            using (FileStream fs = new FileStream(savePath + saveName, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.Write(_gameSave);
                }
            }
        }

        private string GetSavePath()
        {
            return Application.persistentDataPath + SaveDirectory;
        }

        #endregion

    }
}
