﻿using System;
using System.Collections.Generic;
using Ecs.Core;
using Roguelike.Scripts.Abilities;
using Roguelike.Scripts.Attributes;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.Ecs.Systems;
using Roguelike.Scripts.EntitiesGeneration;
using Roguelike.Scripts.StatusEffects;
using Roguelike.Scripts.StatusEffects.InfinityStatusEffects;
using Roguelike.Scripts.StatusEffects.InstantStatusEffects;
using Roguelike.Scripts.StatusEffects.TimedStatusEffects;
using Roguelike.Scripts.Utilities;
using Roguelike.Scripts.WorldUserInterface;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Roguelike.Scripts.Ecs
{
    //TODO наверное сделать систему глобальных эвентов? Типо когда любой из энтити двигается или при регистрации
    //TODO добаить систему инициализатор, которая при регистрации энтити будет выаолнять некоторые действия
    //TODO например для GOComponent ставить его в локал позишн, я для хеадинг разворачивать спрайт
    public sealed class World : MonoBehaviour
    {

        #region Variables

        private static World instance;
        public static World Instance
        {
            get { return instance; }
        }

        private EntityManager entityManager;
        public EntityManager EntityManager
        {
            get { return entityManager; }
        }

        private SystemManager systemManager;
        public SystemManager SystemManager
        {
            get { return systemManager; }
        }

        private SavesManager savesManager;
        public SavesManager SavesManager => savesManager;

        [SerializeField]
        private LevelManager levelManager;
        public LevelManager LevelManager
        {
            get { return levelManager; }
        }

        [SerializeField]
        private EntitiesGenerator entitiesGenerator;
        public EntitiesGenerator EntitiesGenerator => entitiesGenerator;

        [SerializeField]
        private ResourcesManager resourcesManager;
        public ResourcesManager ResourcesManager
        {
            get { return resourcesManager; }
        }

        private FovMapManager fovMapManager;
        public FovMapManager FovMapManager => fovMapManager;

        private GameManager gameManager;
        public GameManager GameManager => gameManager;

        [SerializeField]
        private UserInterfaceManager userInterfaceManager;
        public UserInterfaceManager UserInterfaceManager => userInterfaceManager;

        #endregion

        #region MonoBehaviour methods

        private void Awake()
        {
            instance = this;

            entityManager = new EntityManager();
            systemManager = new SystemManager();
            //levelManager = new LevelManager();    //Initialized from inspector
            savesManager = new SavesManager(levelManager, entityManager);
            //resourcesManager = new ResourcesManager();    //Initialized from inspector
            entitiesGenerator = new EntitiesGenerator(entityManager, resourcesManager);
            fovMapManager = new FovMapManager(levelManager, resourcesManager);
            gameManager = new GameManager(entityManager, levelManager, entitiesGenerator, savesManager, fovMapManager);
            //userInterfaceManager = new UserInterfaceManager();      //Initialized from inspector

            //TODO temp
            //systemManager.RegisterUpdateSystem(new GameLoopSystem(0, EntityManager));
            systemManager.RegisterUpdateSystem(new MovementSystem(1, EntityManager, LevelManager));
            systemManager.RegisterUpdateSystem(new AnimationSystem(2, EntityManager));
            systemManager.RegisterUpdateSystem(new CameraSystem(3, entityManager, resourcesManager));
            systemManager.RegisterUpdateSystem(new FieldOfViewSystem(4, entityManager, levelManager, fovMapManager));
            systemManager.RegisterUpdateSystem(new WorldUserInterfaceSystem(5, entityManager, resourcesManager));


            //TODO temp
            abilityProcessor = new AbilityProcessor(LevelManager);
        }

        //TODO temp
        private AbilityProcessor abilityProcessor;

        private void Update()
        {
            InputManager.ProcessInput();

            if (gameManager.GameState != GameState.Playing)
            {
                return;
            }

            systemManager.ProcessUpdateSystems(Time.deltaTime, entityManager);

            if (Input.GetKeyDown(KeyCode.M))
            {
                entityManager.DisposeAllEntities();
            }

            if (Input.GetKeyDown(KeyCode.N))
            {
                levelManager.DisposeLevel();
            }

            if (Input.GetKeyDown(KeyCode.K))
            {
                savesManager.SaveGame();
            }

            if (Input.GetKeyDown(KeyCode.J))
            {
                gameManager.MoveToTheNextLevel();
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                AttributeModifierEveryTurnStatusEffect attributeModifierEveryTurnStatusEffect = new AttributeModifierEveryTurnStatusEffect(resourcesManager.ScrollsSprites.GetSprite(),
                    new AttributeModifier(10.0f, AttributeModifierType.Flat, AttributeType.Attack), 3);
                StatusEffectsComponent statusEffectsComponent =
                    gameManager.PlayerEntity.GetEntityComponent<StatusEffectsComponent>();
                statusEffectsComponent.ApplyStatusEffect(attributeModifierEveryTurnStatusEffect);
            }

            //TODO temp
            abilityProcessor.ProcessPlayerControllerEntity(GameManager.PlayerEntity, Time.deltaTime);
        }

        #endregion

    }
}
