﻿using System.Collections.Generic;
using Ecs.Core;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.WorldUserInterface.HealthAndEnergyBar;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Systems
{
    public class WorldUserInterfaceSystem : AbstractSystem
    {

        #region Nested classes

        private class HealthBarGroupCache
        {
            public readonly Entity Entity;
            public readonly HealthAttributeComponent HealthAttributeComponent;
            public readonly WorldHealthBarUi WorldHealthBarUi;

            public HealthBarGroupCache(Entity _entity, HealthAttributeComponent _healthAttributeComponent, WorldHealthBarUi _worldHealthBarUi)
            {
                Entity = _entity;
                HealthAttributeComponent = _healthAttributeComponent;
                WorldHealthBarUi = _worldHealthBarUi;
            }


        }

        #endregion

        #region Variables

        private WorldHealthBarUi worldHealthBarUiPrefab;

        private List<HealthBarGroupCache> healthBarGroupCache;

        #endregion

        #region Constructor

        public WorldUserInterfaceSystem(int _priority, EntityManager _entityManager, ResourcesManager _resourcesManager) : base(_priority, _entityManager)
        {
            worldHealthBarUiPrefab = _resourcesManager.WorldHealthBarUiPrefab;

            Group healthBarGroup = entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().AllOf(typeof(HealthAttributeComponent),
                typeof(GameObjectComponent))));
            healthBarGroup.GroupEntityAddedEvent.Add(OnHealthBarGroupEntityAdded);
            healthBarGroup.GroupEntityRemovedEvent.Add(OnHealthBarGroupEntityRemoved);

            healthBarGroupCache = new List<HealthBarGroupCache>();
        }

        private void OnHealthBarGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            GameObjectComponent gameObjectComponent = _entityEventArgs.Entity.GetEntityComponent<GameObjectComponent>();
            HealthAttributeComponent healthAttributeComponent =
                _entityEventArgs.Entity.GetEntityComponent<HealthAttributeComponent>();

            WorldHealthBarUi worldHealthBarUi =
                Object.Instantiate<WorldHealthBarUi>(worldHealthBarUiPrefab, gameObjectComponent.Transform);
            healthBarGroupCache.Add(new HealthBarGroupCache(_entityEventArgs.Entity, healthAttributeComponent, worldHealthBarUi));
        }

        private void OnHealthBarGroupEntityRemoved(Group _group, EntityEventArgs _entityEventArgs)
        {
            for (int i = 0; i < healthBarGroupCache.Count; i++)
            {
                if (ReferenceEquals(healthBarGroupCache[i].Entity, _entityEventArgs.Entity))
                {
                    HealthBarGroupCache cached = healthBarGroupCache[i];
                    healthBarGroupCache.Remove(cached);
                    if (cached.WorldHealthBarUi != null)
                    {
                        Object.Destroy(cached.WorldHealthBarUi.gameObject);
                    }
                    break;
                }
            }
        }

        #endregion

        #region Methods

        public override void OnProcessSystem(float _deltaTime)
        {
            for (int i = 0; i < healthBarGroupCache.Count; i++)
            {
                UpdateHealthValue(healthBarGroupCache[i]);
            }
        }

        private void UpdateHealthValue(HealthBarGroupCache _healthBarGroupCache)
        {
            _healthBarGroupCache.WorldHealthBarUi.SetHealthBarValue(_healthBarGroupCache.HealthAttributeComponent.GetPercentageHealthValue());
            
            ShadowMapVisibilityComponent shadowMapVisibilityComponent = _healthBarGroupCache.Entity.GetEntityComponent<ShadowMapVisibilityComponent>();
            if (shadowMapVisibilityComponent != null)
            {
                _healthBarGroupCache.WorldHealthBarUi.SetActive(shadowMapVisibilityComponent.IsVisible);
            }
        }

        #endregion
    }
}
