﻿using System.Collections.Generic;
using Ecs.Core;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.FieldOfView;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Systems
{
    //TODO скорее всего отдельный комопнент для тех энитти, которые скрываются туманом. это позволит в будущем реализовать подсветку энтити вне радиуса игрока
    public class FieldOfViewSystem : AbstractSystem
    {

        #region Variables

        private readonly LevelManager levelManager;
        private readonly FovMapManager fovMapManager;

        private readonly Group playerActorGroup;
        private readonly Group gridPositionChangedGroup;
        private readonly Group npcActorGroup;
        private readonly Group shadowMapProviderGroup;

        private Entity playerEntity;

        private readonly RecursiveShadowmap recursiveShadowmap;

        #endregion

        #region Properties

        #endregion

        #region Constructor

        public FieldOfViewSystem(int _priority, EntityManager _entityManager, LevelManager _levelManager, FovMapManager _fovMapManager) : base(_priority, _entityManager)
        {
            levelManager = _levelManager;
            fovMapManager = _fovMapManager;

            playerActorGroup =
                entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().AllOf(typeof(PlayerActorComponent), typeof(GridPositionComponent), typeof(ShadowMapProviderComponent))));
            playerActorGroup.GroupEntityAddedEvent.Add(PlayerActorGroupEntityAddedHandler);
            playerActorGroup.GroupEntityRemovedEvent.Add(PlayerActorGroupEntityRemoedHandler);

            gridPositionChangedGroup = _entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().AllOf(typeof(GridPositionChangedTempFlag))));
            gridPositionChangedGroup.GroupEntityAddedEvent.Add(OnGridPositionGroupEntityAdded);

            npcActorGroup = //Only shadow map flagged are hidden in fov
                entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().AllOf(typeof(ShadowMapVisibilityComponent), typeof(RendererComponent), typeof(GridPositionComponent))));

            Group shadowMapVisibilityGroup =
                entityManager.GetOrCreateGroup(
                    new Group(new ComponentsFilter().AllOf(typeof(ShadowMapVisibilityComponent), typeof(GridPositionComponent))));
            shadowMapVisibilityGroup.GroupEntityAddedEvent.Add(OnShadowMapVisibilityGroupEntityAdded);

            shadowMapProviderGroup = entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().AllOf(typeof(ShadowMapProviderComponent), typeof(GridPositionComponent))));
            shadowMapProviderGroup.GroupEntityAddedEvent.Add(OnShadowMapProviderGroupEntityAdded);

            recursiveShadowmap = new RecursiveShadowmap();
        }

        #endregion

        #region Methods

        public override void OnProcessSystem(float _deltaTime)
        {
        }

        private void PlayerActorGroupEntityRemoedHandler(Group _arg1, EntityEventArgs _arg2)
        {
            playerEntity = null;
        }

        private void PlayerActorGroupEntityAddedHandler(Group _group, EntityEventArgs _entityEventArgs)
        {
            playerEntity = _entityEventArgs.Entity;
        }

        private void OnShadowMapProviderGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            RecalculateShadowMap();
        }

        private void OnGridPositionGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            GridPositionChangedTempFlag gridPositionChangedTempFlag = _entityEventArgs.Entity.GetEntityComponent<GridPositionChangedTempFlag>();
            OnGridPositionChanged(gridPositionChangedTempFlag);
        }

        private void OnGridPositionChanged(GridPositionChangedTempFlag _gridPositionChangedTempFlag)
        {
            if (_gridPositionChangedTempFlag.Entity.Equals(playerEntity))
            {
                RecalculateShadowMap();
            }

            Entity[] npcEntities = npcActorGroup.GetEntities();
            for (int i = 0; i < npcEntities.Length; i++)
            {
                GridPositionComponent gridPositionComponent = npcEntities[i].GetEntityComponent<GridPositionComponent>();
                RendererComponent rendererComponent = npcEntities[i].GetEntityComponent<RendererComponent>();
                ShadowMapVisibilityComponent shadowMapVisibilityComponent =
                    npcEntities[i].GetEntityComponent<ShadowMapVisibilityComponent>();
                if (levelManager.CurrentLevel.GetCellProperties(gridPositionComponent.Position).IsObserved)
                {
                    rendererComponent.SetColor(SpriteColor.Opaque);
                    shadowMapVisibilityComponent.IsVisible = true;
                }
                else
                {
                    rendererComponent.SetColor(SpriteColor.Transparent);
                    shadowMapVisibilityComponent.IsVisible = false;
                }
            }
        }

        private void OnShadowMapVisibilityGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            ShadowMapVisibilityComponent shadowMapVisibilityComponent =
                _entityEventArgs.Entity.GetEntityComponent<ShadowMapVisibilityComponent>();
            GridPositionComponent gridPositionComponent =
                _entityEventArgs.Entity.GetEntityComponent<GridPositionComponent>();
            if (levelManager.CurrentLevel.GetCellProperties(gridPositionComponent.Position).IsObserved)
            {
                shadowMapVisibilityComponent.IsVisible = true;
            }
            else
            {
                shadowMapVisibilityComponent.IsVisible = false;
            }
        }

        private void RecalculateShadowMap()
        {
            CastShadowOnPreviouslyLitTiles(levelManager.CurrentLevel, recursiveShadowmap.LightenedArea);

            Entity[] entities = shadowMapProviderGroup.GetEntities();
            Vector2Int[] positions = new Vector2Int[entities.Length];
            float[] viewDistances = new float[entities.Length];
            for (int i = 0; i < entities.Length; i++)
            {
                positions[i] = entities[i].GetEntityComponent<GridPositionComponent>().Position;
                viewDistances[i] = entities[i].GetEntityComponent<ShadowMapProviderComponent>().ViewDistance;
            }

            recursiveShadowmap.ComputeVisibility(levelManager.CurrentLevel, positions, viewDistances);

            CastLightOnLightenedTiles(levelManager.CurrentLevel, recursiveShadowmap.LightenedArea);

            fovMapManager.UpdateShadowMapForCurrentLevel();
        }

        private void CastLightOnLightenedTiles(Level _level, List<Vector2Int> _tilesToLit)
        {
            for (int i = 0; i < _tilesToLit.Count; i++)
            {
                _level.CastLight(_tilesToLit[i]);
            }
        }

        private void CastShadowOnPreviouslyLitTiles(Level _level, List<Vector2Int> _tilesToShadow)
        {
            for (int i = 0; i < _tilesToShadow.Count; i++)
            {
                _level.CastShadow(_tilesToShadow[i]);
            }
        }

        #endregion

    }
}
