﻿using System.Collections.Generic;
using Ecs.Core;
using Roguelike.Scripts.Ecs.Components;

namespace Roguelike.Scripts.Ecs.Systems
{
    public class AnimationSystem : AbstractSystem
    {

        #region Nested classes

        private class CustomAnimatorCache
        {
            public readonly CustomAnimationComponent CustomAnimationComponent;
            public readonly RendererComponent RendererComponent;

            public CustomAnimatorCache(CustomAnimationComponent _customAnimationComponent, RendererComponent _rendererComponent)
            {
                CustomAnimationComponent = _customAnimationComponent;
                RendererComponent = _rendererComponent;
            }
        }

        #endregion

        #region Variables

        private readonly Dictionary<Entity, CustomAnimatorCache> customAnimatorCache;

        private float timer;

        private const float AnimationDeltaTime = 0.3f;

        #endregion

        #region Constructor

        public AnimationSystem(int _priority, EntityManager _entityManager) : base(_priority, _entityManager)
        {
            timer = 0.0f;
            Group customAnimatorGroup = new Group(new ComponentsFilter().AllOf(typeof(CustomAnimationComponent), typeof(RendererComponent), typeof(GameObjectComponent)));
            customAnimatorGroup = _entityManager.GetOrCreateGroup(customAnimatorGroup);
            customAnimatorGroup.GroupEntityAddedEvent.Add(OnCustomAnimatorGroupEntityAdded);
            customAnimatorGroup.GroupEntityRemovedEvent.Add(OnCustomAnimatorGroupEntityRemoved);
            customAnimatorCache = new Dictionary<Entity, CustomAnimatorCache>();
        }

        private void OnCustomAnimatorGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            customAnimatorCache.Add(_entityEventArgs.Entity, new CustomAnimatorCache(_entityEventArgs.Entity.GetEntityComponent<CustomAnimationComponent>(),
                _entityEventArgs.Entity.GetEntityComponent<RendererComponent>()));
        }

        private void OnCustomAnimatorGroupEntityRemoved(Group _group, EntityEventArgs _entityEventArgs)
        {
            customAnimatorCache.Remove(_entityEventArgs.Entity);
        }

        #endregion

        #region Methods

        public override void OnProcessSystem(float _deltaTime)
        {
            timer += _deltaTime;

            if (timer > AnimationDeltaTime)
            {
                foreach (Entity entity in customAnimatorCache.Keys)
                {
                    customAnimatorCache[entity].RendererComponent.SetSprite(customAnimatorCache[entity].CustomAnimationComponent.NextAnimationFrameSprite());
                }
                timer = 0.0f;
            }
        }

        #endregion

    }
}
