﻿using Ecs.Core;
using Roguelike.Scripts.Actions;
using Roguelike.Scripts.Actions.DirectionalActions;
using Roguelike.Scripts.Actions.LinearActions;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Systems
{
    public class ActionSubSystem
    {

        #region Variables

        private Entity currentActingEntity;
        public Entity CurrentActingEntity => currentActingEntity;

        private PlayerActorComponent playerActorComponent;

        private NpcActorComponent npcActorComponent;

        private AbilitiesSetComponent abilitiesSetComponent;

        #endregion

        #region Constructor

        public ActionSubSystem()
        {
            currentActingEntity = null;
            npcActorComponent = null;
            playerActorComponent = null;
        }

        #endregion

        #region Methods

        public void Reset(Entity _entity, PlayerActorComponent _playerActorComponent, NpcActorComponent _npcActorComponent, AbilitiesSetComponent _abilitiesSetComponent)
        {
            currentActingEntity = _entity;
            playerActorComponent = _playerActorComponent;
            npcActorComponent = _npcActorComponent;
            abilitiesSetComponent = _abilitiesSetComponent;
            if (abilitiesSetComponent.CurrentAction == null)
            {
                abilitiesSetComponent.ResetToDefaultAction();
            }
        }

        //TODO возвращать bool - ход завершен или нет
        public bool ProcessSystem(float _deltaTime)
        {
            if (playerActorComponent != null)
            {
                //Debug.Log("Processing player");
                return ProcessPlayerActor(currentActingEntity, abilitiesSetComponent, _deltaTime);
            }

            if (npcActorComponent != null)
            {
                //Debug.Log("Processing npc");
                return ProcessNpcActor(currentActingEntity, npcActorComponent, abilitiesSetComponent, _deltaTime);
            }

            return false;
        }

        //TODO система будет присваивать из компонента скиллов действие актору
        private bool ProcessPlayerActor(Entity _entity, AbilitiesSetComponent _abilitiesSetComponent, float _deltaTime)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                _abilitiesSetComponent.CurrentAction = _abilitiesSetComponent.AttackAction;
                AttackEntityAction act = (AttackEntityAction)_abilitiesSetComponent.AttackAction;
                act.SetDirection(Vector2Int.down);
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                _abilitiesSetComponent.CurrentAction = new LightningAction(new LineProperties(5f, true), new ComponentsFilter().AllOf(typeof(HealthAttributeComponent)));
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                World.Instance.EntityManager.DisposeEntity(_entity);
            }

            ActionResult actionResult = _abilitiesSetComponent.CurrentAction.ProcessAction(_deltaTime, _entity);

            if (actionResult.IsDone)
            {
                //_abilitiesSetComponent.CurrentAction.EndAction();
                _abilitiesSetComponent.ResetToDefaultAction();
                return true;
            }

            //TODO может добавить и проверять флаг isEndsTurn, мол если сфейлили, то все равно тратить ход
            if (actionResult.IsFailed)
            {
                //_abilitiesSetComponent.CurrentAction.EndAction();
                if (actionResult.AlternateAction != null)
                {
                    _abilitiesSetComponent.CurrentAction = actionResult.AlternateAction;
                }
                else
                {
                    _abilitiesSetComponent.ResetToDefaultAction();
                }
            }

            return false;
        }

        private bool ProcessNpcActor(Entity _entity, NpcActorComponent _actorComponent, AbilitiesSetComponent _abilitiesSetComponent, float _deltaTime)
        {
            if (_actorComponent.EntityAction == null)
            {
                _actorComponent.direction = Vector2Int.right;
                DirectionalAction directionalAction = _abilitiesSetComponent.MovementAction as DirectionalAction;
                directionalAction.SetDirection(_actorComponent.direction);
                //directionalAction.StartAction(_entity); //TODO не забыть про старт экшон когда буду писать логику нпц
                _actorComponent.EntityAction = directionalAction;
                //_actorComponent.EntityAction = new AttackEntityAction(_entity, _actorComponent.direction);
            }

            //if (_actorComponent.EntityAction.ActorEntity == null)
            //{
            //    _actorComponent.EntityAction.StartAction(_entity);
            //}

            ActionResult actionResult = _actorComponent.EntityAction.ProcessAction(_deltaTime, _entity);

            if (actionResult.IsDone)
            {
                DirectionalAction directionalAction = _abilitiesSetComponent.MovementAction as DirectionalAction;
                directionalAction.SetDirection(_actorComponent.direction);
                _actorComponent.EntityAction = directionalAction;
                //directionalAction.StartAction(_entity);
                //_actorComponent.EntityAction = new MoveEntityAction(_entity, _actorComponent.direction);
                //_actorComponent.EntityAction = new AttackEntityAction(_entity, _actorComponent.direction);
                return true;
            }

            if (actionResult.IsFailed)
            {
                if (_actorComponent.direction == Vector2Int.right)
                {
                    _actorComponent.direction = Vector2Int.left;
                }
                else
                {
                    _actorComponent.direction = Vector2Int.right;
                }
                DirectionalAction directionalAction = _abilitiesSetComponent.MovementAction as DirectionalAction;
                directionalAction.SetDirection(_actorComponent.direction);
                //directionalAction.StartAction(_entity);
                _actorComponent.EntityAction = directionalAction;
                //_actorComponent.EntityAction = new MoveEntityAction(_entity, _actorComponent.direction);
                //_actorComponent.EntityAction = new AttackEntityAction(_entity, _actorComponent.direction);
            }

            return false;
        }

        #endregion

    }
}
