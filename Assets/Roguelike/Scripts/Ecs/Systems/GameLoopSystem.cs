﻿using System.Collections.Generic;
using Ecs.Core;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Systems
{
    public class GameLoopSystem : AbstractSystem
    {

        #region Variables

        private readonly Group actorableEntitiesGroup;

        private readonly List<EnergyAttributeComponent> energyAttributeComponents;

        private readonly ActionSubSystem actionSubSystem;

        #endregion

        #region Constructor

        public GameLoopSystem(int _priority, EntityManager _entityManager) : base(_priority, _entityManager)
        {
            energyAttributeComponents = new List<EnergyAttributeComponent>();

            actorableEntitiesGroup = _entityManager.GetOrCreateGroup(new Group(new ComponentsFilter()
                .AllOf(typeof(EnergyAttributeComponent), typeof(AbilitiesSetComponent))
                .AnyOf(typeof(NpcActorComponent), typeof(PlayerActorComponent))));
            actorableEntitiesGroup.GroupEntityAddedEvent.Add(OnActorableEntitiesGroupEntityAdded);
            actorableEntitiesGroup.GroupEntityRemovedEvent.Add(OnActorableEntitiesGroupEntityRemoved);

            actionSubSystem = new ActionSubSystem();
        }



        #endregion

        #region Methods

        private void OnActorableEntitiesGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            energyAttributeComponents.Add(_entityEventArgs.Entity.GetEntityComponent<EnergyAttributeComponent>());
            if (_group.Count == 1)
            {
                {
                    ResetSubsystem(energyAttributeComponents[0].Entity, energyAttributeComponents[0].Entity.GetEntityComponent<PlayerActorComponent>(),
                        energyAttributeComponents[0].Entity.GetEntityComponent<NpcActorComponent>(), energyAttributeComponents[0].Entity.GetEntityComponent<AbilitiesSetComponent>());
                }
            }
        }

        private void OnActorableEntitiesGroupEntityRemoved(Group _group, EntityEventArgs _entityEventArgs)
        {
            energyAttributeComponents.Remove(_entityEventArgs.Entity.GetEntityComponent<EnergyAttributeComponent>());
            if (_entityEventArgs.Entity.HasEntityComponent<TurnFlag>())
            {
                entityManager.RemoveComponent<TurnFlag>(_entityEventArgs.Entity);

                if (energyAttributeComponents.Count > 0)
                {
                    energyAttributeComponents.Sort();
                    ResetSubsystem(energyAttributeComponents[0].Entity, energyAttributeComponents[0].Entity.GetEntityComponent<PlayerActorComponent>(),
                        energyAttributeComponents[0].Entity.GetEntityComponent<NpcActorComponent>(), energyAttributeComponents[0].Entity.GetEntityComponent<AbilitiesSetComponent>());
                }
            }
        }

        public override void OnProcessSystem(float _deltaTime)
        {
            if (energyAttributeComponents.Count == 0)
            {
                return;
            }

            if (!energyAttributeComponents[0].HasEnergyToAct())
            {
                GainEnergyAndResetActorSubSystem();
            }

            while (actionSubSystem.ProcessSystem(_deltaTime))
            {
                energyAttributeComponents[0].SpendEnergy();
                entityManager.RemoveComponent<TurnFlag>(energyAttributeComponents[0].Entity);
                energyAttributeComponents.Sort();
                GainEnergyAndResetActorSubSystem();
                if (energyAttributeComponents[0].Entity.HasEntityComponent<PlayerActorComponent>() ||
                    energyAttributeComponents[0].Entity.GetEntityComponent<ShadowMapVisibilityComponent>().IsVisible)
                {
                    return;
                }
            }
        }

        private void GainEnergyAndResetActorSubSystem()
        {
            if (!energyAttributeComponents[0].HasEnergyToAct())
            {
                float energyGain = energyAttributeComponents[0].Difference;

                for (int i = 0; i < energyAttributeComponents.Count; i++)
                {
                    energyAttributeComponents[i].GainEnergy(energyGain);
                }
            }
            for (int i = 0; i < energyAttributeComponents.Count; i++)
            {
                energyAttributeComponents[i].GainEnergy(0.01f);
            }

            if (energyAttributeComponents[0].HasEnergyToAct())
            {
                ResetSubsystem(energyAttributeComponents[0].Entity, energyAttributeComponents[0].Entity.GetEntityComponent<PlayerActorComponent>(),
                    energyAttributeComponents[0].Entity.GetEntityComponent<NpcActorComponent>(), energyAttributeComponents[0].Entity.GetEntityComponent<AbilitiesSetComponent>());
            }
        }

        private void ResetSubsystem(Entity _entity, PlayerActorComponent _playerActorComponent, NpcActorComponent _npcActorComponent, AbilitiesSetComponent _abilitiesSetComponent)
        {
            actionSubSystem.Reset(_entity, _playerActorComponent, _npcActorComponent, _abilitiesSetComponent);
            entityManager.AddComponent(energyAttributeComponents[0].Entity, new TurnFlag());
        }

        #endregion

    }
}