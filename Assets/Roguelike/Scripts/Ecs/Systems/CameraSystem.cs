﻿using Ecs.Core;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Systems
{
    public class CameraSystem : AbstractSystem
    {
        #region Variables

        private readonly CameraSetup cameraSetup;

        #endregion

        #region Constructor

        public CameraSystem(int _priority, EntityManager _entityManager, ResourcesManager _resourcesManager) : base(_priority, _entityManager)
        {
            Group playerActorTurnFlagGroup = _entityManager.GetOrCreateGroup(
                new Group(new ComponentsFilter().AllOf(typeof(PlayerActorComponent), typeof(TurnFlag))));
            playerActorTurnFlagGroup.GroupEntityAddedEvent.Add(OnGroupEntityAdded);
            playerActorTurnFlagGroup.GroupEntityRemovedEvent.Add(OnGroupEntityRemoved);

            cameraSetup = _resourcesManager.CameraSetup;
        }

        private void OnGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            GameObjectComponent gameObjectComponent =
                _entityEventArgs.Entity.GetEntityComponent<GameObjectComponent>();

            cameraSetup.CameraFollowTransform.SetParent(gameObjectComponent.Transform);
            cameraSetup.CameraFollowTransform.localPosition = Vector3.zero;
        }

        private void OnGroupEntityRemoved(Group _group, EntityEventArgs _entityEventArgs)
        {
            cameraSetup.CameraFollowTransform.SetParent(cameraSetup.CameraSetupTransform);
        }

        #endregion

        #region Methods

        public override void OnProcessSystem(float _deltaTime)
        {
        }

        #endregion

    }
}
