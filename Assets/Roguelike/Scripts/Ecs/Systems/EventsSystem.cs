﻿using Ecs.Core;

namespace Roguelike.Scripts.Ecs.Systems
{
    //TODO сделать словари вида гуид -> лист подписчиков, все это по типу эвента, то есть если подписчик 
    //TODO заинтересован только в эвенте от определенного энтити, то он получит только этот эвент
    //TODO если же от всех, то будет получать любой интересующий эвент
    public class EventsSystem : AbstractSystem {

        public EventsSystem(int _priority, EntityManager _entityManager) : base(_priority, _entityManager)
        {
        }

        public override void OnProcessSystem(float _deltaTime)
        {
            throw new System.NotImplementedException();
        }
    }
}
