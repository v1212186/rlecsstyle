﻿using System.Collections.Generic;
using Ecs.Core;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Systems
{
    public class MovementSystem : AbstractSystem
    {
        #region Nested classes

        private class MoverCache
        {
            public readonly EntityMoverComponent EntityMoverComponent;
            public readonly GameObjectComponent GameObjectComponent;

            public MoverCache(EntityMoverComponent _entityMoverComponent, GameObjectComponent _gameObjectComponent)
            {
                EntityMoverComponent = _entityMoverComponent;
                GameObjectComponent = _gameObjectComponent;
            }
        }

        #endregion

        #region Variables

        private readonly Dictionary<Entity, MoverCache> moverCache;

        private readonly LevelManager levelManager;

        #endregion

        #region Constructor

        public MovementSystem(int _priority, EntityManager _entityManager, LevelManager _levelManager) : base(_priority, _entityManager)
        {
            levelManager = _levelManager;

            Group gridPositionGroup = _entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().AllOf(typeof(GridPositionComponent))));
            gridPositionGroup.GroupEntityAddedEvent.Add(OnGridPositionGroupEntityAdded);
            gridPositionGroup.GroupEntityRemovedEvent.Add(OnGridPositionGroupEntityRemoved);

            Group gridPositionChangedGroup = _entityManager.GetOrCreateGroup(new Group(
                new ComponentsFilter().AllOf(typeof(GridPositionComponent), typeof(GridPositionChangedTempFlag))));
            gridPositionChangedGroup.GroupEntityAddedEvent.Add(OnGridPositionChangedGroupEntityAdded);

            Group entityMoverGroup =
                _entityManager.GetOrCreateGroup(new Group(new ComponentsFilter().AllOf(typeof(EntityMoverComponent), typeof(GameObjectComponent))));
            entityMoverGroup.GroupEntityAddedEvent.Add(EntityMoverGroupEntityAdded);
            entityMoverGroup.GroupEntityRemovedEvent.Add(EntityMoverGroupEntityRemoved);
            moverCache = new Dictionary<Entity, MoverCache>();

            Group headingGroup = _entityManager.GetOrCreateGroup(
                new Group(new ComponentsFilter().AllOf(typeof(HeadingComponent), typeof(HeadingChangedTempFlag), typeof(RendererComponent))));
            headingGroup.GroupEntityAddedEvent.Add(OnHeadingGroupEntityAdded);
        }



        #endregion

        #region Methods

        private void OnGridPositionGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            GridPositionComponent gridPositionComponent = _entityEventArgs.Entity.GetEntityComponent<GridPositionComponent>();
            levelManager.CurrentLevel.RegisterEntity(gridPositionComponent.Position, _entityEventArgs.Entity);
        }

        private void OnGridPositionGroupEntityRemoved(Group _group, EntityEventArgs _entityEventArgs)
        {
            GridPositionComponent gridPositionComponent = _entityEventArgs.Entity.GetEntityComponent<GridPositionComponent>();
            levelManager.CurrentLevel.UnregisterEntity(gridPositionComponent.Position, _entityEventArgs.Entity);
        }

        private void OnGridPositionChangedGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            GridPositionComponent gridPositionComponent =
                _entityEventArgs.Entity.GetEntityComponent<GridPositionComponent>();
            GridPositionChangedTempFlag gridPositionChangedTempFlag =
                _entityEventArgs.Entity.GetEntityComponent<GridPositionChangedTempFlag>();
            OnGridPositionChanged(gridPositionComponent, gridPositionChangedTempFlag);
        }

        private void OnGridPositionChanged(GridPositionComponent _gridPositionComponent, GridPositionChangedTempFlag _gridPositionChangedTempFlag)
        {
            levelManager.CurrentLevel.UnregisterEntity(_gridPositionComponent.Position, _gridPositionComponent.Entity);
            _gridPositionComponent.Position = _gridPositionChangedTempFlag.NewPosition;
            levelManager.CurrentLevel.RegisterEntity(_gridPositionComponent.Position, _gridPositionComponent.Entity);
        }

        private void OnHeadingGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            HeadingComponent headingComponent = _entityEventArgs.Entity.GetEntityComponent<HeadingComponent>();
            HeadingChangedTempFlag headingChangedTempFlag = _entityEventArgs.Entity.GetEntityComponent<HeadingChangedTempFlag>();
            headingComponent.Heading = headingChangedTempFlag.NewHeading;
            RendererComponent rendererComponent = _entityEventArgs.Entity.GetEntityComponent<RendererComponent>();
            if (headingComponent.Heading.x > 0)
            {
                rendererComponent.SetMirroring(false);
            }
            if (headingComponent.Heading.x < 0)
            {
                rendererComponent.SetMirroring(true);
            }
        }

        private void EntityMoverGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            moverCache.Add(_entityEventArgs.Entity, new MoverCache(_entityEventArgs.Entity.GetEntityComponent<EntityMoverComponent>(),
                _entityEventArgs.Entity.GetEntityComponent<GameObjectComponent>()));
        }

        private void EntityMoverGroupEntityRemoved(Group _group, EntityEventArgs _entityEventArgs)
        {
            moverCache.Remove(_entityEventArgs.Entity);
        }

        public override void OnProcessSystem(float _deltaTime)
        {
            foreach (Entity entity in moverCache.Keys)
            {
                EntityMoverComponent entityMoverComponent = moverCache[entity].EntityMoverComponent;

                GameObjectComponent gameObjectComponent = moverCache[entity].GameObjectComponent;

                Vector2 start = gameObjectComponent.LocalPosition;
                Vector2 end = entityMoverComponent.EndPosition;

                entityMoverComponent.Timer += _deltaTime;
                gameObjectComponent.SetTransformLocalPosition(Vector2.Lerp(start, end,
                    entityMoverComponent.MovementAnimationCurve.Evaluate(entityMoverComponent.Timer / EntityMoverComponent.MoveTime)));

                Vector2 currentPosition = gameObjectComponent.LocalPosition;

                if (Vector2.SqrMagnitude(end - currentPosition) <= EntityMoverComponent.DistanceThreshold)
                {
                    gameObjectComponent.SetTransformLocalPosition(end);

                    RemoveComponentCommandBuffer<EntityMoverComponent>(entity);
                }
            }
        }

        #endregion
    }
}
