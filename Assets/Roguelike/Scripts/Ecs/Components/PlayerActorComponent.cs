﻿using Ecs.Core;
using Newtonsoft.Json.Linq;

namespace Roguelike.Scripts.Ecs.Components
{
    public class PlayerActorComponent : EntityComponent {

        #region Constructor

        public PlayerActorComponent()
        {

        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            return;
        }

        #endregion
    }
}
