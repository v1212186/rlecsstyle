﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public class GridPositionComponent : EntityComponent
    {
        #region Variables

        public Vector2Int Position;

        #endregion

        #region Constructor

        private GridPositionComponent()
        {

        }

        public GridPositionComponent(Vector2Int _position)
        {
            Position = _position;
        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["posX"] = Position.x,
                ["posY"] = Position.y
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            Position.x = _jsonObject["posX"].ToObject<int>();
            Position.y = _jsonObject["posY"].ToObject<int>();
        }

        #endregion
    }
}
