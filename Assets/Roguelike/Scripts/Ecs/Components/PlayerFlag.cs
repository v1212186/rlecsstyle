﻿using Ecs.Core;
using Newtonsoft.Json.Linq;

namespace Roguelike.Scripts.Ecs.Components
{
    //TODO планируется что будет по сейву на каждый класс игрока, соответственно это можно использовать для идентификатора сейвов
    public class PlayerFlag : EntityComponent {

        #region Constructor

        public PlayerFlag()
        {

        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            return;
        } 

        #endregion
    }
}
