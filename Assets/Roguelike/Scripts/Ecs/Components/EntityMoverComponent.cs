﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public class EntityMoverComponent : EntityComponent
    {

        #region Variables

        public const float MoveTime = 0.35f;
        public const float DistanceThreshold = 0.01f;

        public float Timer;
        public Vector2Int EndPosition;
        public AnimationCurves.AnimationCurveTypes animationCurveType;
        private AnimationCurve movementAnimationCurveCache;
        public AnimationCurve MovementAnimationCurve
        {
            get
            {
                if (movementAnimationCurveCache == null)
                {
                    movementAnimationCurveCache = World.Instance.ResourcesManager.GetAnimationCurve(animationCurveType);
                }

                return movementAnimationCurveCache;
            }
        }

        #endregion

        #region Constructor

        private EntityMoverComponent()
        {

        }

        public EntityMoverComponent(Vector2Int _endPosition, AnimationCurves.AnimationCurveTypes _animationCurveType)
        {
            EndPosition = _endPosition;
            Timer = 0.0f;
            animationCurveType = _animationCurveType;
        }

        #endregion

        #region Methods

        public override void Dispose()
        {

        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["Timer"] = Timer,
                ["EndPositionX"] = EndPosition.x,
                ["EndPositionY"] = EndPosition.y,
                ["AnimationCurveType"] = JToken.FromObject(animationCurveType)
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            Timer = _jsonObject["Timer"].ToObject<float>();
            EndPosition.x = _jsonObject["EndPositionX"].ToObject<int>();
            EndPosition.y = _jsonObject["EndPositionY"].ToObject<int>();
            animationCurveType = _jsonObject["AnimationCurveType"].ToObject<AnimationCurves.AnimationCurveTypes>();
        }

        #endregion

    }
}
