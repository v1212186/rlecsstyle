﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public class CustomAnimationComponent : EntityComponent
    {

        #region Nested classes 

        public class Animation
        {
            #region Variables

            public int AnimationId;

            public Sprite[] Frames;

            #endregion

            #region Constructor

            public Animation(int _animationId, Sprite[] _frames)
            {
                AnimationId = _animationId;
                Frames = _frames;
            }

            #endregion
        }

        #endregion

        #region Variables

        private Animation animation;

        private int currentFrame;

        #endregion

        #region Constructor

        private CustomAnimationComponent()
        {

        }

        public CustomAnimationComponent(Animation _animation)
        {
            currentFrame = 0;
            animation = _animation;
        }

        #endregion

        #region Methods

        public Sprite NextAnimationFrameSprite()
        {
            currentFrame = (currentFrame + 1) % animation.Frames.Length;
            return animation.Frames[currentFrame];
        }

        public override void Dispose()
        {

        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["AnimationId"] = animation.AnimationId
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            int animationId = _jsonObject["AnimationId"].ToObject<int>();
            animation = World.Instance.ResourcesManager.HumanoidAnimations.GetHumanoidAnimationSprite(animationId);
        }

        #endregion

    }
}
