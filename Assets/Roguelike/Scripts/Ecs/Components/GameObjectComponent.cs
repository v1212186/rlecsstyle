﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Ecs.Managers;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public class GameObjectComponent : EntityComponent
    {

        #region Variables

        private GameObject gameObject;
        private GameObject GameObject
        {
            get
            {
                if (gameObject == null)
                {
                    Transform parentTransform =
                        World.Instance.ResourcesManager.GameGridStructure.GetParentTransform(gridObjectLayer);
                    gameObject = new GameObject(GameObjectName);
                    gameObject.transform.SetParent(parentTransform);
                    SetTransformLocalPosition(localPosition);
                }

                return gameObject;
            }
        }

        private Transform cachedTransform;

        public Transform Transform
        {
            get
            {
                if (cachedTransform == null)
                {
                    cachedTransform = GameObject.transform;
                }

                return cachedTransform;
            }
        }

        public string GameObjectName;

        private Vector2 localPosition;
        public Vector2 LocalPosition => localPosition;

        private GameGridStructure.GridObjectLayer gridObjectLayer;

        #endregion

        #region Constructor

        private GameObjectComponent()
        {

        }

        public GameObjectComponent(GameGridStructure.GridObjectLayer _gridObjectLayer, string _name = "GameObjectEntity") : this(_gridObjectLayer, Vector2Int.zero, _name)
        {
        }

        public GameObjectComponent(GameGridStructure.GridObjectLayer _gridObjectLayer, Vector2Int _initialPosition, string _name = "GameObjectEntity")
        {
            GameObjectName = _name;
            gridObjectLayer = _gridObjectLayer;
            localPosition = _initialPosition;
        }

        #endregion

        #region Methods

        public void SetTransformLocalPosition(Vector2 _position)
        {
            GameObject.transform.localPosition = new Vector3(_position.x, _position.y, 0.0f);
            localPosition = _position;
        }

        public void SetGameObjectName(string _name)
        {
            GameObjectName = _name;
            GameObject.name = GameObjectName;
        }

        public T GetComponent<T>() where T : Component
        {
            return GameObject.GetComponent<T>();
        }

        public T AddComponent<T>() where T : Component
        {
            return GameObject.AddComponent<T>();
        }

        public override void Dispose()
        {
            Object.Destroy(GameObject);
            gameObject = null;
            cachedTransform = null;
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["GameObjectName"] = GameObjectName,
                ["localPositionX"] = localPosition.x,
                ["localPositionY"] = localPosition.y,
                ["gridObjectLayer"] = JToken.FromObject(gridObjectLayer)
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            GameObjectName = _jsonObject["GameObjectName"].ToObject<string>();
            localPosition.x = _jsonObject["localPositionX"].ToObject<float>();
            localPosition.y = _jsonObject["localPositionY"].ToObject<float>();
            gridObjectLayer = _jsonObject["gridObjectLayer"].ToObject<GameGridStructure.GridObjectLayer>();
        }

        #endregion

    }
}
