﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Actions;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public class NpcActorComponent : EntityComponent
    {

        #region Variables

        public EntityAction EntityAction;

        //TODO temp
        public Vector2Int direction;

        #endregion

        #region Constructor

        public NpcActorComponent()
        {

        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["dirX"] = direction.x,
                ["dirY"] = direction.y,
                ["entityAction"] = EntityAction.ToJson()
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            direction.x = _jsonObject["dirX"].ToObject<int>();
            direction.y = _jsonObject["dirY"].ToObject<int>();

            Type movementActionType = _jsonObject["entityAction"]["Type"].ToObject<Type>();
            EntityAction = (EntityAction)Activator.CreateInstance(movementActionType);
            EntityAction.FromJson(_jsonObject["entityAction"].ToObject<JObject>());
        }

        #endregion
    }
}
