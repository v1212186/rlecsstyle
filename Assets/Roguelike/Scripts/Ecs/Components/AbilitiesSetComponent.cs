﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Actions;

namespace Roguelike.Scripts.Ecs.Components
{
    public class AbilitiesSetComponent : EntityComponent
    {

        #region Variables

        private EntityAction currentAction;

        /// <summary>
        /// Sets <see cref="EntityAction"/> as current and calls <see cref="EntityAction.OnActionFailOrCancel"/> method for previous
        /// </summary>
        public EntityAction CurrentAction
        {
            get { return currentAction; }
            set
            {
                if (currentAction == null)
                {
                    currentAction = value;
                    return;
                }

                if (currentAction.RecentActionResult.IsPerforming)
                {
                    return;
                }

                currentAction.OnActionFailOrCancel();
                currentAction = value;
            }
        }

        private EntityAction defaultAction;
        public EntityAction DefaultAction
        {
            set { defaultAction = value; }
        }

        public EntityAction MovementAction { get; set; }

        public EntityAction AttackAction { get; set; }

        #endregion

        #region Constructor

        public AbilitiesSetComponent()
        {

        }

        #endregion

        #region Methods

        public void ResetToDefaultAction()
        {
            CurrentAction = defaultAction;
        }

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["CurrentActionType"] = JToken.FromObject(currentAction.GetType()),
                ["DefaultActionType"] = JToken.FromObject(defaultAction.GetType()),
                ["movementAction"] = MovementAction.ToJson(),
                ["attackAction"] = AttackAction.ToJson()
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            Type currentActionType = _jsonObject["CurrentActionType"].ToObject<Type>();
            Type defaultActionType = _jsonObject["DefaultActionType"].ToObject<Type>();
            Type movementActionType = _jsonObject["movementAction"]["Type"].ToObject<Type>();
            Type attackActionType = _jsonObject["attackAction"]["Type"].ToObject<Type>();

            EntityAction movement = (EntityAction)Activator.CreateInstance(movementActionType);
            movement.FromJson(_jsonObject["movementAction"].ToObject<JObject>());
            MovementAction = movement;

            EntityAction attack = (EntityAction)Activator.CreateInstance(attackActionType);
            attack.FromJson(_jsonObject["attackAction"].ToObject<JObject>());
            AttackAction = attack;

            if (defaultActionType == movement.GetType())
            {
                defaultAction = movement;
            }

            if (defaultActionType == attack.GetType())
            {
                defaultAction = attack;
            }

            //TODO при восстановлении текущего действия чекать все сохраненные действия, скорее всего они будут в реодерабл листе, чтобы соответствовать положение на хотбаре
            if (currentActionType == movement.GetType())
            {
                currentAction = movement;
            }

            if (currentActionType == attack.GetType())
            {
                currentAction = attack;
            }
        }

        #endregion
    }
}
