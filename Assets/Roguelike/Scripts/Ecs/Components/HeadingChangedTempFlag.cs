﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public class HeadingChangedTempFlag : EntityComponent
    {

        #region Variables

        public Vector2Int NewHeading;

        #endregion

        #region Constructor

        private HeadingChangedTempFlag()
        {

        }

        public HeadingChangedTempFlag(Vector2Int _oldPos, Vector2Int _newPos)
        {
            int x = (int)Mathf.Clamp(_newPos.x - _oldPos.x, -1f, 1f);
            int y = (int)Mathf.Clamp(_newPos.y - _oldPos.y, -1f, 1f);
            NewHeading = new Vector2Int(x, y);
        }

        #endregion

        #region Methods

        public override void Dispose()
        {

        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["newHeadingX"] = NewHeading.x,
                ["newHeadingY"] = NewHeading.y
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            NewHeading.x = _jsonObject["newHeadingX"].ToObject<int>();
            NewHeading.y = _jsonObject["newHeadingY"].ToObject<int>();
        }

        #endregion
    }
}
