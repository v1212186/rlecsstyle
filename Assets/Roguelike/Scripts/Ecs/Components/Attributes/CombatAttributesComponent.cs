﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Attributes;

namespace Roguelike.Scripts.Ecs.Components.Attributes
{
    public class CombatAttributesComponent : EntityComponent, IAttributeComponent
    {

        #region Variables

        private AttackAttribute attackAttribute;
        public AttackAttribute AttackAttribute => attackAttribute;
        private DefenceAttribute defenceAttribute;
        public DefenceAttribute DefenceAttribute => defenceAttribute;
        private CriticalHitChanceAttribute criticalHitChanceAttribute;
        public CriticalHitChanceAttribute CriticalHitChanceAttribute => criticalHitChanceAttribute;
        private HitChanceAttribute hitChanceAttribute;
        public HitChanceAttribute HitChanceAttribute => hitChanceAttribute;

        #endregion

        #region Constructor

        private CombatAttributesComponent()
        {

        }

        public CombatAttributesComponent(AttackAttribute _attackAttribute, DefenceAttribute _defenceAttribute,
            CriticalHitChanceAttribute _criticalHitChanceAttribute, HitChanceAttribute _hitChanceAttribute)
        {
            attackAttribute = _attackAttribute;
            defenceAttribute = _defenceAttribute;
            criticalHitChanceAttribute = _criticalHitChanceAttribute;
            hitChanceAttribute = _hitChanceAttribute;
        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        public bool ApplyAttributeModifier(AttributeModifier _attributeModifier)
        {
            switch (_attributeModifier.AttributeType)
            {
                case AttributeType.Attack:
                    attackAttribute.AddModifier(_attributeModifier);
                    break;
                case AttributeType.Defence:
                    defenceAttribute.AddModifier(_attributeModifier);
                    break;
                case AttributeType.CriticalHit:
                    criticalHitChanceAttribute.AddModifier(_attributeModifier);
                    break;
                case AttributeType.HitChance:
                    hitChanceAttribute.AddModifier(_attributeModifier);
                    break;
            }

            return true;
        }

        public bool RemoveAttributeModifier(AttributeModifier _attributeModifier)
        {
            switch (_attributeModifier.AttributeType)
            {
                case AttributeType.Attack:
                    return attackAttribute.RemoveModifier(_attributeModifier);
                case AttributeType.Defence:
                    return defenceAttribute.RemoveModifier(_attributeModifier);
                case AttributeType.CriticalHit:
                    return criticalHitChanceAttribute.RemoveModifier(_attributeModifier);
                case AttributeType.HitChance:
                    return hitChanceAttribute.RemoveModifier(_attributeModifier);
                default: return false;
            }
        }

        public bool RemoveAllAttributeModifiersFromSource(FastGuid _sourceGuid)
        {
            bool isRemoved = false;
            isRemoved |= attackAttribute.RemoveAllAttributeModifiersFromSource(_sourceGuid);
            isRemoved |= defenceAttribute.RemoveAllAttributeModifiersFromSource(_sourceGuid);
            isRemoved |= criticalHitChanceAttribute.RemoveAllAttributeModifiersFromSource(_sourceGuid);
            isRemoved |= hitChanceAttribute.RemoveAllAttributeModifiersFromSource(_sourceGuid);
            return isRemoved;
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["attackAttribute"] = attackAttribute.ToJson(),
                ["defenceAttribute"] = defenceAttribute.ToJson(),
                ["criticalHitChanceAttribute"] = criticalHitChanceAttribute.ToJson(),
                ["hitChanceAttribute"] = hitChanceAttribute.ToJson()
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            attackAttribute = new AttackAttribute();
            attackAttribute.FromJson(_jsonObject["attackAttribute"].ToObject<JObject>());
            defenceAttribute = new DefenceAttribute();
            defenceAttribute.FromJson(_jsonObject["defenceAttribute"].ToObject<JObject>());
            criticalHitChanceAttribute = new CriticalHitChanceAttribute();
            criticalHitChanceAttribute.FromJson(_jsonObject["criticalHitChanceAttribute"].ToObject<JObject>());
            hitChanceAttribute = new HitChanceAttribute();
            hitChanceAttribute.FromJson(_jsonObject["hitChanceAttribute"].ToObject<JObject>());
        }

        #endregion


    }
}
