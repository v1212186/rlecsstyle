﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Attributes;

namespace Roguelike.Scripts.Ecs.Components.Attributes
{
    public class HealthAttributeComponent : EntityComponent, IAttributeComponent
    {

        #region Variables

        private HealthAttribute healthAttribute;

        #endregion

        #region Constructor

        private HealthAttributeComponent()
        {

        }

        public HealthAttributeComponent(HealthAttribute _healthAttribute)
        {
            healthAttribute = _healthAttribute;
        }

        #endregion

        #region Methods

        public bool ApplyAttributeModifier(AttributeModifier _attributeModifier)
        {
            healthAttribute.AddModifier(_attributeModifier);
            return true;
        }

        public bool RemoveAttributeModifier(AttributeModifier _attributeModifier)
        {
            return healthAttribute.RemoveModifier(_attributeModifier);
        }

        public bool RemoveAllAttributeModifiersFromSource(FastGuid _sourceGuid)
        {
            return healthAttribute.RemoveAllAttributeModifiersFromSource(_sourceGuid);
        }

        public void ChangeHealthValue(float _value)
        {
            healthAttribute.ChangeCurrentValue(_value);
        }

        public float GetCurrentHealthValue()
        {
            return healthAttribute.CurrentValue;
        }

        public float GetPercentageHealthValue()
        {
            return healthAttribute.CurrentPercentage;
        }

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["healthAttribute"] = healthAttribute.ToJson()
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            healthAttribute = new HealthAttribute();
            healthAttribute.FromJson(_jsonObject["healthAttribute"].ToObject<JObject>());
        }

        #endregion

    }
}
