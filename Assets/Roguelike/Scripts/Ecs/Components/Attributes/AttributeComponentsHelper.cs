﻿using Ecs.Core;
using Roguelike.Scripts.Attributes;

namespace Roguelike.Scripts.Ecs.Components.Attributes
{
    public static class AttributeComponentsHelper
    {

        #region Methods

        public static bool ApplyModifierToAttributeComponent(Entity _entity, AttributeModifier _attributeModifier)
        {
            switch (_attributeModifier.AttributeType)
            {
                case AttributeType.Health:
                    return _entity.GetEntityComponent<HealthAttributeComponent>().ApplyAttributeModifier(_attributeModifier);
                case AttributeType.Energy:
                    return _entity.GetEntityComponent<EnergyAttributeComponent>().ApplyAttributeModifier(_attributeModifier);

                case AttributeType.Attack:
                case AttributeType.Defence:
                case AttributeType.CriticalHit:
                case AttributeType.HitChance:
                    return _entity.GetEntityComponent<CombatAttributesComponent>().ApplyAttributeModifier(_attributeModifier);
                default: return false;
            }
        }

        public static bool RemoveModifierFromAttributeComponent(Entity _entity, AttributeModifier _attributeModifier)
        {
            switch (_attributeModifier.AttributeType)
            {
                case AttributeType.Health:
                    return _entity.GetEntityComponent<HealthAttributeComponent>()
                        .RemoveAttributeModifier(_attributeModifier);
                case AttributeType.Energy:
                    return _entity.GetEntityComponent<EnergyAttributeComponent>().RemoveAttributeModifier(_attributeModifier);

                case AttributeType.Attack:
                case AttributeType.Defence:
                case AttributeType.CriticalHit:
                case AttributeType.HitChance:
                    return _entity.GetEntityComponent<CombatAttributesComponent>().RemoveAttributeModifier(_attributeModifier);
                default: return false;
            }
        }

        //TODO когда появится инвентарь или статус эффект с множеством аттрибутов, протестить это
        public static bool RemoveModifierFromAttributeComponentBySource(Entity _entity, FastGuid _source)
        {
            bool isRemoved = false;

            if (_entity.HasEntityComponent<HealthAttributeComponent>())
            {
                isRemoved |= _entity.GetEntityComponent<HealthAttributeComponent>()
                   .RemoveAllAttributeModifiersFromSource(_source);
            }

            if (_entity.HasEntityComponent<EnergyAttributeComponent>())
            {
                isRemoved |= _entity.GetEntityComponent<EnergyAttributeComponent>()
                    .RemoveAllAttributeModifiersFromSource(_source);
            }

            if (_entity.HasEntityComponent<CombatAttributesComponent>())
            {
                isRemoved |= _entity.GetEntityComponent<CombatAttributesComponent>()
                    .RemoveAllAttributeModifiersFromSource(_source);
            }

            return isRemoved;
        }

        #endregion

    }
}
