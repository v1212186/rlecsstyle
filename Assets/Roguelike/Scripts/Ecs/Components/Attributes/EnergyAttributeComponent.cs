﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Attributes;

namespace Roguelike.Scripts.Ecs.Components.Attributes
{
    public class EnergyAttributeComponent : EntityComponent, IComparable<EnergyAttributeComponent>, IAttributeComponent
    {

        #region Variables

        private EnergyAttribute energyAttribute;

        public float Difference => energyAttribute.FinalValue - energyAttribute.CurrentValue;

        #endregion

        #region Constructor

        private EnergyAttributeComponent()
        {

        }
        
        public EnergyAttributeComponent(EnergyAttribute _energyAttribute)
        {
            energyAttribute = _energyAttribute;
        }

        #endregion

        #region Methods

        public void GainEnergy(float _value = +1.0f)
        {
            energyAttribute.ChangeCurrentValue(_value);
        }

        public void SpendEnergy()
        {
            energyAttribute.ChangeCurrentValue(-energyAttribute.FinalValue);
        }

        public bool HasEnergyToAct()
        {
            return energyAttribute.CurrentValue >= energyAttribute.FinalValue;
        }

        public bool ApplyAttributeModifier(AttributeModifier _attributeModifier)
        {
            energyAttribute.AddModifier(_attributeModifier);
            return true;
        }

        public bool RemoveAttributeModifier(AttributeModifier _attributeModifier)
        {
            return energyAttribute.RemoveModifier(_attributeModifier);
        }

        public bool RemoveAllAttributeModifiersFromSource(FastGuid _sourceGuid)
        {
            return energyAttribute.RemoveAllAttributeModifiersFromSource(_sourceGuid);
        }

        public override void Dispose()
        {

        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["energyAttribute"] = energyAttribute.ToJson()
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            energyAttribute = new EnergyAttribute();
            energyAttribute.FromJson(_jsonObject["energyAttribute"].ToObject<JObject>());
        }

        public int CompareTo(EnergyAttributeComponent _other)
        {
            float thisEnrg = Difference;
            float otherEnrg = _other.Difference;
            if (thisEnrg > otherEnrg)
            {
                return 1;
            }
            if (thisEnrg < otherEnrg)
            {
                return -1;
            }
            return 0;
        }

        #endregion

    }
}
