﻿using Roguelike.Scripts.Attributes;

namespace Roguelike.Scripts.Ecs.Components.Attributes
{
    public interface IAttributeComponent  {

        #region Methods

        bool ApplyAttributeModifier(AttributeModifier _attributeModifier);

        bool RemoveAttributeModifier(AttributeModifier _attributeModifier);

        #endregion

    }
}
