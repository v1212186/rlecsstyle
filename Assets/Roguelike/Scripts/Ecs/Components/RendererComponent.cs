﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.Enums;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public enum SpriteColor
    {
        Transparent, Opaque, SemiTransparent
    }

    public class RendererComponent : EntityComponent
    {

        #region Variables

        private SpriteRenderer spriteRendererCached;

        public SpriteRenderer SpriteRenderer
        {
            get
            {
                if (spriteRendererCached != null)
                {
                    return spriteRendererCached;
                }
                GameObjectComponent gameObjectComponent = Entity.GetEntityComponent<GameObjectComponent>();
                if (gameObjectComponent == null)
                {
                    return null;
                }
                else
                {
                    spriteRendererCached = gameObjectComponent.GetComponent<SpriteRenderer>();
                    if (spriteRendererCached == null)
                    {
                        spriteRendererCached = gameObjectComponent.AddComponent<SpriteRenderer>();
                        spriteRendererCached.flipX = !facingLeft;
                    }
                    SetSortingLayer();
                    ApplyColor();
                }

                return spriteRendererCached;
            }
        }

        private SortingLayers sortingLayer;

        private SpriteColor spriteColor = SpriteColor.Opaque;

        private bool facingLeft = true;

        #endregion

        #region Constructor

        private RendererComponent()
        {

        }

        public RendererComponent(SortingLayers _sortingLayer)
        {
            sortingLayer = _sortingLayer;
        }

        #endregion

        #region Methods

        public void SetSprite(Sprite _sprite)
        {
            SpriteRenderer.sprite = _sprite;
        }

        public void SetSortingLayer()
        {
            SpriteRenderer.sortingLayerName = sortingLayer.ToString();
        }

        public void SetColor(SpriteColor _spriteColor)
        {
            spriteColor = _spriteColor;
            ApplyColor();
        }

        private void ApplyColor()
        {
            Color color;
            switch (spriteColor)
            {
                case SpriteColor.Opaque:
                    color = Color.white;
                    break;
                case SpriteColor.Transparent:
                    color = Color.clear;
                    break;
                case SpriteColor.SemiTransparent:
                    color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
                    break;
                default:
                    color = Color.white;
                    break;
            }

            SpriteRenderer.color = color;
        }

        //By default sprites are facing left
        public void SetMirroring(bool _facingLeft)
        {
            facingLeft = _facingLeft;
            SpriteRenderer.flipX = !_facingLeft;
        }

        public override void Dispose()
        {
            if (spriteRendererCached != null)
            {
                Object.Destroy(SpriteRenderer);
            }
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["sortingLayer"] = JToken.FromObject(sortingLayer),
                ["spriteColor"] = JToken.FromObject(spriteColor),
                ["facingLeft"] = facingLeft
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            sortingLayer = _jsonObject["sortingLayer"].ToObject<SortingLayers>();
            spriteColor = _jsonObject["spriteColor"].ToObject<SpriteColor>();
            facingLeft = _jsonObject["facingLeft"].ToObject<bool>();
        }

        #endregion

    }
}
