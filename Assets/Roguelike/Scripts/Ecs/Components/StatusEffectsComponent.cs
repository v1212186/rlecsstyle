﻿using System;
using System.Collections.Generic;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using Roguelike.Scripts.StatusEffects;
using Roguelike.Scripts.StatusEffects.InfinityStatusEffects;
using Roguelike.Scripts.StatusEffects.InstantStatusEffects;
using Roguelike.Scripts.StatusEffects.TimedStatusEffects;

namespace Roguelike.Scripts.Ecs.Components
{
    public class StatusEffectsComponent : EntityComponent
    {

        #region Variables

        private readonly List<TimedStatusEffect> timedStatusEffects = new List<TimedStatusEffect>();
        public List<TimedStatusEffect> TimedStatusEffects => timedStatusEffects;

        private readonly List<InfinityStatusEffect> infinityStatusEffects = new List<InfinityStatusEffect>();
        public List<InfinityStatusEffect> InfinityStatusEffects => infinityStatusEffects;

        private Group turnGroup;

        #endregion

        #region Constructor

        public StatusEffectsComponent()
        {
            SubscribeToTurnProcessing();
        }

        ~StatusEffectsComponent()
        {
            UnsubscribeFromTurnProcessing();
        }

        #endregion

        #region Methods

        #region Turn processing

        //TODO вот точно такой же метод вынести в статик хелпер класс и передавать туда методы с нужной сигнатурой
        private void SubscribeToTurnProcessing()
        {
            turnGroup =
               World.Instance.EntityManager.GetOrCreateGroup(
                   new Group(new ComponentsFilter().AllOf(typeof(TurnFlag))));
            turnGroup.GroupEntityAddedEvent.Add(OnTurnGroupEntityAdded);
            turnGroup.GroupEntityRemovedEvent.Add(OnTurnGroupEntityRemoved);
        }

        private void UnsubscribeFromTurnProcessing()
        {
            turnGroup.GroupEntityAddedEvent.Remove(OnTurnGroupEntityAdded);
            turnGroup.GroupEntityRemovedEvent.Remove(OnTurnGroupEntityRemoved);
        }

        private void OnTurnGroupEntityAdded(Group _group, EntityEventArgs _entityEventArgs)
        {
            if (Entity.Equals(_entityEventArgs.Entity))
            {
                //Debug.Log("Started turn");
                ChangeTimedStatusEffectsDuration(-1);
            }
        }

        private void OnTurnGroupEntityRemoved(Group _group, EntityEventArgs _entityEventArgs)
        {
            if (Entity.Equals(_entityEventArgs.Entity))
            {
                //Debug.Log("Ended turn");
            }
        } 

        #endregion

        public void ChangeTimedStatusEffectsDuration(int _value)
        {
            for (int i = timedStatusEffects.Count - 1; i >= 0; i--)
            {
                timedStatusEffects[i].ChangeDuration(_value);
            }
            CheckTimedStatusEffectsForExpiration();
        }

        private void CheckTimedStatusEffectsForExpiration()
        {
            for (int i = timedStatusEffects.Count - 1; i >= 0; i--)
            {
                if (timedStatusEffects[i].CurrentDuration <= 0)
                {
                    RemoveTimedStatusEffect(timedStatusEffects[i]);
                }
            }
        }

        #region Applying status effects

        public bool ApplyStatusEffect(AbstractStatusEffect _statusEffect)
        {
            bool isApplied = false;

            TimedStatusEffect timedStatusEffect = _statusEffect as TimedStatusEffect;
            if (timedStatusEffect != null)
            {
                isApplied = ApplyTimedStatusEffect(timedStatusEffect);
            }

            InfinityStatusEffect infinityStatusEffect = _statusEffect as InfinityStatusEffect;
            if (infinityStatusEffect != null)
            {
                isApplied = ApplyInfinityStatusEffect(infinityStatusEffect);
            }

            InstantStatusEffect instantStatusEffect = _statusEffect as InstantStatusEffect;
            if (instantStatusEffect != null)
            {
                isApplied = ApplyInstantStatusEffect(instantStatusEffect);
            }

            if (isApplied)
            {
                //TODO нотифаить (что) о том, что эффект применен
            }

            return isApplied;
        }

        private bool ApplyTimedStatusEffect(TimedStatusEffect _timedStatusEffect)
        {
            if (timedStatusEffects.Contains(_timedStatusEffect))
            {
                return false;
            }

            if (_timedStatusEffect.ApplyStatusEffect(Entity))
            {
                timedStatusEffects.Add(_timedStatusEffect);
                return true;
            }

            return false;
        }

        private bool ApplyInfinityStatusEffect(InfinityStatusEffect _infinityStatusEffect)
        {
            if (infinityStatusEffects.Contains(_infinityStatusEffect))
            {
                return false;
            }

            if (_infinityStatusEffect.ApplyStatusEffect(Entity))
            {
                infinityStatusEffects.Add(_infinityStatusEffect);
                return true;
            }

            return false;
        }

        private bool ApplyInstantStatusEffect(InstantStatusEffect _instantStatusEffect)
        {
            return _instantStatusEffect.ApplyStatusEffect(Entity);
        }

        #endregion

        #region Removing status effects

        public void RemoveStatusEffect(AbstractStatusEffect _statusEffect)
        {
            TimedStatusEffect timedStatusEffect = _statusEffect as TimedStatusEffect;
            if (timedStatusEffect != null)
            {
                RemoveTimedStatusEffect(timedStatusEffect);
            }

            InfinityStatusEffect infinityStatusEffect = _statusEffect as InfinityStatusEffect;
            if (infinityStatusEffect != null)
            {
                RemoveInfinityStatusEffect(infinityStatusEffect);
            }
        }

        private void RemoveTimedStatusEffect(TimedStatusEffect _timedStatusEffect)
        {
            if (_timedStatusEffect.RemoveStatusEffect(Entity))
            {
                timedStatusEffects.Remove(_timedStatusEffect);
                //TODO нотифайить
            }
        }

        private void RemoveInfinityStatusEffect(InfinityStatusEffect _infinityStatusEffect)
        {
            if (_infinityStatusEffect.RemoveStatusEffect(Entity))
            {
                infinityStatusEffects.Remove(_infinityStatusEffect);
                //TODO нотифайить
            }
        }

        public void RemoveAllStatusEffectsFromSource(FastGuid _sourceGuid)
        {
            for (int i = timedStatusEffects.Count - 1; i >= 0; i--)
            {
                if (timedStatusEffects[i].SourceGuid.Equals(_sourceGuid))
                {
                    RemoveTimedStatusEffect(timedStatusEffects[i]);
                }
            }
            for (int i = infinityStatusEffects.Count - 1; i >= 0; i--)
            {
                if (infinityStatusEffects[i].SourceGuid.Equals(_sourceGuid))
                {
                    RemoveInfinityStatusEffect(infinityStatusEffects[i]);
                }
            }
        }

        #endregion

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JArray arrayTimed = new JArray();
            foreach (TimedStatusEffect statusEffect in timedStatusEffects)
            {
                arrayTimed.Add(statusEffect.ToJson());
            }

            JArray arrayInfinity = new JArray();
            foreach (InfinityStatusEffect statusEffect in infinityStatusEffects)
            {
                arrayInfinity.Add(statusEffect.ToJson());
            }

            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["timedStatusEffects"] = arrayTimed,
                ["infinityStatusEffects"] = arrayInfinity
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            foreach (JObject effect in _jsonObject["timedStatusEffects"].Values<JObject>())
            {
                Type type = effect["Type"].ToObject<Type>();
                if (type == null)
                {
                    throw new NullReferenceException();
                }
                TimedStatusEffect statusEffect = (TimedStatusEffect)Activator.CreateInstance(type, true);
                statusEffect.FromJson(effect);
                timedStatusEffects.Add(statusEffect);
            }

            foreach (JObject effect in _jsonObject["infinityStatusEffects"].Values<JObject>())
            {
                Type type = effect["Type"].ToObject<Type>();
                if (type == null)
                {
                    throw new NullReferenceException();
                }
                InfinityStatusEffect statusEffect = (InfinityStatusEffect)Activator.CreateInstance(type, true);
                statusEffect.FromJson(effect);
                infinityStatusEffects.Add(statusEffect);
            }
        }

        #endregion
    }
}
