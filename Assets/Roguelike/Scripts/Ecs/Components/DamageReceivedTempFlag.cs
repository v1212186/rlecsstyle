﻿using Ecs.Core;
using Newtonsoft.Json.Linq;

namespace Roguelike.Scripts.Ecs.Components
{
    public class DamageReceivedTempFlag : EntityComponent {

        #region Constructor

        public DamageReceivedTempFlag()
        {

        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {

        } 

        #endregion
    }
}
