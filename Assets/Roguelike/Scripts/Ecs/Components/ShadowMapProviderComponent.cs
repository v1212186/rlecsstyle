﻿using Ecs.Core;
using Newtonsoft.Json.Linq;

namespace Roguelike.Scripts.Ecs.Components
{
    //TODO возможность сделать его статиком для того чтобы не просчитывать каждый раз для кучи объектов
    public class ShadowMapProviderComponent : EntityComponent
    {

        #region Variables

        private float viewDistance;
        public float ViewDistance => viewDistance;

        #endregion

        #region Constructors

        private ShadowMapProviderComponent()
        {

        }

        public ShadowMapProviderComponent(float _viewDistance)
        {
            viewDistance = _viewDistance;
        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["viewDistance"] = viewDistance
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            viewDistance = _jsonObject["viewDistance"].ToObject<float>();
        }

        #endregion

    }
}
