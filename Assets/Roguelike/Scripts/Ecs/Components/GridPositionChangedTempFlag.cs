﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public class GridPositionChangedTempFlag : EntityComponent
    {

        #region Variables

        public Vector2Int NewPosition;

        #endregion

        #region Properties

        #endregion

        #region Constructor

        private GridPositionChangedTempFlag()
        {

        }

        public GridPositionChangedTempFlag(Vector2Int _newPosition)
        {
            NewPosition = _newPosition;
        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["NewPositionX"] = NewPosition.x,
                ["NewPositionY"] = NewPosition.y
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            NewPosition.x = _jsonObject["NewPositionX"].ToObject<int>();
            NewPosition.y = _jsonObject["NewPositionY"].ToObject<int>();
        }

        #endregion

    }
}
