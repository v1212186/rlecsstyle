﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Roguelike.Scripts.Ecs.Components
{
    public class HeadingComponent : EntityComponent
    {

        #region Variables

        private Vector2Int heading;

        public Vector2Int Heading
        {
            get { return heading; }
            set { heading = value; }
        }


        #endregion

        #region Constructor

        private HeadingComponent()
        {

        }

        public HeadingComponent(Vector2Int _heading)
        {
            heading = _heading;
        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["headingX"] = heading.x,
                ["headingY"] = heading.y
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            heading.x = _jsonObject["headingX"].ToObject<int>();
            heading.y = _jsonObject["headingY"].ToObject<int>();
        }

        #endregion
    }
}
