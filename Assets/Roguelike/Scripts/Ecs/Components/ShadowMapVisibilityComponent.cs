﻿using Ecs.Core;
using Newtonsoft.Json.Linq;

namespace Roguelike.Scripts.Ecs.Components
{
    public class ShadowMapVisibilityComponent : EntityComponent {

        #region Variables

        public bool IsVisible;

        #endregion

        #region Constructor

        public ShadowMapVisibilityComponent()
        {

        }

        #endregion

        #region Methods

        public override void Dispose()
        {
        }

        protected override JObject OnToJson()
        {
            JObject jObject = new JObject
            {
                ["Type"] = JToken.FromObject(GetType()),
                ["IsHidden"] = IsVisible
            };
            return jObject;
        }

        protected override void OnFromJson(JObject _jsonObject)
        {
            IsVisible = _jsonObject["IsHidden"].ToObject<bool>();
        }
        #endregion
    }
}
