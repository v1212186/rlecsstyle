﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace Roguelike.Scripts.DynamicTiles
{
    public class DynamicTile : TileBase
    {
        //bitmasks https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673
        [SerializeField]
        private Sprite[] sprites;
        public Sprite[] Sprites
        {
            get
            {
#if UNITY_EDITOR
                if (sprites == null || sprites.Length != 16)
                {
                    Debug.LogError("No sprites on dynamic tile " + this);
                }
#endif
                return sprites;
            }
        }

        public override void RefreshTile(Vector3Int _position, ITilemap _tilemap)   //Refreshing neighbour tiles
        {
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    Vector3Int pos = new Vector3Int(_position.x + i, _position.y + j, _position.z);
                    if (TileValue(_tilemap, pos))
                    {
                        _tilemap.RefreshTile(pos);
                    }
                }
            }
        }

        public override void GetTileData(Vector3Int _position, ITilemap _tilemap, ref TileData _tileData)
        {
            UpdateTile(_position, _tilemap, ref _tileData);
        }

        private void UpdateTile(Vector3Int _position, ITilemap _tileMap, ref TileData _tileData)
        {
            _tileData.transform = Matrix4x4.identity;
            _tileData.color = Color.white;

            int mask = TileValue(_tileMap, _position + new Vector3Int(0, 1, 0)) ? 1 : 0;
            mask += TileValue(_tileMap, _position + new Vector3Int(1, 0, 0)) ? 4 : 0;
            mask += TileValue(_tileMap, _position + new Vector3Int(0, -1, 0)) ? 8 : 0;
            mask += TileValue(_tileMap, _position + new Vector3Int(-1, 0, 0)) ? 2 : 0;

            int index = GetIndex((byte)mask);
            if (index >= 0 && index < sprites.Length && TileValue(_tileMap, _position))
            {
                _tileData.sprite = sprites[index];
                _tileData.transform = Matrix4x4.identity;
                _tileData.flags = TileFlags.LockTransform | TileFlags.LockColor;
                _tileData.colliderType = Tile.ColliderType.Sprite;
            }
        }

        private bool TileValue(ITilemap _tileMap, Vector3Int _position)
        {
            TileBase tile = _tileMap.GetTile(_position);
            return (tile != null && tile == this);
        }

        private int GetIndex(byte _mask)
        {
            if (_mask > 15)
            {
                return -1;
            }
            return _mask;
        }
    }
}