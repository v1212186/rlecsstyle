﻿using System;

namespace Roguelike.Scripts.Attributes
{
    [Serializable]
    public class AttackAttribute : AbstractAttribute
    {
        #region Constructor

        public AttackAttribute() : base()
        {
            attributeType = AttributeType.Attack;
        }

        public AttackAttribute(float _baseValue) : base(_baseValue)
        {
            attributeType = AttributeType.Attack;
        }

        #endregion

    }
}
