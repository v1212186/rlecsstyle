﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;

namespace Roguelike.Scripts.Attributes
{

    public enum AttributeModifierType
    {
        Flat = 0, PercentAdd = 1, PercentMult = 2
    }

    public class AttributeModifier : IJsonSerializable, IEquatable<AttributeModifier>, IComparable<AttributeModifier>
    {
        #region Varibles
        private float value;
        public float Value
        {
            get { return value; }
            set { this.value = value; }
        }
        private AttributeModifierType attributeModifierType;
        public AttributeModifierType AttributeModifierType => attributeModifierType;
        private AttributeType attributeType;
        public AttributeType AttributeType => attributeType;
        private int order;
        public int Order => order;
        private FastGuid guid;  //Used for comparison
        public FastGuid Guid => guid;
        private FastGuid? sourceGuid;   //Used to remove all modifiers from source
        public FastGuid? SourceGuid => sourceGuid;

        #endregion

        #region Constructors

        public AttributeModifier(JObject _jObject)
        {
            FromJson(_jObject);
        }

        public AttributeModifier(float _value, AttributeModifierType _attributeModifierType, AttributeType _attributeType, FastGuid? _sourceGuid)
        {
            value = _value;
            attributeModifierType = _attributeModifierType;
            attributeType = _attributeType;
            order = (int)_attributeModifierType;
            guid = FastGuid.NewGuid();
            sourceGuid = _sourceGuid;
        }

        public AttributeModifier(float _value, AttributeModifierType _attributeModifierType, AttributeType _attributeType) :
            this(_value, _attributeModifierType, _attributeType, null)
        {
        }
        #endregion

        #region Methods

        public void SetSourceGuid(FastGuid? _sourceGuid)
        {
            sourceGuid = _sourceGuid;
        }

        public JObject ToJson()
        {
            JObject jObject = new JObject
            {
                ["value"] = value,
                ["attributeModifierType"] = JToken.FromObject(attributeModifierType),
                ["attributeType"] = JToken.FromObject(attributeType),
                ["order"] = order,
                ["guid"] = guid.GuidToJson(),
                ["sourceGuid"] = sourceGuid?.GuidToJson()
            };
            return jObject;
        }

        public void FromJson(JObject _jObject)
        {
            value = _jObject["value"].ToObject<float>();
            attributeModifierType = _jObject["attributeModifierType"].ToObject<AttributeModifierType>();
            attributeType = _jObject["attributeType"].ToObject<AttributeType>();
            order = _jObject["order"].ToObject<int>();
            guid = Guid.GuidFromJson(_jObject["guid"].ToObject<JObject>());
            sourceGuid = EcsUtilities.GuidFromJson(_jObject["sourceGuid"].ToObject<JObject>());
        }

        public int CompareTo(AttributeModifier _other)
        {
            if (order < _other.Order)
            {
                return -1;
            }
            if (order > _other.Order)
            {
                return 1;
            }
            return 0;   // if (_a.order == _b.order)
        }

        public bool Equals(AttributeModifier _other)
        {
            if (ReferenceEquals(null, _other))
            {
                return false;
            }

            if (ReferenceEquals(this, _other))
            {
                return true;
            }
            return guid.Equals(_other.guid);
        }

        public override bool Equals(object _obj)
        {
            if (ReferenceEquals(null, _obj))
            {
                return false;
            }

            if (ReferenceEquals(this, _obj))
            {
                return true;
            }

            if (_obj.GetType() != this.GetType())
            {
                return false;
            }
            return Equals((AttributeModifier)_obj);
        }

        public override int GetHashCode()
        {
            return guid.GetHashCode();
        }

        #endregion

    }
}
