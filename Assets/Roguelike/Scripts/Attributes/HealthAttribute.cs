﻿using System;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Roguelike.Scripts.Attributes
{
    [Serializable]
    public class HealthAttribute : AbstractAttribute, IJsonSerializable
    {

        #region Variables

        public const float ValueMinCap = 0.0f;

        [SerializeField]
        private float currentValue;
        public float CurrentValue { get { return currentValue; } }

        public float CurrentPercentage => currentValue / FinalValue;

        #endregion

        #region Constructors

        public HealthAttribute() : base()
        {
            attributeType = AttributeType.Health;
        }

        public HealthAttribute(float _baseValue) : base(_baseValue)
        {
            currentValue = _baseValue;
            attributeType = AttributeType.Health;
        }

        #endregion

        #region Methods

        public override void AddModifier(AttributeModifier _attributeModifier)
        {
            float currentPercentage = CurrentPercentage;
            base.AddModifier(_attributeModifier);
            currentValue = FinalValue * currentPercentage;
        }

        public override bool RemoveModifier(AttributeModifier _attributeModifier)
        {
            float currentPercentage = CurrentPercentage;
            if (base.RemoveModifier(_attributeModifier))
            {
                currentValue = FinalValue * currentPercentage;
                return true;
            }
            return false;
        }

        public override bool RemoveAllAttributeModifiersFromSource(FastGuid _sourceGuid)
        {
            float currentPercentage = CurrentPercentage;
            bool isRemoved = base.RemoveAllAttributeModifiersFromSource(_sourceGuid);
            currentValue = FinalValue * currentPercentage;
            return isRemoved;
        }

        public float ChangeCurrentValue(float _value)
        {
            return currentValue = Mathf.Clamp(currentValue + _value, ValueMinCap, FinalValue);
        }

        public new JObject ToJson()
        {
            JObject jObject = new JObject
            {
                ["currentValue"] = currentValue,
                ["baseClass"] = base.ToJson()
            };
            return jObject;
        }

        public new void FromJson(JObject _jObject)
        {
            currentValue = _jObject["currentValue"].ToObject<float>();

            JObject baseClass = _jObject["baseClass"].ToObject<JObject>();
            base.FromJson(baseClass);
        }

        #endregion

    }
}
