﻿using System;
using UnityEngine;

namespace Roguelike.Scripts.Attributes
{
    [Serializable]
    public class HitChanceAttribute : AbstractAttribute
    {
        #region Variables

        public const float ValueMaxCap = 100.0f;
        public const float ValueMinCap = 10.0f;
        public const float DefaultValue = 90.0f;

        #endregion

        #region Constructors

        public HitChanceAttribute() : base()
        {
            baseValue = DefaultValue;
            attributeType = AttributeType.HitChance;
        }

        #endregion

        #region Methods

        protected override float CalculateFinalValue()
        {
            return Mathf.Clamp(base.CalculateFinalValue(), ValueMinCap, ValueMaxCap);
        }

        #endregion
    }
}
