﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Ecs.Core;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Roguelike.Scripts.Attributes
{
    [Serializable]
    public abstract class AbstractAttribute : IJsonSerializable
    {

        #region Variables
        /// <summary>
        /// Initaial value, assigned from constructor
        /// </summary>
        [SerializeField]
        protected float baseValue;

        /// <summary>
        /// Initaial value, assigned from constructor
        /// </summary>
        public virtual float BaseValue
        {
            get { return baseValue; }
            set
            {
                baseValue = value;
                needToRecalculate = true;
            }
        }

        /// <summary>
        /// Final value, including all attribute modifiers
        /// </summary>
        public virtual float FinalValue
        {
            get
            {
                if (needToRecalculate)
                {
                    finalValue = CalculateFinalValue();
                    needToRecalculate = false;
                }
                return finalValue;
            }
        }

        /// <summary>
        /// Flag to recalculate final value afted added/removed attribute modifier
        /// </summary>
        protected bool needToRecalculate = true;

        /// <summary>
        /// Final value , including all attribute modifiers
        /// </summary>
        [SerializeField]
        protected float finalValue;

        [SerializeField]
        protected AttributeType attributeType;
        public AttributeType AttributeType { get { return attributeType; } }

        /// <summary>
        /// List of all modifiers
        /// </summary>
        protected readonly List<AttributeModifier> attributeModifiers;

        /// <summary>
        /// Immutable list of all attributes
        /// </summary>
        public readonly ReadOnlyCollection<AttributeModifier> attributeModifiersReadOnly;

        #endregion

        #region Constructors

        protected AbstractAttribute()
        {
            attributeModifiers = new List<AttributeModifier>();
            attributeModifiersReadOnly = attributeModifiers.AsReadOnly();
        }

        protected AbstractAttribute(float _baseValue) : this()
        {
            baseValue = _baseValue;
        }
        #endregion

        #region Methods

        public virtual void AddModifier(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType != attributeType)
            {
                throw new MismatchingAttributeTypeException(_attributeModifier.ToString());
            }
            needToRecalculate = true;
            attributeModifiers.Add(_attributeModifier);
            attributeModifiers.Sort();
        }

        public virtual bool RemoveModifier(AttributeModifier _attributeModifier)
        {
            if (_attributeModifier.AttributeType != attributeType)
            {
                throw new MismatchingAttributeTypeException(_attributeModifier.ToString());
            }

            if (attributeModifiers.Remove(_attributeModifier))
            {
                needToRecalculate = true;
                return true;
            }

            return false;
        }


        public virtual bool RemoveAllAttributeModifiersFromSource(FastGuid _sourceGuid)
        {
            bool isRemoved = false;

            for (int i = attributeModifiers.Count - 1; i >= 0; i--)
            {
                if (attributeModifiers[i].SourceGuid.HasValue)
                {
                    if (attributeModifiers[i].SourceGuid.Value.Equals(_sourceGuid))
                    {
                        needToRecalculate = true;
                        isRemoved = true;
                        attributeModifiers.RemoveAt(i);
                    }
                }
            }

            return isRemoved;
        }

        protected virtual float CalculateFinalValue()
        {
            finalValue = baseValue;
            float sumPercentAdd = 0;

            for (int i = 0; i < attributeModifiers.Count; i++)
            {
                switch (attributeModifiers[i].AttributeModifierType)
                {
                    case AttributeModifierType.Flat:
                        finalValue += attributeModifiers[i].Value;
                        break;
                    case AttributeModifierType.PercentAdd:
                        sumPercentAdd += attributeModifiers[i].Value;
                        if (i + 1 >= attributeModifiers.Count || attributeModifiers[i + 1].AttributeModifierType !=
                            AttributeModifierType.PercentAdd)
                        {
                            finalValue *= 1 + sumPercentAdd;
                            sumPercentAdd = 0;
                        }
                        break;
                    case AttributeModifierType.PercentMult:
                        finalValue *= 1 + attributeModifiers[i].Value;
                        break;
                    default: break;
                }
            }

            return Mathf.Round(finalValue);
        }

        public JObject ToJson()
        {
            JArray array = new JArray();
            foreach (AttributeModifier attributeModifier in attributeModifiers)
            {
                array.Add(attributeModifier.ToJson());
            }

            JObject jObject = new JObject
            {
                ["baseValue"] = baseValue,
                ["finalValue"] = finalValue,
                ["needToRecalculate"] = needToRecalculate,
                ["attributeType"] = JToken.FromObject(attributeType),
                ["attributeModifiers"] = array
            };
            return jObject;
        }

        public void FromJson(JObject _jObject)
        {
            baseValue = _jObject["baseValue"].ToObject<float>();
            finalValue = _jObject["finalValue"].ToObject<float>();
            needToRecalculate = _jObject["needToRecalculate"].ToObject<bool>();
            attributeType = _jObject["attributeType"].ToObject<AttributeType>();

            JArray array = _jObject["attributeModifiers"].ToObject<JArray>();
            foreach (JToken token in array)
            {
                AttributeModifier attributeModifier = new AttributeModifier(token.ToObject<JObject>());
                attributeModifiers.Add(attributeModifier);
            }
        }

        #endregion
    }
}
