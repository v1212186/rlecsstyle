﻿using System;

namespace Roguelike.Scripts.Attributes
{
    [Serializable]
    public class DefenceAttribute : AbstractAttribute {
        #region Constructor

        public DefenceAttribute() : base()
        {
            attributeType = AttributeType.Defence;
        }

        public DefenceAttribute(float _baseValue) : base(_baseValue)
        {
            attributeType = AttributeType.Defence;
        }

        #endregion
    }
}
