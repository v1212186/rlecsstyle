﻿using System;
using UnityEngine;

namespace Roguelike.Scripts.Attributes
{
    [Serializable]
    public class CriticalHitChanceAttribute : AbstractAttribute {

        #region Variables

        public const float ValueMaxCap = 100.0f;
        public const float ValueMinCap = 1.0f;
        public const float DefaultValue = 15.0f;

        #endregion

        #region Constructors

        public CriticalHitChanceAttribute() : base()
        {
            baseValue = DefaultValue;
            attributeType = AttributeType.CriticalHit;
        }

        #endregion

        #region Methods

        protected override float CalculateFinalValue()
        {
            return Mathf.Clamp(base.CalculateFinalValue(), ValueMinCap, ValueMaxCap);
        }

        #endregion
    }
}
