﻿using System;

namespace Roguelike.Scripts.Attributes
{
    [Serializable]
    public class MismatchingAttributeTypeException : Exception
    {
        #region Constructor

        public MismatchingAttributeTypeException(string _message) : base(_message)
        {

        }

        #endregion
    }
}
