﻿namespace Roguelike.Scripts.Attributes
{
    /// <summary>
    /// Type of the attribute
    /// </summary>
    public enum AttributeType
    {
        Health, Energy, Attack, Defence, CriticalHit, HitChance
    }
}
