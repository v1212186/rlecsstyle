﻿using Ecs.Core;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Roguelike.Scripts.Attributes
{
    /// <summary>
    /// Lesser final value, faster entity acts
    /// </summary>
    public class EnergyAttribute : AbstractAttribute, IJsonSerializable
    {
        #region Variables

        private float currentValue;
        public float CurrentValue => currentValue;
        public const float DefaultValue = 100.0f;
        public const float ValueMinCap = 1.0f;

        #endregion

        #region Constructors

        public EnergyAttribute() : base()
        {
            baseValue = DefaultValue;
            currentValue = ValueMinCap;
            attributeType = AttributeType.Energy;
        }

        #endregion

        #region Methods

        public float GetBonusValue()
        {
            return baseValue - FinalValue;
        }

        protected override float CalculateFinalValue()
        {
            finalValue = baseValue;
            float sumPercentAdd = 0;

            for (int i = 0; i < attributeModifiers.Count; i++)
            {
                switch (attributeModifiers[i].AttributeModifierType)
                {
                    case AttributeModifierType.Flat:
                        finalValue -= attributeModifiers[i].Value;
                        break;
                    case AttributeModifierType.PercentAdd:
                        sumPercentAdd += attributeModifiers[i].Value;
                        if (i + 1 >= attributeModifiers.Count || attributeModifiers[i + 1].AttributeModifierType !=
                            AttributeModifierType.PercentAdd)
                        {
                            finalValue /= 1 + sumPercentAdd;
                            sumPercentAdd = 0;
                        }
                        break;
                    case AttributeModifierType.PercentMult:
                        finalValue /= 1 + attributeModifiers[i].Value;
                        break;
                    default: break;
                }
            }

            return Mathf.Round(Mathf.Clamp(finalValue, ValueMinCap, float.MaxValue));
        }

        public float ChangeCurrentValue(float _value)
        {
            return currentValue = Mathf.Clamp(currentValue + _value, ValueMinCap, FinalValue);
        }

        public new JObject ToJson()
        {
            JObject jObject = new JObject
            {
                ["currentValue"] = currentValue,
                ["baseClass"] = base.ToJson()
            };
            return jObject;
        }

        public new void FromJson(JObject _jObject)
        {
            currentValue = _jObject["currentValue"].ToObject<float>();

            JObject baseClass = _jObject["baseClass"].ToObject<JObject>();
            base.FromJson(baseClass);
        }

        #endregion

    }
}
