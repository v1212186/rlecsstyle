﻿using System.Collections.Generic;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.FieldOfView
{
    public class RecursiveShadowmap
    {

        #region Nested classes

        /// <summary>
        /// Immutable class for holding coordinate transform constants.  Bulkier than a 2D
        /// array of ints, but it's self-formatting if you want to log it while debugging.
        /// </summary>
        private class OctantTransform
        {
            public int xx { get; private set; }
            public int xy { get; private set; }
            public int yx { get; private set; }
            public int yy { get; private set; }

            public OctantTransform(int _xx, int _xy, int _yx, int _yy)
            {
                xx = _xx;
                xy = _xy;
                yx = _yx;
                yy = _yy;
            }

            public override string ToString()
            {
                // consider formatting in constructor to reduce garbage
                return $"[OctantTransform {xx,2:D} {xy,2:D} {yx,2:D} {yy,2:D}]";
            }
        }

        #endregion

        #region Variables

        public readonly List<Vector2Int> LightenedArea = new List<Vector2Int>();

        private OctantTransform[] octantTransforms =
        {
            new OctantTransform(1, 0, 0, 1), // 0 E-NE
            new OctantTransform(0, 1, 1, 0), // 1 NE-N
            new OctantTransform(0, -1, 1, 0), // 2 N-NW
            new OctantTransform(-1, 0, 0, 1), // 3 NW-W
            new OctantTransform(-1, 0, 0, -1), // 4 W-SW
            new OctantTransform(0, -1, -1, 0), // 5 SW-S
            new OctantTransform(0, 1, -1, 0), // 6 S-SE
            new OctantTransform(1, 0, 0, -1), // 7 SE-E
        };

        #endregion

        #region Properties

        #endregion

        #region Methods

        public void ComputeVisibility(Level _level, Vector2Int[] _lightProviderPositions, float[] _viewRadiuses)
        {
            LightenedArea.Clear();
            for (int i = 0; i < _lightProviderPositions.Length; i++)
            {
                ComputeVisibility(_level, _lightProviderPositions[i], _viewRadiuses[i]);
            }
        }

        private void AddCell(Vector2Int _pos)
        {
            if (!LightenedArea.Contains(_pos))
            {
                LightenedArea.Add(_pos);
            }
        }

        /// <summary>
        /// Lights up cells visible from the current position.  Clear all lighting before calling.
        /// </summary>
        /// <param name="_level">The cell grid definition.</param>
        /// <param name="_lightProviderPos">The player's position within the grid.</param>
        /// <param name="_viewRadius">Maximum view distance; can be a fractional value.</param>
        private void ComputeVisibility(Level _level, Vector2Int _lightProviderPos, float _viewRadius)
        {
            // Viewer's cell is always visible.
            AddCell(_lightProviderPos);

            int startColumn = 1;
            float leftViewSlope = 1.0f;
            float rightViewSlope = 0.0f;

            // Cast light into cells for each of 8 octants.
            //
            // The left/right inverse slope values are initially 1 and 0, indicating a diagonal
            // and a horizontal line.  These aren't strictly correct, as the view area is supposed
            // to be based on corners, not center points.  We only really care about one side of the
            // wall at the edges of the octant though.
            //
            // NOTE: depending on the compiler, it's possible that passing the octant transform
            // values as four integers rather than an object reference would speed things up.
            // It's much tidier this way though.
            for (int txidx = 0; txidx < octantTransforms.Length; txidx++)
            {
                CastLight(_level, _lightProviderPos, _viewRadius, startColumn, leftViewSlope, rightViewSlope, octantTransforms[txidx]);
            }
        }

        /// <summary>
        /// Recursively casts light into cells.  Operates on a single octant.
        /// </summary>
        /// <param name="_level">The cell grid definition.</param>
        /// <param name="_lightProviderPos">The player's position within the grid.</param>
        /// <param name="_viewRadius">The view radius; can be a fractional value.</param>
        /// <param name="_startColumn">Current column; pass 1 as initial value.</param>
        /// <param name="_leftViewSlope">Slope of the left (upper) view edge; pass 1.0 as
        ///   the initial value.</param>
        /// <param name="_rightViewSlope">Slope of the right (lower) view edge; pass 0.0 as
        ///   the initial value.</param>
        /// <param name="_txfrm">Coordinate multipliers for the octant transform.</param>
        /// Maximum recursion depth is (Ceiling(viewRadius)).
        private void CastLight(Level _level, Vector2Int _lightProviderPos, float _viewRadius,
            int _startColumn, float _leftViewSlope, float _rightViewSlope, OctantTransform _txfrm)
        {
            // Used for distance test.
            float viewRadiusSq = _viewRadius * _viewRadius;

            int viewCeiling = Mathf.CeilToInt(_viewRadius);

            // Set true if the previous cell we encountered was blocked.
            bool prevWasBlocked = false;

            // As an optimization, when scanning past a block we keep track of the
            // rightmost corner (bottom-right) of the last one seen.  If the next cell
            // is empty, we can use this instead of having to compute the top-right corner
            // of the empty cell.
            float savedRightSlope = -1;

            int xDim = _level.Dimensions.x;
            int yDim = _level.Dimensions.y;

            // Outer loop: walk across each column, stopping when we reach the visibility limit.
            for (int currentCol = _startColumn; currentCol <= viewCeiling; currentCol++)
            {
                int xc = currentCol;

                // Inner loop: walk down the current column.  We start at the top, where X==Y.
                //
                // TODO: we waste time walking across the entire column when the view area
                //   is narrow.  Experiment with computing the possible range of cells from
                //   the slopes, and iterate over that instead.
                for (int yc = currentCol; yc >= 0; yc--)
                {
                    // Translate local coordinates to grid coordinates.  For the various octants
                    // we need to invert one or both values, or swap X for Y.
                    int gridX = (int)_lightProviderPos.x + xc * _txfrm.xx + yc * _txfrm.xy;
                    int gridY = (int)_lightProviderPos.y + xc * _txfrm.yx + yc * _txfrm.yy;

                    // Range-check the values.  This lets us avoid the slope division for blocks
                    // that are outside the grid.
                    //
                    // Note that, while we will stop at a solid column of blocks, we do always
                    // start at the top of the column, which may be outside the grid if we're (say)
                    // checking the first octant while positioned at the north edge of the map.
                    if (gridX < 0 || gridX >= xDim || gridY < 0 || gridY >= yDim)
                    {
                        continue;
                    }

                    // Compute slopes to corners of current block.  We use the top-left and
                    // bottom-right corners.  If we were iterating through a quadrant, rather than
                    // an octant, we'd need to flip the corners we used when we hit the midpoint.
                    //
                    // Note these values will be outside the view angles for the blocks at the
                    // ends -- left value > 1, right value < 0.
                    float leftBlockSlope = (yc + 0.5f) / (xc - 0.5f);
                    float rightBlockSlope = (yc - 0.5f) / (xc + 0.5f);

                    // Check to see if the block is outside our view area.  Note that we allow
                    // a "corner hit" to make the block visible.  Changing the tests to >= / <=
                    // will reduce the number of cells visible through a corner (from a 3-wide
                    // swath to a single diagonal line), and affect how far you can see past a block
                    // as you approach it.  This is mostly a matter of personal preference.
                    if (rightBlockSlope > _leftViewSlope)
                    {
                        // Block is above the left edge of our view area; skip.
                        continue;
                    }
                    else if (leftBlockSlope < _rightViewSlope)
                    {
                        // Block is below the right edge of our view area; we're done.
                        break;
                    }

                    // This cell is visible, given infinite vision range.  If it's also within
                    // our finite vision range, light it up.
                    //
                    // To avoid having a single lit cell poking out N/S/E/W, use a fractional
                    // viewRadius, e.g. 8.5.
                    //
                    // TODO: we're testing the middle of the cell for visibility.  If we tested
                    //  the bottom-left corner, we could say definitively that no part of the
                    //  cell is visible, and reduce the view area as if it were a wall.  This
                    //  could reduce iteration at the corners.
                    float distanceSquared = xc * xc + yc * yc;
                    if (distanceSquared <= viewRadiusSq)
                    {
                        AddCell(new Vector2Int(gridX, gridY));
                    }

                    bool curBlocked = _level.GetCellProperties(new Vector2Int(gridX, gridY)).IsBlockingView;

                    if (prevWasBlocked)
                    {
                        if (curBlocked)
                        {
                            // Still traversing a column of walls.
                            savedRightSlope = rightBlockSlope;
                        }
                        else
                        {
                            // Found the end of the column of walls.  Set the left edge of our
                            // view area to the right corner of the last wall we saw.
                            prevWasBlocked = false;
                            _leftViewSlope = savedRightSlope;
                        }
                    }
                    else
                    {
                        if (curBlocked)
                        {
                            // Found a wall.  Split the view area, recursively pursuing the
                            // part to the left.  The leftmost corner of the wall we just found
                            // becomes the right boundary of the view area.
                            //
                            // If this is the first block in the column, the slope of the top-left
                            // corner will be greater than the initial view slope (1.0).  Handle
                            // that here.
                            if (leftBlockSlope <= _leftViewSlope)
                            {
                                CastLight(_level, _lightProviderPos, _viewRadius, currentCol + 1,
                                    _leftViewSlope, leftBlockSlope, _txfrm);
                            }

                            // Once that's done, we keep searching to the right (down the column),
                            // looking for another opening.
                            prevWasBlocked = true;
                            savedRightSlope = rightBlockSlope;
                        }
                    }
                }

                // Open areas are handled recursively, with the function continuing to search to
                // the right (down the column).  If we reach the bottom of the column without
                // finding an open cell, then the area defined by our view area is completely
                // obstructed, and we can stop working.
                if (prevWasBlocked)
                {
                    break;
                }
            }
        }

        #endregion

    }
}
