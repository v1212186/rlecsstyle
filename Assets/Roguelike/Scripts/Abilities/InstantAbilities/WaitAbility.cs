﻿using Ecs.Core;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Abilities.InstantAbilities
{
    public class WaitAbility : Ability
    {


        #region Constructor

        public WaitAbility(bool _spendsEnergy) : base(_spendsEnergy)
        {
        }

        #endregion

        #region Methods

        public override bool CanUse(Entity _actorEntity, Level _currentLevel)
        {
            return true;
        }

        protected override bool CanPerform(Entity _actorEntity, Level _currentLevel, Vector2Int _input,
            ref AbilityResult _abilityResult)
        {
            return true;
        }

        protected override void PerformAbility(Entity _actorEntity, Level _currentLevel, float _deltaTime,
            Vector2Int _input, ref AbilityResult _abilityResult)
        {
            _abilityResult.SetFlag(AblityResultFlag.Done);
        }

        protected override Vector2Int? GetInput(Entity _actorEntity, Level _currentLevel,
            ref AbilityResult _abilityResult)
        {
            _abilityResult.SetFlag(AblityResultFlag.Done);
            return null;
        }

        #endregion   
    }
}
