﻿using Ecs.Core;
using Roguelike.Scripts.Abilities.DirectionalAbilities;
using Roguelike.Scripts.Abilities.InstantAbilities;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Abilities
{
    public class AbilityProcessor
    {

        private readonly LevelManager levelManager;

        private  Ability moveAbilityPlayer;
        private  Ability moveAbilityNpc;

        public AbilityProcessor(LevelManager _levelManager)
        {
            levelManager = _levelManager;
            moveAbilityPlayer = new MoveAbility(true);
            moveAbilityNpc = new MoveAbility(true);
        }

        //Returns if current entity turn ended
        public bool ProcessPlayerControllerEntity(Entity _playerControlledEntity, float _deltaTime)
        {
            AbilityResult abilityResult =
                moveAbilityPlayer.ProcessAbility(_playerControlledEntity, levelManager.CurrentLevel, _deltaTime);

            if (abilityResult.IsDone)
            {
                //Действие выполнилось, потратить энергию
                Debug.Log("Player done action");
                return false;
            }

            if (abilityResult.IsFailed)
            {
                Debug.Log("Player failed action");
                //Действие провалилось, потратить энергию
                if (abilityResult.AlternateAbility != null)
                {
                    moveAbilityPlayer = abilityResult.AlternateAbility;
                }

                return true;
            }

            return false;
        }

        public void ProcessNpcEntity(Entity _npcEntity, float _deltaTime)
        {
            Vector2Int input = Vector2Int.zero;

            if (moveAbilityNpc.CanUse(_npcEntity, levelManager.CurrentLevel) && moveAbilityNpc.CanPerform(_npcEntity, levelManager.CurrentLevel, input))
            {
                AbilityResult abilityResult = moveAbilityNpc.ProcessAbility(_npcEntity, levelManager.CurrentLevel, _deltaTime, input);

                if (abilityResult.IsDone)
                {
                    //Действие выполнилось, потратить энергию
                    Debug.Log("Npc done action");
                }

                if (abilityResult.IsFailed)
                {
                    //Действие провалилось, потратить энергию
                    Debug.Log("Npc failed action");
                }
            }
        }
    }
}
