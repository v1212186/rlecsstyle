﻿using UnityEngine;

namespace Roguelike.Scripts.Abilities
{
    public enum AblityResultFlag
    {
        AwaitingInput, Checking, Performing, Done, Failed, None
    }

    public class AbilityResult
    {

        #region Variables

        private AblityResultFlag flag;

        #endregion

        #region Properties

        public Ability AlternateAbility { get; set; }
        public Vector2Int? SavedInput { get; set; }

        public bool IsAwaitingInput => flag == AblityResultFlag.AwaitingInput;
        public bool IsChecking => flag == AblityResultFlag.Checking;
        public bool IsPerforming => flag == AblityResultFlag.Performing;
        public bool IsDone => flag == AblityResultFlag.Done;
        public bool IsFailed => flag == AblityResultFlag.Failed;

        #endregion

        #region Constructors

        public AbilityResult() : this(AblityResultFlag.None)
        {

        }

        public AbilityResult(AblityResultFlag _ablityResultFlag) : this(_ablityResultFlag, null, null)
        {
            flag = _ablityResultFlag;
        }

        public AbilityResult(AblityResultFlag _ablityResultFlag, Ability _alternateAbility, Vector2Int? _savedInput)
        {
            flag = _ablityResultFlag;
            AlternateAbility = _alternateAbility;
            SavedInput = _savedInput;
        }

        #endregion

        #region Methods

        public AbilityResult SetFlag(AblityResultFlag _ablityResultFlag)
        {
            flag = _ablityResultFlag;
            return this;
        }

        #endregion

    }
}