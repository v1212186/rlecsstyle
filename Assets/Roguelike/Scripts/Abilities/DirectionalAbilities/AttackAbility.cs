﻿using Ecs.Core;
using Roguelike.Scripts.CombatSystem;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Components.Attributes;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Abilities.DirectionalAbilities
{
    public class AttackAbility : Ability
    {

        private ComponentsFilter usageFilter;
        private ComponentsFilter performFilter;

        public AttackAbility(bool _spendsEnergy) : base(_spendsEnergy)
        {
            usageFilter = new ComponentsFilter().AllOf(typeof(CombatAttributesComponent));
            performFilter = new ComponentsFilter().AllOf(typeof(HealthAttributeComponent));
        }

        public override bool CanUse(Entity _actorEntity, Level _currentLevel)
        {
            return usageFilter.IsEntityMatches(_actorEntity);
        }

        protected override bool CanPerform(Entity _actorEntity, Level _currentLevel, Vector2Int _input, ref AbilityResult _abilityResult)
        {
            Vector2Int targetPos = _actorEntity.GetEntityComponent<GridPositionComponent>()
                                       .Position + _input;

            //TODO может прокешировать тут энтити, выбранный для атаки?
            bool canPerform = World.Instance.LevelManager.CurrentLevel.IsWithinBounds(targetPos) &&
                              World.Instance.LevelManager.CurrentLevel.HasMatchingEntity(targetPos, performFilter);
            if (canPerform)
            {
                GridPositionComponent gridPositionComponent =
                    _actorEntity.GetEntityComponent<GridPositionComponent>();
                World.Instance.EntityManager.AddTempFlagComponent(_actorEntity, new HeadingChangedTempFlag(gridPositionComponent.Position,
                    gridPositionComponent.Position + _input));
                _abilityResult.SetFlag(AblityResultFlag.Performing);
            }
            else
            {
                _abilityResult.SetFlag(AblityResultFlag.Failed);
            }

            return canPerform;
        }

        protected override void PerformAbility(Entity _actorEntity, Level _currentLevel, float _deltaTime, Vector2Int _input,
            ref AbilityResult _abilityResult)
        {
            //TODO все что с анимацией связано вынести в компонент отдельный
            //timer += _deltaTime;
            //GameObjectComponent gameObjectComponent = _actorEntity.GetEntityComponent<GameObjectComponent>();
            //GridPositionComponent gridPositionComponent =
            //    _actorEntity.GetEntityComponent<GridPositionComponent>();
            //gameObjectComponent.SetTransformLocalPosition(Vector2.Lerp(gridPositionComponent.Position, gridPositionComponent.Position + direction.Value,
            //    AttackAnimationCurve.Evaluate(timer / AttackTime)));

            //if (timer >= AttackTime)
            //{
            //    //TODO не делать анимацию здесь, вынести в отдельный компонент, брать время анимации из константы (AttackTime), добавить возможность атаковать без ожидания анимации
            //    gameObjectComponent.SetTransformLocalPosition(new Vector3(gridPositionComponent.Position.x, gridPositionComponent.Position.y));
            //    recentActionResult.SetFlag(ActionResultFlags.Done);

            //    Vector2Int targetPos = gridPositionComponent.Position + direction.Value;

            //    Combat.Attack(_actorEntity, World.Instance.LevelManager.CurrentLevel.GetMatchingEntity(targetPos,
            //        canApplyInDirectionComponentsFilter));
            //}

            GridPositionComponent gridPositionComponent =
                _actorEntity.GetEntityComponent<GridPositionComponent>();
            Vector2Int targetPos = gridPositionComponent.Position + _input;

            Combat.Attack(_actorEntity, World.Instance.LevelManager.CurrentLevel.GetMatchingEntity(targetPos,
                performFilter));

            _abilityResult.SetFlag(AblityResultFlag.Done);
        }

        protected override Vector2Int? GetInput(Entity _actorEntity, Level _currentLevel, ref AbilityResult _abilityResult)
        {
            Vector2Int? directionalInput = InputManager.GetDirectionalInput();
            _abilityResult.SetFlag(directionalInput.HasValue ? AblityResultFlag.Checking : AblityResultFlag.AwaitingInput);
            return directionalInput;
        }
    }
}
