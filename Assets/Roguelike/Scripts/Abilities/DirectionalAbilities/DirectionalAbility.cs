﻿using Ecs.Core;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Abilities.DirectionalAbilities
{
    public abstract class DirectionalAbility : Ability {

        #region Variables



        #endregion

        #region Constructor

        protected DirectionalAbility(bool _spendsEnergy) : base(_spendsEnergy)
        {

        }

        #endregion

        #region Methods

       
        #endregion
    }
}
