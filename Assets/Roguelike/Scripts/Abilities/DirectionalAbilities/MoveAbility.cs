﻿using Ecs.Core;
using Roguelike.Scripts.Ecs;
using Roguelike.Scripts.Ecs.Components;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Abilities.DirectionalAbilities
{
    public class MoveAbility : Ability
    {
        private ComponentsFilter usageFilter;
        private ComponentsFilter performFilter;

        public MoveAbility(bool _spendsEnergy) : base(_spendsEnergy)
        {
            usageFilter = new ComponentsFilter().AllOf(typeof(GridPositionComponent));
            performFilter = new ComponentsFilter().AllOf(typeof(IsCollidableFlag));
        }

        public override bool CanUse(Entity _actorEntity, Level _currentLevel)
        {
            return usageFilter.IsEntityMatches(_actorEntity);
        }

        protected override bool CanPerform(Entity _actorEntity, Level _currentLevel, Vector2Int _input, ref AbilityResult _abilityResult)
        {
            Vector2Int targetPos = _actorEntity.GetEntityComponent<GridPositionComponent>()
                                       .Position + _input;

            bool canPerform = _currentLevel.IsWithinBounds(targetPos) &&
                              !_currentLevel.GetCellProperties(targetPos).IsCollidable &&
                              !_currentLevel.HasMatchingEntity(targetPos, performFilter);

            if (canPerform)
            {
                _abilityResult.SetFlag(AblityResultFlag.Performing);
            }
            else
            {
                _abilityResult.SetFlag(AblityResultFlag.Failed);
                _abilityResult.AlternateAbility = new AttackAbility(true);
                _abilityResult.SavedInput = _input;
            }
            return canPerform;
        }

        protected override void PerformAbility(Entity _actorEntity, Level _currentLevel, float _deltaTime, Vector2Int _input, ref AbilityResult _abilityResult)
        {
            GridPositionComponent gridPositionComponent =
                _actorEntity.GetEntityComponent<GridPositionComponent>();

            Vector2Int destinationPos = gridPositionComponent.Position + _input;

            //TODO вынести это в отдельный статик метод в мувемент системе
            EntityMoverComponent entityMoverComponent = _actorEntity.GetEntityComponent<EntityMoverComponent>();
            if (entityMoverComponent != null)
            {
                entityMoverComponent.EndPosition = destinationPos;
                entityMoverComponent.Timer = 0.0f;
            }
            else
            {
                World.Instance.EntityManager.AddComponent(_actorEntity,
                    new EntityMoverComponent(destinationPos, AnimationCurves.AnimationCurveTypes.Movement));
            }

            //TODO передавать энтити менеджера в конструктор? или в тело метода. может даже тупо передавать ссылку на мир
            //TODO когда изучу как работать с аттрибутами, скорее всего буду инжектить левел/менеджер или ворлд рантаймово
            World.Instance.EntityManager.AddTempFlagComponent(_actorEntity, new HeadingChangedTempFlag(gridPositionComponent.Position, destinationPos));
            World.Instance.EntityManager.AddTempFlagComponent(_actorEntity, new GridPositionChangedTempFlag(destinationPos));

            _abilityResult.SetFlag(AblityResultFlag.Done);
        }

        protected override Vector2Int? GetInput(Entity _actorEntity, Level _currentLevel,  ref AbilityResult _abilityResult)
        {
            Vector2Int? directionalInput = InputManager.GetDirectionalInput();
            _abilityResult.SetFlag(directionalInput.HasValue ? AblityResultFlag.Checking : AblityResultFlag.AwaitingInput);
            return directionalInput;
        }
    }
}
