﻿using System;
using Ecs.Core;
using Roguelike.Scripts.Ecs.Managers;
using Roguelike.Scripts.Levels.LevelData;
using UnityEngine;

namespace Roguelike.Scripts.Abilities
{
    public abstract class Ability
    {

        #region Variables

        //TODO сделать прайват и передавать в качестве рефа в остальные методы
        protected AbilityResult abilityResult;

        protected bool spendsEnergy;

        private Vector2Int? input;

        private bool isAbilityStarted;

        #endregion

        #region Properties

        public AbilityResult AbilityResult => abilityResult;

        public bool SpendsEnergy => spendsEnergy;

        #endregion

        #region Constructor

        protected Ability(bool _spendsEnergy)
        {
            abilityResult = new AbilityResult();
            spendsEnergy = _spendsEnergy;
        }

        #endregion

        #region Methods

        public AbilityResult ProcessAbility(Entity _actorEntity, Level _currentLevel, float _deltaTime)
        {
            if (!isAbilityStarted)
            {
                AbilityStart(_actorEntity, _currentLevel);
                abilityResult.SetFlag(AblityResultFlag.AwaitingInput);
            }

            if (abilityResult.IsAwaitingInput)
            {
                input = GetInput(_actorEntity, _currentLevel, ref abilityResult);
            }

            if (abilityResult.IsChecking)
            {
                if (!input.HasValue)
                {
                    throw new NullReferenceException("Can't check with null input value");
                }
                CanPerform(_actorEntity, _currentLevel, input.Value, ref abilityResult);
            }

            if (abilityResult.IsPerforming)
            {
                if (!input.HasValue)
                {
                    throw new NullReferenceException("Can't perform with null input value");
                }
                PerformAbility(_actorEntity, _currentLevel, _deltaTime, input.Value, ref abilityResult);
            }

            if (abilityResult.IsDone)
            {
                AbilityDone(_actorEntity, _currentLevel);
            }

            if (abilityResult.IsFailed)
            {
                AbilityFail(_actorEntity, _currentLevel);
            }

            return abilityResult;
        }

        public AbilityResult ProcessAbility(Entity _actorEntity, Level _currentLevel, float _deltaTime, Vector2Int _input)
        {
            if (!isAbilityStarted)
            {
                AbilityStart(_actorEntity, _currentLevel);
                input = _input;
            }

            if (abilityResult.IsPerforming)
            {
                if (!input.HasValue)
                {
                    throw new NullReferenceException("Can't perform with null input value");
                }
                PerformAbility(_actorEntity, _currentLevel, _deltaTime, input.Value, ref abilityResult);
            }

            if (abilityResult.IsDone)
            {
                AbilityDone(_actorEntity, _currentLevel);
            }

            if (abilityResult.IsFailed)
            {
                AbilityFail(_actorEntity, _currentLevel);
            }

            return abilityResult;
        }


        public abstract bool CanUse(Entity _actorEntity, Level _currentLevel);

        public bool CanPerform(Entity _actorEntity, Level _currentLevel, Vector2Int _input)
        {
            return CanPerform(_actorEntity, _currentLevel, _input, ref abilityResult);
        }

        protected abstract bool CanPerform(Entity _actorEntity, Level _currentLevel, Vector2Int _input, ref AbilityResult _abilityResult);

        protected abstract void PerformAbility(Entity _actorEntity, Level _currentLevel, float _deltaTime, Vector2Int _input, ref AbilityResult _abilityResult);

        protected abstract Vector2Int? GetInput(Entity _actorEntity, Level _currentLevel, ref AbilityResult _abilityResult);

        protected virtual void AbilityStart(Entity _actorEntity, Level _currentLevel)
        {
            isAbilityStarted = true;
        }

        protected virtual void AbilityDone(Entity _actorEntity, Level _currentLevel)
        {
            isAbilityStarted = false;
        }

        protected virtual void AbilityFail(Entity _actorEntity, Level _currentLevel)
        {
            isAbilityStarted = false;
        }

        public virtual void CancelAbility(Entity _actorEntity, Level _currentLevel)
        {
            isAbilityStarted = false;
        }

        #endregion

    }
}
