﻿using Newtonsoft.Json.Linq;

namespace Ecs.Core
{
    public static class EcsUtilities
    {

        #region Methods

        public static JObject GuidToJson(this FastGuid _fastGuid)
        {
            JObject jObject = new JObject();
            jObject["guidPart0"] = _fastGuid.guidPart0;
            jObject["guidPart1"] = _fastGuid.guidPart1;
            return jObject;
        }

        public static FastGuid GuidFromJson(this FastGuid _fastGuid, JObject _jObject)
        {
            long guidPart0 = _jObject["guidPart0"].ToObject<long>();
            long guidPart1 = _jObject["guidPart1"].ToObject<long>();
            return new FastGuid(guidPart0, guidPart1);
        }

        public static FastGuid? GuidFromJson(JObject _jObject)
        {
            if (_jObject == null)
            {
                return null;
            }
            long guidPart0 = _jObject["guidPart0"].ToObject<long>();
            long guidPart1 = _jObject["guidPart1"].ToObject<long>();
            return new FastGuid(guidPart0, guidPart1);
        }

        #endregion

    }
}
