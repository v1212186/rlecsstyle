﻿using System;
using System.Collections.Generic;

namespace Ecs.Core
{
    class NullGroupException : Exception
    {
        public NullGroupException(string _message) : base(_message)
        {

        }
    }

    public sealed partial class EntityManager
    {
        #region Variables

        private readonly List<Group> groups;

        #endregion

        #region Methods

        private void UpdateGroups<T>(Entity _entity) where T : EntityComponent
        {
            for (int i = 0; i < groups.Count; i++)
            {
                groups[i].UpdateGroup<T>(_entity);
            }
        }

        private void RemoveEntityFromGroups(Entity _entity)
        {
            for (int i = 0; i < groups.Count; i++)
            {
                groups[i].RemoveEntityFromGroup(_entity);
            }
        }

        private void AddEntityToGroups(Entity _entity)
        {
            for (int i = 0; i < groups.Count; i++)
            {
                groups[i].AddEntityToGroup(_entity);
            }
        }

        public Group GetOrCreateGroup(Group _group)
        {
            if (_group == null)
            {
                throw new NullGroupException("Can't retrieve null group");
            }

            for (int i = 0; i < groups.Count; i++)
            {
                if (Equals(groups[i], _group))
                {
                    return groups[i];
                }
            }
            groups.Add(_group);
            foreach (KeyValuePair<FastGuid, Entity> entity in entities)
            {
                _group.AddEntityToGroup(entity.Value);
            }
            return _group;
        }

        #endregion
    }
}