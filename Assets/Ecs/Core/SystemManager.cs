﻿using System.Collections.Generic;

namespace Ecs.Core
{
    public sealed class SystemManager
    {

        #region Variables

        private readonly List<AbstractSystem> updateSystems;

        #endregion

        #region Constructor

        public SystemManager()
        {
            updateSystems = new List<AbstractSystem>();
        }

        #endregion

        #region Methods

        public void ProcessUpdateSystems(float _deltaTime, EntityManager _entityManager)
        {
            for (int i = 0; i < updateSystems.Count; i++)
            {
                updateSystems[i].ProcessSystem(_deltaTime);
            }
        }

        public void RegisterUpdateSystem(AbstractSystem _system)
        {
            if (updateSystems.Contains(_system))
            {
                return;
            }
            updateSystems.Add(_system);
            updateSystems.Sort();
        }

        public T GetSystem<T>() where T : AbstractSystem
        {
            for (int i = 0; i < updateSystems.Count; i++)
            {
                if (updateSystems[i].GetType() == typeof(T))
                {
                    return updateSystems[i] as T;
                }
            }

            return null;
        }

        #endregion

    }
}
