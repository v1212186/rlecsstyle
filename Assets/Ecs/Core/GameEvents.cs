﻿using System;
using System.Collections.Generic;

namespace Ecs.Core
{
    public interface IEvent<TSender, TArgs>
    {
        void Add(Action<TSender, TArgs> _handler);
        void Remove(Action<TSender, TArgs> _handler);
    }

    public class GameEvent<TSender, TArgs> : IEvent<TSender, TArgs> where TArgs : EventArgs
    {

        private List<Action<TSender, TArgs>> eventHandlers;

        public void Add(Action<TSender, TArgs> _handler)
        {
            if (eventHandlers == null)
            {
                eventHandlers = new List<Action<TSender, TArgs>>();
            }

            eventHandlers.Add(_handler);
        }

        public void Remove(Action<TSender, TArgs> _handler)
        {
            if (eventHandlers == null)
                throw new InvalidOperationException("Cannot remove a handler because it does not contain any.");

            eventHandlers.Remove(_handler);
        }

        public void Raise(TSender _sender, TArgs _args)
        {
            if (eventHandlers != null)
            {
                for (int i = 0; i < eventHandlers.Count; i++)
                {
                    eventHandlers[i](_sender, _args);
                }
            }
        }
    }

    public class ValueChangeEventArgs<TValue> : EventArgs
    {
        #region Variables

        public readonly TValue NewValue;
        public readonly TValue OldValue;

        #endregion

        #region Constructor

        public ValueChangeEventArgs(TValue _oldValueValue, TValue _newValueValue)
        {
            OldValue = _oldValueValue;
            NewValue = _newValueValue;
        }

        #endregion
    }
}