﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace Ecs.Core
{
    //TODO сделать пул компонентов?
    //TODO добавить эксепшенов
    public class Entity : IEquatable<Entity>, IJsonSerializable
    {
        #region Variables

        private readonly Dictionary<Type, EntityComponent> entityComponents;
        internal ReadOnlyDictionary<Type, EntityComponent> EntityComponents;

        private FastGuid guid;
        public FastGuid Guid => guid;

        #endregion

        #region Constructor

        internal Entity()
        {
            guid = FastGuid.NewGuid();
            entityComponents = new Dictionary<Type, EntityComponent>();
            EntityComponents = new ReadOnlyDictionary<Type, EntityComponent>(entityComponents);
        }

        #endregion

        #region Methods

        internal bool AddEntityComponent<T>(T _entityComponent) where T : EntityComponent
        {
            if (_entityComponent == null)
            {
                return false;
            }

            if (GetEntityComponent<T>() != null)
            {
                return false;
            }

            entityComponents.Add(_entityComponent.GetType(), _entityComponent);
            _entityComponent.Entity = this;

            return true;
        }

        public T GetEntityComponent<T>() where T : EntityComponent
        {
            if (!entityComponents.ContainsKey(typeof(T)))
            {
                return null;
            }

            return entityComponents[typeof(T)] as T;
        }

        public bool HasEntityComponent<T>() where T : EntityComponent
        {
            return entityComponents.ContainsKey(typeof(T));
        }

        internal bool RemoveEntityComponent<T>() where T : EntityComponent
        {
            if (!entityComponents.ContainsKey(typeof(T)))
            {
                return false;
            }

            EntityComponent entityComponent = GetEntityComponent<T>();

            entityComponent.Dispose();
            entityComponent.Entity = null;
            entityComponents.Remove(entityComponent.GetType());

            return true;
        }

        private void DisposeAllComponents()
        {
            foreach (KeyValuePair<Type, EntityComponent> entityComponent in entityComponents)
            {
                entityComponent.Value.Dispose();
                //entityComponent.Value.Entity = null;
            }
            //entityComponents.Clear();
        }

        public void Dispose()
        {
            DisposeAllComponents();
        }

        public JObject ToJson()
        {
            JObject entity = new JObject();
            JArray jArray = new JArray();
            foreach (KeyValuePair<Type, EntityComponent> entityComponent in entityComponents)
            {
                JObject jObject = entityComponent.Value.ToJson();
                if (jObject != null)
                {
                    jArray.Add(jObject);
                }
            }

            entity["Guid"] = Guid.GuidToJson();
            entity["Components"] = jArray;
            return entity;
        }

        public void FromJson(JObject _jObject)
        {
            guid = Guid.GuidFromJson(_jObject["Guid"].ToObject<JObject>());

            foreach (JObject component in _jObject["Components"].Values<JObject>())
            {
                Type type = component["Type"].ToObject<Type>();
                if (type == null)
                {
                    throw new NullReferenceException();
                }
                EntityComponent entityComponent = (EntityComponent)Activator.CreateInstance(type, true);
                AddEntityComponent(entityComponent);
                entityComponent.FromJson(component);
            }
        }

        public bool Equals(Entity _other)
        {
            if (ReferenceEquals(null, _other)) return false;
            if (ReferenceEquals(this, _other)) return true;
            return Guid.Equals(_other.Guid);
        }

        public override bool Equals(object _obj)
        {
            if (ReferenceEquals(null, _obj)) return false;
            if (ReferenceEquals(this, _obj)) return true;
            if (_obj.GetType() != this.GetType()) return false;
            return Equals((Entity)_obj);
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }

        #endregion
    }
}
