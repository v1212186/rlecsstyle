﻿using Newtonsoft.Json.Linq;

namespace Ecs.Core
{
    public interface IJsonSerializable
    {

        JObject ToJson();

        void FromJson(JObject _jsonObject);
    }
}
