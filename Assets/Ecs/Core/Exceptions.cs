﻿using System;

namespace Ecs.Core
{
    [Serializable]
    public class CantRegisterEntityException : Exception
    {
        #region Constructor

        public CantRegisterEntityException(string _message) : base(_message)
        {

        }

        #endregion
    }

    [Serializable]
    public class CantUnregisterEntityException : Exception
    {
        #region Constructor

        public CantUnregisterEntityException(string _message) : base(_message)
        {

        }

        #endregion
    }
}
