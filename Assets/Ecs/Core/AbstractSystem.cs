﻿using System;

namespace Ecs.Core
{
    //TODO в качестве кешироания создавать структуры(классы), которые будут хранить все нужные для группы компоненты + словарь, позволит избавиться от GC 
    public abstract class AbstractSystem : IComparable<AbstractSystem>
    {

        #region Variables

        private readonly int priority;

        protected readonly EntityManager entityManager;

        #endregion

        #region Constructor

        protected AbstractSystem(int _priority, EntityManager _entityManager)
        {
            priority = _priority;
            entityManager = _entityManager;
        }

        #endregion

        #region Methods

        public void ProcessSystem(float _deltaTime)
        {
            OnProcessSystem(_deltaTime);
            entityManager.ProcessCommandBuffer();
        }

        public abstract void OnProcessSystem(float _deltaTime);

        public int CompareTo(AbstractSystem _other)
        {
            if (priority > _other.priority)
            {
                return 1;
            }
            if (priority < _other.priority)
            {
                return -1;
            }
            return 0;
        }

        /// <summary>
        /// Put in queue <see cref="Entity"/> to register in <see cref="CommandBuffer"/>
        /// </summary>
        /// <param name="_entity"><see cref="Entity"/> to register</param>
        protected void RegisterEntityCommandBuffer(Entity _entity)
        {
            entityManager.RegisterEntityToCommandBuffer(_entity);
        }

        /// <summary>
        /// Put in queue <see cref="EntityComponent"/> to add to <see cref="Entity"/> in <see cref="CommandBuffer"/>
        /// </summary>
        /// <param name="_entity"><see cref="Entity"/></param>
        /// <param name="_entityComponent"><see cref="EntityComponent"/> to add</param>
        protected void AddComponentCommandBuffer<T>(Entity _entity, T _entityComponent) where T:EntityComponent
        {
            entityManager.AddComponentToCommandBuffer<T>(_entity, _entityComponent);
        }

        /// <summary>
        /// Put in queue <see cref="EntityComponent"/> to remove from <see cref="Entity"/> in <see cref="CommandBuffer"/>
        /// </summary>
        /// <param name="_entity"><see cref="Entity"/></param>
        /// <param name="_id">Entity component id</param>
        protected void RemoveComponentCommandBuffer<T>(Entity _entity) where T : EntityComponent
        {
            entityManager.RemoveComponentToCommandBuffer<T>(_entity);
        }

        /// <summary>
        /// Put in queue <see cref="Entity"/> to dispose in <see cref="CommandBuffer"/>
        /// </summary>
        /// <param name="_entity"><see cref="Entity"/> to dispose</param>
        protected void DisposeEntityCommandBuffer(Entity _entity)
        {
            entityManager.DisposeEntityToCommandBuffer(_entity);
        }

        #endregion

    }
}
