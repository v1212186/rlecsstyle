﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace Ecs.Core
{
    public class ComponentsFilter : IEquatable<ComponentsFilter>, IJsonSerializable
    {

        #region Variables

        private readonly HashSet<Type> anyType = new HashSet<Type>();
        private readonly HashSet<Type> allType = new HashSet<Type>();
        private readonly HashSet<Type> noneType = new HashSet<Type>();

        #endregion

        #region Constructor

        public ComponentsFilter()
        {

        }

        public ComponentsFilter(JObject _jObject)
        {
            FromJson(_jObject);
        }

        #endregion

        #region Methods

        #region Construction methods

        public ComponentsFilter AnyOf(params Type[] _includeAnyType)
        {
            if (_includeAnyType.Length == 0)
            {
                return this;
            }

            foreach (Type t in _includeAnyType)
            {
                if (!anyType.Contains(t))
                {
                    anyType.Add(t);
                }
            }

            return this;
        }

        public ComponentsFilter AllOf(params Type[] _includeAllType)
        {
            if (_includeAllType.Length == 0)
            {
                return this;
            }

            foreach (Type t in _includeAllType)
            {
                if (!allType.Contains(t))
                {
                    allType.Add(t);
                }
            }

            return this;
        }

        public ComponentsFilter NoneOf(params Type[] _excludeType)
        {
            if (_excludeType.Length == 0)
            {
                return this;
            }

            foreach (Type t in _excludeType)
            {
                if (!noneType.Contains(t))
                {
                    noneType.Add(t);
                }
            }

            return this;
        }

        #endregion

        #region Entity matching checks

        public bool IsEntityMatches(Entity _entity)
        {
            return HasAllComponents(_entity.EntityComponents) &&
                   HasNoneComponent(_entity.EntityComponents) &&
                   HasAnyComponent(_entity.EntityComponents);
        }

        private bool HasAnyComponent(ReadOnlyDictionary<Type, EntityComponent> _entityComponents)
        {
            if (anyType.Count == 0)
            {
                return true;
            }

            foreach (Type type in anyType)
            {
                if (_entityComponents.ContainsKey(type))
                {
                    return true;
                }
            }

            return _entityComponents.Count == 0 && anyType.Count == 0;
        }

        private bool HasAllComponents(ReadOnlyDictionary<Type, EntityComponent> _entityComponents)
        {
            if (allType.Count == 0)
            {
                return true;
            }

            bool hasAll = true;

            foreach (Type type in allType)
            {
                if (!_entityComponents.ContainsKey(type))
                {
                    hasAll = false;
                    break;
                }
            }

            return hasAll;
        }

        private bool HasNoneComponent(ReadOnlyDictionary<Type, EntityComponent> _entityComponents)
        {
            if (noneType.Count == 0)
            {
                return true;
            }

            foreach (Type type in noneType)
            {
                if (_entityComponents.ContainsKey(type))
                    return false;
            }

            return true;
        }

        #endregion

        #region Containment checks

        internal bool AnyTypeContains(Type _type)
        {
            return anyType.Contains(_type);
        }

        internal bool AllTypeContains(Type _type)
        {
            return allType.Contains(_type);
        }

        internal bool NoneTypeContains(Type _type)
        {
            return noneType.Contains(_type);
        }

        #endregion

        #region Equality comparison

        public bool Equals(ComponentsFilter _other)
        {
            if (ReferenceEquals(null, _other)) return false;
            if (ReferenceEquals(this, _other)) return true;
            return anyType.SetEquals(_other.anyType) && noneType.SetEquals(_other.noneType) && allType.SetEquals(_other.allType);
        }

        public override bool Equals(object _obj)
        {
            if (ReferenceEquals(null, _obj)) return false;
            if (ReferenceEquals(this, _obj)) return true;
            if (_obj.GetType() != this.GetType()) return false;
            return Equals((ComponentsFilter)_obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (anyType != null ? anyType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (allType != null ? allType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (noneType != null ? noneType.GetHashCode() : 0);
                return hashCode;
            }
        }

        #endregion

        #region Serialization

        public JObject ToJson()
        {
            JArray any = new JArray(anyType);
            JArray all = new JArray(allType);
            JArray none = new JArray(noneType);

            JObject jObject = new JObject
            {
                ["anyType"] = any,
                ["allType"] = all,
                ["noneType"] = none
            };
            return jObject;
        }

        public void FromJson(JObject _jsonObject)
        {
            JArray any = _jsonObject["anyType"].ToObject<JArray>();
            foreach (JToken token in any)
            {
                anyType.Add(token.ToObject<Type>());
            }

            JArray all = _jsonObject["allType"].ToObject<JArray>();
            foreach (JToken token in all)
            {
                allType.Add(token.ToObject<Type>());
            }

            JArray none = _jsonObject["noneType"].ToObject<JArray>();
            foreach (JToken token in none)
            {
                noneType.Add(token.ToObject<Type>());
            }
        }

        #endregion

        #endregion


    }
}
