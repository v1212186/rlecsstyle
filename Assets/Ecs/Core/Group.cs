﻿using System;
using System.Collections.Generic;

namespace Ecs.Core
{
    public class EntityEventArgs : EventArgs
    {
        #region Variables

        public readonly Entity Entity;

        #endregion

        #region Constructor

        public EntityEventArgs(Entity _entity)
        {
            Entity = _entity;
        }

        #endregion
    }

    public sealed class Group : IEquatable<Group>
    {

        #region Variables

        private readonly HashSet<Entity> entities = new HashSet<Entity>();

        public int Count
        {
            get { return entities.Count; }
        }

        private Entity[] entitiesCache;

        public readonly GameEvent<Group, EntityEventArgs> GroupEntityAddedEvent = new GameEvent<Group, EntityEventArgs>();
        public readonly GameEvent<Group, EntityEventArgs> GroupEntityRemovedEvent = new GameEvent<Group, EntityEventArgs>();

        private readonly ComponentsFilter componentsFilter;

        #endregion

        #region Constructor

        public Group(ComponentsFilter _componentsFilter)
        {
            componentsFilter = _componentsFilter;
        }

        #endregion

        #region Methods

        public void UpdateGroup<T>(Entity _entity) where T : EntityComponent
        {
            if (GroupContainsEntityComponent<T>())
            {
                if (IsEntityMatches(_entity))
                {
                    AddEntityInternal(_entity);
                }
                else
                {
                    RemoveEntityInternal(_entity);
                }
            }
        }

        public bool AddEntityToGroup(Entity _entity)
        {
            if (!IsEntityMatches(_entity))
            {
                return false;
            }

            return AddEntityInternal(_entity);
        }

        public bool RemoveEntityFromGroup(Entity _entity)
        {
            if (!IsEntityMatches(_entity))
            {
                return false;
            }

            return RemoveEntityInternal(_entity);
        }

        private bool IsEntityMatches(Entity _entity)
        {
            return componentsFilter.IsEntityMatches(_entity);
        }

        private bool AddEntityInternal(Entity _entity)
        {
            if (entities.Add(_entity))
            {
                entitiesCache = null;
                GroupEntityAddedEvent.Raise(this, new EntityEventArgs(_entity));
                return true;
            }

            return false;
        }

        private bool RemoveEntityInternal(Entity _entity)
        {
            if (entities.Remove(_entity))
            {
                entitiesCache = null;
                GroupEntityRemovedEvent.Raise(this, new EntityEventArgs(_entity));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if group contains <see cref="EntityComponent"/> in <see cref="ComponentsFilter"/>
        /// </summary>
        /// <typeparam name="T">Entity component <see cref="Type"/></typeparam>
        /// <returns></returns>
        private bool GroupContainsEntityComponent<T>() where T : EntityComponent
        {
            Type type = typeof(T);

            return componentsFilter.AllTypeContains(type) || componentsFilter.AnyTypeContains(type) || componentsFilter.NoneTypeContains(type);
        }

        public bool ContainsEntity(Entity _entity)
        {
            return entities.Contains(_entity);
        }

        //TODO профайлер говорит что тут постоянно генерируется GC, может что-то не так с кешем?
        public Entity[] GetEntities()
        {
            if (entitiesCache == null)
            {
                entitiesCache = new Entity[entities.Count];
                entities.CopyTo(entitiesCache);
            }

            return entitiesCache;
        }

        public bool Equals(Group _other)
        {
            if (ReferenceEquals(null, _other)) return false;
            if (ReferenceEquals(this, _other)) return true;
            return Equals(componentsFilter, _other.componentsFilter);
        }

        public override bool Equals(object _obj)
        {
            if (ReferenceEquals(null, _obj)) return false;
            if (ReferenceEquals(this, _obj)) return true;
            return _obj is Group && Equals((Group)_obj);
        }

        public override int GetHashCode()
        {
            return (componentsFilter != null ? componentsFilter.GetHashCode() : 0);
        }

        #endregion
    }
}
