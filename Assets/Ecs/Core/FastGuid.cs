﻿using System;

namespace Ecs.Core
{
    public struct FastGuid : IEquatable<FastGuid>
    {

        #region Variables

        public readonly long guidPart0;
        public readonly long guidPart1;

        #endregion

        #region Properties

        #endregion

        public FastGuid(long _guidPart0, long _guidPart1)
        {
            guidPart0 = _guidPart0;
            guidPart1 = _guidPart1;
        }

        #region Methods

        public static bool operator ==(FastGuid _fastGuid0, FastGuid _fastGuid1)
        {
            return _fastGuid0.Equals(_fastGuid1);
        }

        public static bool operator !=(FastGuid _fastGuid0, FastGuid _fastGuid1)
        {
            return _fastGuid0.Equals(_fastGuid1);
        }

        public static FastGuid NewGuid()
        {
            Guid guid = Guid.NewGuid();
            byte[] guidByteArray = guid.ToByteArray();
            long guidPart0 = BitConverter.ToInt64(guidByteArray, 0);
            long guidPart1 = BitConverter.ToInt64(guidByteArray, 8);
            return new FastGuid(guidPart0, guidPart1);
        }

        public bool Equals(FastGuid _other)
        {
            return guidPart0 == _other.guidPart0 && guidPart1 == _other.guidPart1;
        }

        public override bool Equals(object _other)
        {
            FastGuid? fastOther = _other as FastGuid?;
            return fastOther.HasValue && Equals(fastOther.Value);
        }

        public override int GetHashCode()
        {
            return (guidPart0.GetHashCode() * 397) ^ guidPart1.GetHashCode();
        }

        #endregion


    }
}
