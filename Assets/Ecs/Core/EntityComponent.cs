﻿using Newtonsoft.Json.Linq;

namespace Ecs.Core
{
    public abstract class EntityComponent : IJsonSerializable
    {

        #region Variables

        private Entity entity;
        public Entity Entity
        {
            get
            {
                return entity;
            }
            internal set
            {
                entity = value;
            }
        }

        #endregion

        #region Empty constructor for serialization

        protected EntityComponent()
        {

        }

        #endregion

        #region Methods

        public abstract void Dispose();

        public JObject ToJson()
        {
            return OnToJson();
        }

        protected abstract JObject OnToJson();

        public void FromJson(JObject _jObject)
        {
            OnFromJson(_jObject);
        }

        protected abstract void OnFromJson(JObject _jsonObject);

        #endregion


    }
}
