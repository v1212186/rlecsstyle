﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Ecs.Core
{
    public sealed partial class EntityManager
    {
        #region Variables

        private readonly Dictionary<FastGuid, Entity> entities;

        private readonly CommandBuffer commandBuffer;

        #endregion

        #region Constructor

        public EntityManager()
        {
            entities = new Dictionary<FastGuid, Entity>();
            groups = new List<Group>();
            commandBuffer = new CommandBuffer();
        }

        #endregion

        #region Methods

        public void ProcessCommandBuffer()
        {
            commandBuffer.ProcessCommandBuffer(this);
        }

        public bool IsEntityRegistered(Entity _entity)
        {
            return entities.ContainsKey(_entity.Guid);
        }

        public Entity GetEntityByGuid(FastGuid _guid)
        {
            return entities[_guid];
        }

        public Entity CreateEntity()
        {
            return new Entity();
        }

        public bool RegisterEntity(Entity _entity)
        {
            if (IsEntityRegistered(_entity))
            {
                Exception exception = new CantRegisterEntityException("Tried to register already registered entity");
                exception.Data.Add("Entity", _entity);
                exception.Data.Add("EntityManager", this);
                throw exception;
            }
            entities.Add(_entity.Guid, _entity);
            AddEntityToGroups(_entity);
            return true;
        }

        internal void RegisterEntityToCommandBuffer(Entity _entity)
        {
            commandBuffer.RegisterEntity(_entity);
        }


        public void AddTempFlagComponent<T>(Entity _entity, T _entityComponent) where T : EntityComponent
        {
            AddComponent(_entity, _entityComponent);
            RemoveComponent<T>(_entity);
        }

        //TODO может пусть возвращает сам компонент?
        public bool AddComponent<T>(Entity _entity, T _entityComponent) where T : EntityComponent
        {
            bool isAdded = _entity.AddEntityComponent<T>(_entityComponent);
            if (isAdded && entities.ContainsKey(_entity.Guid))
            {
                UpdateGroups<T>(_entity);
            }
            return isAdded;
        }

        internal void AddComponentToCommandBuffer<T>(Entity _entity, T _entityComponent) where T : EntityComponent
        {
            commandBuffer.AddComponent<T>(_entity, _entityComponent);
        }

        public bool RemoveComponent<T>(Entity _entity) where T : EntityComponent
        {
            bool isRemoved = _entity.RemoveEntityComponent<T>();
            if (isRemoved && entities.ContainsKey(_entity.Guid))
            {
                UpdateGroups<T>(_entity);
            }

            return isRemoved;
        }

        internal void RemoveComponentToCommandBuffer<T>(Entity _entity) where T : EntityComponent
        {
            commandBuffer.RemoveComponent<T>(_entity);
        }

        public bool DisposeEntity(Entity _entity)
        {
            if (!IsEntityRegistered(_entity))
            {
                Exception exception = new CantUnregisterEntityException("Tried to dispose not registered entity");
                exception.Data.Add("Entity", _entity);
                exception.Data.Add("EntityManager", this);
                throw exception;
            }
            RemoveEntityFromGroups(_entity);
            _entity.Dispose();
            entities.Remove(_entity.Guid);
            return true;
        }

        public Entity CreateEntityFromJson(JObject _jObject)
        {
            Entity entity = CreateEntity();

            entity.FromJson(_jObject);

            return entity;
        }

        public void DisposeAllEntities()
        {
            Entity[] entitesToDispose = entities.Values.ToArray();
            foreach (Entity entity in entitesToDispose)
            {
                DisposeEntity(entity);
            }
        }

        internal void DisposeEntityToCommandBuffer(Entity _entity)
        {
            commandBuffer.DisposeEntity(_entity);
        }

        #endregion

    }
}
