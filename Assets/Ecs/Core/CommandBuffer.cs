﻿using System.Collections.Generic;

namespace Ecs.Core
{
    public class CommandBuffer
    {

        #region Nested classes

        private interface IBufferCommand
        {

            #region Methods

            void ProcessCommand(EntityManager _entityManager);

            #endregion

        }

        private class AddComponentBufferCommand<T> : IBufferCommand where T : EntityComponent
        {
            #region Variables

            private readonly Entity entity;
            private readonly T entityComponent;

            #endregion

            #region Constructor

            public AddComponentBufferCommand(Entity _entity, T _entityComponent)
            {
                entity = _entity;
                entityComponent = _entityComponent;
            }

            #endregion

            #region Methods

            public void ProcessCommand(EntityManager _entityManager)
            {
                _entityManager.AddComponent(entity, entityComponent);
            }

            #endregion
        }


        private class RemoveComponentBufferCommand<T> : IBufferCommand where T : EntityComponent
        {
            #region Variables

            private readonly Entity entity;

            #endregion

            #region Constructor

            public RemoveComponentBufferCommand(Entity _entity)
            {
                entity = _entity;
            }

            #endregion

            #region Methods

            public void ProcessCommand(EntityManager _entityManager)
            {
                _entityManager.RemoveComponent<T>(entity);
            }

            #endregion
        }

        private class RegisterEntityBufferCommand : IBufferCommand
        {
            #region Variables

            private readonly Entity entity;

            #endregion

            #region Constructor

            public RegisterEntityBufferCommand(Entity _entity)
            {
                entity = _entity;
            }

            #endregion

            #region Methods

            public void ProcessCommand(EntityManager _entityManager)
            {
                _entityManager.RegisterEntity(entity);
            }

            #endregion
        }

        private class DisposeEntityBufferCommand : IBufferCommand
        {
            #region Variables

            private readonly Entity entity;

            #endregion

            #region Constructor

            public DisposeEntityBufferCommand(Entity _entity)
            {
                entity = _entity;
            }

            #endregion

            #region Methods

            public void ProcessCommand(EntityManager _entityManager)
            {
                _entityManager.DisposeEntity(entity);
            }

            #endregion
        }

        #endregion

        #region Variables

        private readonly Queue<IBufferCommand> bufferCommands;

        #endregion

        #region Constructor

        public CommandBuffer()
        {
            bufferCommands = new Queue<IBufferCommand>();
        }

        #endregion

        #region Methods

        internal void RegisterEntity(Entity _entity)
        {
            bufferCommands.Enqueue(new RegisterEntityBufferCommand(_entity));
        }

        internal void AddComponent<T>(Entity _entity, T _entityComponent) where T : EntityComponent
        {
            bufferCommands.Enqueue(new AddComponentBufferCommand<T>(_entity, _entityComponent));
        }

        internal void RemoveComponent<T>(Entity _entity) where T : EntityComponent
        {
            bufferCommands.Enqueue(new RemoveComponentBufferCommand<T>(_entity));
        }

        internal void DisposeEntity(Entity _entity)
        {
            bufferCommands.Enqueue(new DisposeEntityBufferCommand(_entity));
        }

        //TODO может сделать апдейт групп только после выполнения всех команд буфера
        public void ProcessCommandBuffer(EntityManager _entityManager)
        {
            while (bufferCommands.Count > 0)
            {
                bufferCommands.Dequeue().ProcessCommand(_entityManager);
            }
        }

        #endregion
    }
}
